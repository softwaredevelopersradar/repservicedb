﻿using System;
using ModelsTablesDBLib;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace InheritorsEventArgs
{
    public class TableEventArs<T> :EventArgs where T :AbstractCommonTable
    {
        public List<T> Table { get; protected set; }

        public TableEventArs(ClassDataCommon lData)
        {
            Table = lData.ToList<T>();
        }

        public TableEventArs(List<T> lData)
        {
            Table = lData;
        }
    }
}