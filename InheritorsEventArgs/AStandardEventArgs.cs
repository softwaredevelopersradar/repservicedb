﻿using System;
using System.Runtime.Serialization;

namespace InheritorsEventArgs
{
    [DataContract]
    //[KnownType(typeof(EventArgs))]
    public abstract class AStandardEventArgs : EventArgs
    {
        [DataMember]
        public abstract string GetMessage { get; protected set; }
    }
}
