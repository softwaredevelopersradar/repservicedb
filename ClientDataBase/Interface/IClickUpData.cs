﻿using InheritorsEventArgs;

namespace ClientDataBase
{
    internal interface IClickUpData
    {
       void ClickUpTable(ModelsTablesDBLib.ClassDataCommon dataCommon);
    }
}
