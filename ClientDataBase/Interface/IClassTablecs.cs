﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ModelsTablesDBLib;

namespace ClientDataBase
{
    public interface IClassTables
    {
        void Add(object obj);

        Task AddAsync(object obj);

        void AddRange(object rangeObj);

        Task AddRangeAsync(object rangeObj);

        void RemoveRange(object rangeObj);

        Task RemoveRangeAsync(object rangeObj);

        void Delete(object obj);

        Task DeleteAsync(object obj);

        void Clear();

        Task CLearAsync();

        void Change(object obj);

        Task ChangeAsync(object obj);

        List<T> Load<T>() where T: ModelsTablesDBLib.AbstractCommonTable;

        Task<List<T>> LoadAsync<T>() where T : ModelsTablesDBLib.AbstractCommonTable;
    }
}
