﻿using System;

namespace ClientDataBase
{
    public interface ITableUpRecord<T> where T:ModelsTablesDBLib.AbstractCommonTable
    {
        event EventHandler<T> OnAddRecord;

        event EventHandler<T> OnDeleteRecord;

        event EventHandler<T> OnChangeRecord;

    }
}
