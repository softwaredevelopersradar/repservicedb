﻿using ModelsTablesDBLib;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClientDataBase
{
    public interface ITableRoute
    {
        List<TableRoute> LoadByFilter(int NumberRoute);

        Task<List<TableRoute>> LoadByFilterAsync(int NumberRoute);
    }
}
