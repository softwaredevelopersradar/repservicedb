﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableUpdate<T> where T:ModelsTablesDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArs<T>> OnUpTable ;
    }
}
