﻿using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientDataBase
{
    public interface ITableAddRange<T> where T : ModelsTablesDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArs<T>> OnAddRange;
    }
}
