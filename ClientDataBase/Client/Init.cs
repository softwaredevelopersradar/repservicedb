﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using System.Text.RegularExpressions;
using ModelsTablesDBLib;
using InheritorsEventArgs;
using DataEventArgs = InheritorsEventArgs.DataEventArgs;
using OperationTableEventArgs = InheritorsEventArgs.OperationTableEventArgs;
using System.Threading.Tasks;

namespace ClientDataBase
{

    //[CallbackBehavior(AutomaticSessionShutdown = true, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public partial class ClientDB : IServiceDBCallback
    {
        private int ID;
        private string name = "";
        private string endpointAddress = "net.tcp://127.0.0.1:8302/";
        private ServiceDBClient ClientServiceDB;

        /// <summary>
        /// словарь, который хранит NameTable - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        public readonly Dictionary<NameTable, IClassTables> Tables = new Dictionary<NameTable, IClassTables>
        {
            {NameTable.TableASP, new ClassTable<TableASP>() },
            { NameTable.TableSectorsRangesRecon, new ClassInheritTableAsp<TableSectorsRangesRecon>() },
            { NameTable.TableSectorsRangesSuppr, new ClassInheritTableAsp<TableSectorsRangesSuppr>() },
            { NameTable.TableFreqForbidden, new ClassInheritTableAsp<TableFreqForbidden>() },
            { NameTable.TableFreqImportant, new ClassInheritTableAsp<TableFreqImportant>() },
            { NameTable.TableFreqKnown, new ClassInheritTableAsp<TableFreqKnown>() },
            { NameTable.TempFWS, new ClassTable<TempFWS>() },
            { NameTable.TempADSB, new ClassTable<TempADSB>()},
            { NameTable.TableReconFWS, new ClassTable<TableReconFWS>() },
            { NameTable.TableMission, new ClassTable<TableMission>() },
            { NameTable.TableOwnResources, new ClassTable<TableOwnResources>() },
            { NameTable.TableAreaResponsibility, new ClassTable<TableAreaResponsibility>() },
            { NameTable.TableLandCoordUAV, new ClassTable<TableLandCoordUAV>() },
            { NameTable.TableEnemyResources, new ClassTable<TableEnemyResources>() },
            { NameTable.TableFrontLineEnemyTroops, new ClassTable<TableFrontLineEnemyTroops>() },
            { NameTable.TableFrontLineOwnTroops, new ClassTable<TableFrontLineOwnTroops>() },
            { NameTable.TableRoute, new ClassTableRoute() },
            { NameTable.GlobalProperties, new ClassTable<GlobalProperties>()},
            { NameTable.TableSuppressFWS, new ClassInheritTableAsp<TableSuppressFWS>()},
            { NameTable.TempSuppressFWS, new ClassInheritTableAsp<TempSuppressFWS>()},
            { NameTable.TableSuppressFHSS, new ClassInheritTableAsp<TableSuppressFHSS>()},
            { NameTable.TableFHSSExcludedFreq, new ClassDependFhss<TableFHSSExcludedFreq>()},
            { NameTable.TableReconFHSS, new ClassTable<TableReconFHSS>()},
            { NameTable.TableSourceFHSS, new ClassDependFhss<TableSourceFHSS>()},
            { NameTable.TableFHSSReconExcluded, new ClassDependFhss<TableFHSSReconExcluded>()},
            { NameTable.TempGNSS, new ClassTable<TempGNSS>()},
            { NameTable.TempSuppressFHSS, new ClassTable<TempSuppressFHSS>() },
            { NameTable.ButtonsNAV, new ClassInheritTableAsp<ButtonsNAV>() },
            { NameTable.TableChat, new ClassTable<TableChatMessage>() }

        };

        #region Events
        public event EventHandler<ClientEventArgs> OnConnect = (object obj, ClientEventArgs evenArgs) => { };
        public event EventHandler<ClientEventArgs> OnDisconnect = (object obj, ClientEventArgs evenArgs) => { };
        public event EventHandler<DataEventArgs> OnUpData = (object obj, DataEventArgs evenArgs) => { };
        public event EventHandler<OperationTableEventArgs> OnErrorDataBase = (object obj, OperationTableEventArgs evenArgs) => { };
        #endregion

        private bool ValidEndPoint(string endpointAddress)
        {
            string ValidEndpointRegex = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";    // IP validation 
            Regex r = new Regex(ValidEndpointRegex, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(endpointAddress);
            return m.Success;
        }
        

        public async void DataCallback(DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
                {
                    OnUpData(this, lData);
                    (Tables[lData.Name] as IClickUpData).ClickUpTable(lData.AbstractData);
               }).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }

        public async void ErrorCallback(OperationTableEventArgs eventArgs)
        {
            await Task.Run(() =>
            OnErrorDataBase(this, eventArgs)
            ).ConfigureAwait(false);
        }

        public void Abort()
        {
            ClientServiceDB.Abort();
            ((IDisposable)ClientServiceDB).Dispose();
            ClientServiceDB = null;

            foreach (ClassTable table in Tables.Values)
                table.Dispose();
            OnDisconnect(null, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect));
        }

        public async  void DataCallback(ServiceDB.DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
               {
                    OnUpData(this, new DataEventArgs(lData.Name, lData.AbstractData));
                    (Tables[lData.Name] as IClickUpData).ClickUpTable(lData.AbstractData);
               }).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }

        public async void ErrorCallback(ServiceDB.OperationTableEventArgs error)
        {
            await Task.Run(() => 
            OnErrorDataBase(this, new OperationTableEventArgs(error.Operation, error.TypeError))
            ).ConfigureAwait(false);
        }

        public async void RecordCallBack(ServiceDB.RecordEventArgs recordEventArgs)
        {
            await Task.Run(() => 
            (Tables[recordEventArgs.Name] as IClickUpRecord).ClickUpRecord(new InheritorsEventArgs.RecordEventArgs(recordEventArgs.Name, recordEventArgs.AbstractRecord, recordEventArgs.NameAction))
            ).ConfigureAwait(false);
        }

        public async void RangeCallBack(ServiceDB.DataEventArgs lData)
        {
            try
            {
                await Task.Run(() => 
                (Tables[lData.Name] as IClickUpAddRange).ClickUpAddRange(lData.AbstractData)
               ).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }
    }
}
