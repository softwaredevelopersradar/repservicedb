﻿using Errors;

namespace ClientDataBase.Exceptions
{
    public interface IExceptionClient
    {
        EnumClientError Error { get; }

        string Message { get; }
    }
}
