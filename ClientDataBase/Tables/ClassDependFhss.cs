﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using ModelsTablesDBLib;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ClientDataBase
{
    internal class ClassDependFhss<T> : ClassTable<T>, IDependentFHSS where T: AbstractDependentFHSS
    {
        public void ClearByFilter(int FhssId)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new Exceptions.ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                ClientServiceDB.ClearTableByFilter(Name, FhssId, Id);
                return;
            }
            catch (Exceptions.ExceptionClient except)
            {
                throw new Exceptions.ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new Exceptions.ExceptionClient(error.Message);
            }
        }
    }
}
