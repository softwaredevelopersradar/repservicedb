﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using ModelsTablesDBLib;
using InheritorsEventArgs;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ClientDataBase
{
    internal class ClassTableRoute : ClassTable<TableRoute>, ITableRoute
    {
        public ClassTableRoute(ref ServiceDBClient clientServiceDB, int id) : base(ref clientServiceDB, id) 
        { }

        public ClassTableRoute() : base()
        { }

        public List<TableRoute> LoadByFilter(int NumberRoute)
        {
            ClassDataCommon Data = new ClassDataCommon();
            try
            {
                if (!ValidConnection())
                {
                    throw new Exceptions.ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                Data = ClientServiceDB.LoadDataFilterRoute(Id, NumberRoute);
                return Data.ToList<TableRoute>();
            }
            catch (Exceptions.ExceptionClient except)
            {
                throw new Exceptions.ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new Exceptions.ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new Exceptions.ExceptionClient(error.Message);
            }
        }

        public async Task<List<TableRoute>> LoadByFilterAsync(int NumberRoute)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new Exceptions.ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var result = await ClientServiceDB.LoadDataFilterRouteAsync(Id, NumberRoute);
                return result.ToList<TableRoute>();
            }
            catch (Exceptions.ExceptionClient except)
            {
                throw new Exceptions.ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new Exceptions.ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new Exceptions.ExceptionClient(error.Message);
            }
        }
    }
}
