﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using ModelsTablesDBLib;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ClientDataBase
{
    internal class ClassInheritTableAsp<T> : ClassTable<T>, IDependentAsp where T : AbstractDependentASP
    {
        public ClassInheritTableAsp(ref  ServiceDBClient clientServiceDB, int id) : base(ref clientServiceDB, id)
        { }

        public ClassInheritTableAsp() : base()
        {
        }

        public List<V>  LoadByFilter<V>(int NumberASP) where V : AbstractDependentASP
        {
            ClassDataDependASP Data = new ClassDataDependASP();
            try
            {
                //lock (locker)
                //{
                    if (!ValidConnection())
                    {
                        throw new Exceptions.ExceptionClient(Errors.EnumClientError.NoConnection);
                    }
                    //Data = ClientServiceDB.LoadDataFilterASPAsync(Name, NumberASP, Id).Result;
                    Data = ClientServiceDB.LoadDataFilterASP(Name, NumberASP, Id);
                    return Data.ToList<V>();
                //}
            }
            catch (Exceptions.ExceptionClient except)
            {
                throw new Exceptions.ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new Exceptions.ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new Exceptions.ExceptionClient(error.Message);
            }
        }

        public async Task<List<V>> LoadByFilterAsync<V>(int NumberASP) where V : AbstractDependentASP
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new Exceptions.ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var result =  await ClientServiceDB.LoadDataFilterASPAsync(Name, NumberASP, Id);
                return result.ToList<V>();
            }
            catch (Exceptions.ExceptionClient except)
            {
                throw new Exceptions.ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new Exceptions.ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new Exceptions.ExceptionClient(error.Message);
            }
        }

        public void ClearByFilter(int NumberASP)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new Exceptions.ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                ClientServiceDB.ClearTableByFilter(Name, NumberASP, Id);
                return;
            }
            catch (Exceptions.ExceptionClient except)
            {
                throw new Exceptions.ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new Exceptions.ExceptionClient(error.Message);
            }
        }
    }
}
