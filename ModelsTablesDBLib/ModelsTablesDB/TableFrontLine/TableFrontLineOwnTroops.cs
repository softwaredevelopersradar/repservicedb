﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Линия фронта своих войск
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableFrontLineOwnTroops)]
    [KnownType(typeof(TableFrontLine))]
    
    public class TableFrontLineOwnTroops: TableFrontLine
    {
    }
}
