﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Линия фронта войск противника
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableAreaResponsibility)]
    [KnownType(typeof(TableFrontLine))]
    
    public class TableAreaResponsibility : TableFrontLine
    {
    }
}
