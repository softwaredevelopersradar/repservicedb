﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractDependentMission))]
    [KnownType(typeof(TableFrontLineOwnTroops))]
    [KnownType(typeof(TableFrontLineEnemyTroops))]
    [KnownType(typeof(TableAreaResponsibility))]
    
    public class TableFrontLine : AbstractDependentMission
    {
        [DataMember]        
        public override int IdMission { get; set; }
        
        [DataMember]        
        public override int Id { get; set; }

        [DataMember]        
        public byte Num { get; set; } // Номер точки

        [DataMember]        
        public Coord Coordinates { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id }; // IdMission, 
        }

        public override void Update(AbstractCommonTable record)
        {
            Coordinates = ((TableFrontLine)record).Coordinates;
            Num = ((TableFrontLine)record).Num;
        }

        public TableFrontLine Clone()
        {
            return new TableFrontLine
            {
                Id = Id,
                IdMission = IdMission,
                Num = Num,
                Coordinates = Coordinates
            };
        }

        public TableFrontLineOwnTroops ToFrontLineOwnTroops()
        {
            return new TableFrontLineOwnTroops
            {
                Id = Id,
                IdMission = IdMission,
                Coordinates = Coordinates,
                Num = Num
            };
        }
        public TableFrontLineEnemyTroops ToFrontLineEnemyTroops()
        {
            return new TableFrontLineEnemyTroops
            {
                Id = Id,
                IdMission = IdMission,
                Coordinates = Coordinates,
                Num = Num
            };
        }
        public TableAreaResponsibility ToAreaRespons()
        {
            return new TableAreaResponsibility
            {
                Id = Id,
                IdMission = IdMission,
                Coordinates = Coordinates,
                Num = Num
            };
        }
    }
}
