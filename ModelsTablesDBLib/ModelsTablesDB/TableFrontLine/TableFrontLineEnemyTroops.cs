﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Линия фронта войск противника
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableFrontLineEnemyTroops)]
    [KnownType(typeof(TableFrontLine))]
    
    public class TableFrontLineEnemyTroops : TableFrontLine
    {
    }
}
