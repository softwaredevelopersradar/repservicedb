﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [InfoTable(NameTable.TableSourceFHSS)]
    [KnownType(typeof(AbstractDependentFHSS))]
    
    public class TableSourceFHSS : AbstractDependentFHSS
    {
        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public override int IdFHSS { get; set; }

        [DataMember]
        
        public ObservableCollection<TableJamDirect> ListJamDirect { get; set; }

        [DataMember]
        public Coord Coordinates { get; set; }
        
        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Coordinates.Altitude = ((TableSourceFHSS)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TableSourceFHSS)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TableSourceFHSS)record).Coordinates.Longitude;

            if (((TableSourceFHSS)record).ListJamDirect != null && ((TableSourceFHSS)record).ListJamDirect.Count != 0)
            {
                ListJamDirect.Clear();
                foreach (var rec in ((TableSourceFHSS)record).ListJamDirect)
                    ListJamDirect.Add(rec);
            }
        }
    }
}
