﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [InfoTable(NameTable.TableFHSSReconExcluded)]
    [KnownType(typeof(AbstractDependentFHSS))]
    
    public class TableFHSSReconExcluded : AbstractDependentFHSS
    {
        [DataMember]
        
        public override int IdFHSS { get; set; }

        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public double FreqKHz { get; set; }

        [DataMember]
        
        public float Deviation { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Deviation = ((TableFHSSReconExcluded)record).Deviation;
            FreqKHz = ((TableFHSSReconExcluded)record).FreqKHz;
        }

        public TableFHSSReconExcluded Clone()
        {
            return new TableFHSSReconExcluded()
            {
                Deviation = Deviation,
                FreqKHz = FreqKHz,
                Id = Id,
                IdFHSS = IdFHSS
            };
        }
    }
}
