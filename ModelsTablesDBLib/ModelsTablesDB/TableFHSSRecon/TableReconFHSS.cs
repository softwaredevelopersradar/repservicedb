﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// ИРИ ППРЧ ЦР
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableReconFHSS)]
    [KnownType(typeof(AbstractDependentMission))]
    
    public class TableReconFHSS : AbstractDependentMission
    {
        [DataMember]
        
        public override int IdMission { get; set; }

        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public double FreqMinKHz { get; set; }

        [DataMember]
        
        public double FreqMaxKHz { get; set; }

        [DataMember]
        
        public float Deviation { get; set; }

        [DataMember]
        
        public byte Modulation { get; set; }

        [DataMember]
        
        public DateTime Time { get; set; }
        
        [DataMember]
        
        public short StepKHz { get; set; }

        [DataMember]
        
        public ushort ImpulseDuration { get; set; }

        [DataMember]
        
        public short QuantitySignal { get; set; } 

        [DataMember]
        
        public bool? IsSelected { get; set; }
        
        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Deviation = ((TableReconFHSS)record).Deviation;
            FreqMaxKHz = ((TableReconFHSS)record).FreqMaxKHz;
            FreqMinKHz = ((TableReconFHSS)record).FreqMinKHz;
            ImpulseDuration = ((TableReconFHSS)record).ImpulseDuration;
            Modulation = ((TableReconFHSS)record).Modulation;
            QuantitySignal = ((TableReconFHSS)record).QuantitySignal;
            StepKHz = ((TableReconFHSS)record).StepKHz;
            Time = ((TableReconFHSS)record).Time;

            if (((TableReconFHSS)record).IsSelected.HasValue)
                IsSelected = ((TableReconFHSS)record).IsSelected;

        }
    }
}
