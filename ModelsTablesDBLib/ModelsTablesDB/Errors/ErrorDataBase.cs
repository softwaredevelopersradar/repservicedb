﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib
{
    [DataContract]
    public class ErrorDataBase
    {
        [DataContract]
        public enum Error
        {
            [EnumMember]
            UnknownError,
            [EnumMember]
            RecordExist,
            [EnumMember]
            RecordNotFound,
            [EnumMember]
            SuchASPAbsent,
            [EnumMember]
            SuchMissionAbsent,
            [EnumMember]
            NoColumnAsp,
            [EnumMember]
            EmptyMission,
            [EnumMember]
            NotActiveMission,
            [EnumMember]
            None
        }
        
        [DataMember]
        public Error Type { get; protected set; }
        
        [DataMember]
        private readonly Dictionary<Error, string> DicDescriptError = new Dictionary<Error, string>()
        {
            { Error.UnknownError, "Error: "},
            { Error.RecordExist, "Error: The table already has the similar record! "},
            { Error.SuchASPAbsent, "Error: There is no such station (ASP)"},
            { Error.SuchMissionAbsent, "Error: There is no such mission "},
            { Error.NoColumnAsp, "Error: The table hasn't column AddrAsp"},
            { Error.EmptyMission, "Error: The table of Mission is empty. Add some mission!"},
            { Error.NotActiveMission, "Error: The table of Mission hasn't any active mission."},
            { Error.RecordNotFound, "Error: The record not found! "},
            { Error.None, ""}
        };

        public string GetErrorMessage() {  return DicDescriptError[Type];  }

        public ErrorDataBase(Error error)
        {
            Type = error;
        }
    }
}
