﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(Coord))]
    public class CoordRoute
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public Coord Coordinates { get; set; } = new Coord();

    }
}
