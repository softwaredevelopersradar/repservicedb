﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Маршрут
    /// </summary>
    [DataContract]
    [KnownType(typeof(CoordRoute))]
    [KnownType(typeof(AbstractDependentMission))]
    [InfoTable(NameTable.TableRoute)]
    public class TableRoute : AbstractDependentMission
    {
        [DataMember]
        public override int IdMission { get; set; }
        
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public string Caption { get; set; } = string.Empty;

        [DataMember]
        public List<CoordRoute> ListCoordinates
        {
            get;
            set;
        }
        
        public override object[] GetKey()
        {
            return new object[] { Id };//, IdMission};
        }

        public override void Update(AbstractCommonTable record)
        {
            Caption = ((TableRoute)record).Caption;
            ListCoordinates = ((TableRoute)record).ListCoordinates;
        }
    }
}
