﻿using ModelsTablesDBLib.Interfaces;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// ИРИ ФРЧ РП
    /// </summary>
    [DataContract]
    [CategoryOrder(nameof(InterferenceParam),2)]
    [CategoryOrder(nameof(Coordinates),3)]
    [KnownType(typeof(AbstractDependentASP))]
    [InfoTable(NameTable.TableSuppressFWS)]
    
    public class TableSuppressFWS : AbstractDependentASP, IFixSupprFWS
    {
        #region Properties

        [DataMember]
        [Browsable(false)]
        
        public override int Id { get; set; } // уникальный номер записи

        [DataMember]
        [Browsable(false)]
        
        public override int NumberASP { get; set; } // номер станции

        [DataMember]
        [Browsable(false)]
        
        public override int IdMission { get; set; } // номер обстановки 

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Browsable(false)]
        
        public SignSender? Sender { get; set; } // знак отправителя

        [DataMember]
        [DisplayName("F, кГц")]
        
        public double? FreqKHz { get; set; } // частота

        [DataMember]
        [DisplayName("θ, °")]
        
        public float? Bearing { get; set; } //пеленг

        [DataMember]
        [Browsable(false)]
        
        public byte? Letter { get; set; } // Литера

        [DataMember]
        [DisplayName("U, дБ")]
        
        public short? Threshold { get; set; } // порог

        [DataMember]
        [DisplayName("Приоритет")]
        
        public byte? Priority { get; set; }

        [DataMember]
        [Category(nameof(InterferenceParam))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        
        public InterferenceParam InterferenceParam { get; set; } = new InterferenceParam();

        #endregion

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Bearing = ((TableSuppressFWS)record).Bearing ?? Bearing;
            FreqKHz = ((TableSuppressFWS)record).FreqKHz ?? FreqKHz;
            if (((TableSuppressFWS)record).InterferenceParam != null)
                InterferenceParam.Update(((TableSuppressFWS)record).InterferenceParam);
            Letter = ((TableSuppressFWS)record).Letter ?? Letter;
            Threshold = ((TableSuppressFWS)record).Threshold ?? Threshold;
            Sender = ((TableSuppressFWS)record).Sender;
            Priority = ((TableSuppressFWS)record).Priority ?? Priority;

            if (((TableSuppressFWS)record).Coordinates != null)
            {
                Coordinates.Altitude = ((TableSuppressFWS)record).Coordinates.Altitude;
                Coordinates.Latitude = ((TableSuppressFWS)record).Coordinates.Latitude;
                Coordinates.Longitude = ((TableSuppressFWS)record).Coordinates.Longitude;
            }
        }

        public TableSuppressFWS Clone()
        {
            return new TableSuppressFWS()
            {
                Bearing = Bearing,
                FreqKHz = FreqKHz,
                Id = Id,
                IdMission = IdMission,
                InterferenceParam = InterferenceParam.Clone(),
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Letter = Letter,
                Threshold = Threshold,
                NumberASP = NumberASP,
                Sender = Sender,
                Priority = Priority
            };
        }
    }
}
