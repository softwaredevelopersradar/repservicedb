﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Параметры помехи
    /// </summary>
    [DataContract]
    
    public class InterferenceParam : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private byte modulation;
        private byte deviation;
        private byte manipulation;
        private byte duration;

        #endregion

        #region Properties 

        [DataMember]
        [Column(nameof(Modulation))]
        [NotifyParentProperty(true)]
        
        public byte Modulation
        {
            get => modulation;
            set
            {
                if (modulation == value)
                    return;
                modulation = value;
                OnPropertyChanged(nameof(Modulation));
            }
        }

        [DataMember]
        [Column(nameof(Deviation))]
        [NotifyParentProperty(true)]
        
        public byte Deviation
        {
            get => deviation;
            set
            {
                if (deviation == value)
                    return;
                deviation = value;
                OnPropertyChanged(nameof(Deviation));
            }
        }
        
        [DataMember]
        [Column(nameof(Manipulation))]
        [NotifyParentProperty(true)]
        
        public byte Manipulation
        {
            get => manipulation;
            set
            {
                if (manipulation == value)
                    return;
                manipulation = value;
                OnPropertyChanged(nameof(Manipulation));
            }
        }

        [DataMember]
        [Column(nameof(Duration))]
        [NotifyParentProperty(true)]
        
        public byte Duration
        {
            get => duration;
            set
            {
                if (duration == value)
                    return;
                duration = value;
                OnPropertyChanged(nameof(Duration));
            }
        }

        #endregion

        public InterferenceParam Clone()
        {
            return new InterferenceParam()
            {
                Deviation = deviation,
                Duration = duration,
                Manipulation = manipulation,
                Modulation = modulation
            };
        }

        public void Update(InterferenceParam param)
        {
            Deviation = param.Deviation;
            Duration = param.Duration;
            Manipulation = param.Manipulation;
            Modulation = param.Modulation;
        }

    }
}
