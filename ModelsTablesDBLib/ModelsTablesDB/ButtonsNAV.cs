﻿
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Кнопки
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.ButtonsNAV)]
    [KnownType(typeof(AbstractDependentASP))]
    public class ButtonsNAV : AbstractDependentASP
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public bool BIridium { get; set; }

        [DataMember]
        public bool BInmarsat { get; set; }

        [DataMember]
        public bool B2_4 { get; set; }

        [DataMember]
        public bool B5_8 { get; set; }

        [DataMember]
        public bool BGS { get; set; }

        [DataMember]
        public bool B433 { get; set; }

        [DataMember]
        public bool B868 { get; set; }

        [DataMember]
        public bool B920 { get; set; }

        [DataMember]
        public bool BBPLA { get; set; }

        [DataMember]
        public bool BNAV { get; set; }


        [DataMember]
        public override int NumberASP { get; set; }

        [DataMember]
        public override int IdMission { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (ButtonsNAV)record;
            BIridium = newRecord.BIridium;
            BInmarsat = newRecord.BInmarsat;
            B2_4 = newRecord.B2_4;
            B5_8 = newRecord.B5_8;
            BGS = newRecord.BGS;
            B433 = newRecord.B433;
            B868 = newRecord.B868;
            B920 = newRecord.B920;
            BBPLA = newRecord.BBPLA;
            BNAV = newRecord.BNAV;
        }

        public ButtonsNAV Clone()
        {
            return new ButtonsNAV
            {
                Id = Id,
                BIridium = BIridium,
                BInmarsat = BInmarsat,
                B2_4 = B2_4,
                B5_8 = B5_8,
                BGS = BGS,
                B433 = B433,
                B868 = B868,
                B920 = B920,
                BBPLA = BBPLA,
                BNAV = BNAV,
                NumberASP = NumberASP,
                IdMission = IdMission,
        };
        }
    }
}
