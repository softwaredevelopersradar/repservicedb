﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Controls.WpfPropertyGrid;
using ModelsTablesDBLib.Interfaces;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Сектора и диапазоны 
    /// </summary>
    [DataContract]
    [CategoryOrder("Диапазон", 2)]
    [CategoryOrder("Сектор", 3)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(TableSectorsRangesRecon))]
    [KnownType(typeof(TableSectorsRangesSuppr))]
    public class TableSectorsRanges : AbstractDependentASP, IFixSectorRanges
    {
        [DataMember]
        [Category("ID")]
        [DisplayName("ID"), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("ID")]
        [PropertyOrder(1)]
        [DisplayName("№ АСП"), ReadOnly(true), Browsable(true)]
        public override int NumberASP { get; set; }  // адрес станции

        [DataMember]
        [Browsable(false)]
        public override int IdMission { get; set; }

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(2)]
        [DisplayName("F min, кГц")]
        [Browsable(true)]
        public double FreqMinKHz { get; set; }  // частота мин.

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(3)]
        [DisplayName("F max, кГц")]
        [Browsable(true)]
        public double FreqMaxKHz { get; set; }  // частота макс.
        
        [DataMember]
        [Category("Сектор")]
        [PropertyOrder(4)]
        [DisplayName("θ min,°")]
        public short AngleMin { get; set; } // угол мин.

        [DataMember]
        [Category("Сектор")]
        [PropertyOrder(5)]
        [DisplayName("θ max,°")]
        public short AngleMax { get; set; } // угол макс.

        [DataMember]
        [Category("Прочее")]
        [PropertyOrder(6)]
        [DisplayName("Примечание")]
        public string Note { get; set; } = string.Empty;// примечание 

        [DataMember]
        [DisplayName("Примечание"), Browsable(false)]
        public bool IsCheck { get; set; } // поле выбрано/не выбрано 
        
        public TableSectorsRanges Clone()
        {
            return new TableSectorsRanges
            {
                Id = this.Id,
                FreqMinKHz = this.FreqMinKHz,
                FreqMaxKHz = this.FreqMaxKHz,
                AngleMin = this.AngleMin,
                AngleMax = this.AngleMax,
                NumberASP = this.NumberASP,
                Note = this.Note,
                IsCheck = this.IsCheck
            };
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableSectorsRangesRecon ToRangesRecon()
        {
            TableSectorsRangesRecon table = new TableSectorsRangesRecon()
            {
                Id = this.Id,
                IdMission = this.IdMission,
                NumberASP = this.NumberASP,
                AngleMax = this.AngleMax,
                AngleMin = this.AngleMin,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsCheck = this.IsCheck
            };
            return table;
        }

        public TableSectorsRangesSuppr ToRangesSuppr()
        {
            TableSectorsRangesSuppr table = new TableSectorsRangesSuppr()
            {
                Id = this.Id,
                IdMission = this.IdMission,
                NumberASP = this.NumberASP,
                AngleMax = this.AngleMax,
                AngleMin = this.AngleMin,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsCheck = this.IsCheck
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            //IdMission = ((TableSectorsRanges)record).IdMission;
            //NumberASP = ((TableSectorsRanges)record).NumberASP;
            AngleMax = ((TableSectorsRanges)record).AngleMax;
            AngleMin = ((TableSectorsRanges)record).AngleMin;
            FreqMaxKHz = ((TableSectorsRanges)record).FreqMaxKHz;
            FreqMinKHz = ((TableSectorsRanges)record).FreqMinKHz;
            Note = ((TableSectorsRanges)record).Note;
            IsCheck = ((TableSectorsRanges)record).IsCheck;
        }
    }
}