﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Сектора и диапазоны РП
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableSectorsRanges))]
    [InfoTable(NameTable.TableSectorsRangesSuppr)]
    
    public class TableSectorsRangesSuppr : TableSectorsRanges
    {
    }

}
