﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Сектора и диапазоны РР
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableSectorsRanges))]
    [InfoTable(NameTable.TableSectorsRangesRecon)]
    
    public class TableSectorsRangesRecon : TableSectorsRanges
    {
    }
}
