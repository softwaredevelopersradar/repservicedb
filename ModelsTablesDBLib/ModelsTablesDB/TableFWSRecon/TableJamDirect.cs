﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(JamDirect))]
    
    public class TableJamDirect
    {
        [DataMember]
        
        public int ID { get; set; }
        
        [DataMember]
        
        public JamDirect JamDirect { get; set; }
    }
}
