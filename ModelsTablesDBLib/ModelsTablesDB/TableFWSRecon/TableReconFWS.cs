﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.ObjectModel;
using ModelsTablesDBLib.Interfaces;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Таблица ИРИ ФРЧ ЦР
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableJamDirect))]
    [KnownType(typeof(AbstractDependentMission))]
    [InfoTable(NameTable.TableReconFWS)]
    
    public class TableReconFWS : AbstractDependentMission, IFixReconFWS
    {
        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public override int IdMission { get; set; }

        [DataMember]
        
        public double FreqKHz { get; set; }       // частота

        [DataMember]
        
        public float Deviation { get; set; }      // ширина полосы

        [DataMember]
        
        public Coord Coordinates { get; set; }

        [DataMember]
        
        public ObservableCollection<TableJamDirect> ListJamDirect { get; set; }        

        [DataMember]
        
        public DateTime Time { get; set; }    // время

        [DataMember]
        
        public int ASPSuppr { get; set; }   // 

        [DataMember]
        
        public byte Type { get; set; }

        [DataMember]
        public SignSender? Sender { get; set; } // знак отправителя

        [DataMember]
        public ModulationKondor Modulation { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            ASPSuppr = ((TableReconFWS)record).ASPSuppr;
            Deviation = ((TableReconFWS)record).Deviation;
            Coordinates = ((TableReconFWS)record).Coordinates;
            FreqKHz = ((TableReconFWS)record).FreqKHz;
            Time = ((TableReconFWS)record).Time;
            Type = ((TableReconFWS)record).Type;
            Sender = ((TableReconFWS)record).Sender;
            Modulation = ((TableReconFWS)record).Modulation;
            ListJamDirect = ListJamDirect ?? ((TableReconFWS)record).ListJamDirect;
            if (ListJamDirect != ((TableReconFWS)record).ListJamDirect)
            {
                ListJamDirect.Clear();
                foreach (var jamDirect in ((TableReconFWS)record).ListJamDirect)
                    ListJamDirect.Add(jamDirect);
            }
        }
    }

}
