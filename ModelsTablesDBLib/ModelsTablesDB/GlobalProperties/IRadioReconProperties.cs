﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ModelsTablesDBLib
{
    public interface IRadioReconProperties: IMethodCompare<IRadioReconProperties> // РР
    {
        int NumberAveragingPhase { get; set; } //Кол-во усреднений (default ?)

        int NumberAveragingBearing { get; set; } //Кол-во усреднений (default ?)

        int HeadingAngle { get; set; } //Курсовой угол (default 0)

        byte DetectionFHSS { get; set; } //Обнаружение ППРЧ (default 0)

        bool SignHeadingAngle { get; set; } //Признак определения курсового угла (1 - компас) (default 1)

        EnumTypeRadioRecon TypeRadioRecon { get; set; } //Тип пеленгования        
    }
}
