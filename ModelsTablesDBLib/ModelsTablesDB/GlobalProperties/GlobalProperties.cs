﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(TableMission))]
    [InfoTable(NameTable.GlobalProperties)]
    #region CategoryOrder
    [CategoryOrder("Common", 1)]
    [CategoryOrder("RadioRecon", 2)]
    [CategoryOrder("FWS", 3)]
    [CategoryOrder("FAHI", 4)]
    [CategoryOrder("FHSS", 5)]
    [CategoryOrder("Spoofing", 6)]
    [CategoryOrder("UAV", 7)]
    #endregion
    public class GlobalProperties : AbstractCommonTable, INotifyPropertyChanged, IGlobalCommonProperties, IRadioReconProperties,
        IRadioSupprFAHIProperties, IRadioSupprFHSSProperties, IRadioSupprFWSProperties, ISpoofingProperties, IUAVProperties
    {

        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        #region Common

        private EnumRangeRadioRecon rangeRadioRecon = EnumRangeRadioRecon.Range_30_3000;
        private EnumRangeJamming rangeJamming = EnumRangeJamming.Range_30_1215;
        private int pingIntervalFHSAP;
        private EnumTypeRadioSuppr typeRadioSuppr;
        private short gnssInaccuracy;
        private EnumAmplifiers amplifiers = EnumAmplifiers.Standard;

        [DataMember]
        [Category(CommonCategory)]
        [PropertyOrder(1)]
        public EnumRangeRadioRecon RangeRadioRecon
        {
            get => rangeRadioRecon;
            set
            {
                if (rangeRadioRecon == value) return;
                rangeRadioRecon = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CommonCategory)]
        [PropertyOrder(2)]
        public EnumRangeJamming RangeJamming
        {
            get => rangeJamming;
            set
            {
                if (rangeJamming == value) return;
                rangeJamming = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CommonCategory)]
        [DisplayName("Ping interval FHSAP")]
        [Required]
        [Range(0, 18000)]
        [PropertyOrder(3)]
        public int PingIntervalFHSAP
        { get { return pingIntervalFHSAP; }
            set
            {
                if (pingIntervalFHSAP == value)
                    return;
                pingIntervalFHSAP = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CommonCategory)]
        [Browsable(false)]
        public EnumTypeRadioSuppr TypeRadioSuppr
        {
            get => typeRadioSuppr;
            set
            {
                if (typeRadioSuppr == value) return;
                typeRadioSuppr = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CommonCategory)]
        [PropertyOrder(4)]
        public short GnssInaccuracy
        {
            get => gnssInaccuracy;
            set
            {
                if (gnssInaccuracy == value) return;
                gnssInaccuracy = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CommonCategory)]
        [PropertyOrder(5)]
        public EnumAmplifiers Amplifiers
        {
            get => amplifiers;
            set
            {
                if (amplifiers == value) return;
                amplifiers = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region RadioRecon

        private int numberAveragingPhase;
        private int numberAveragingBearing;
        private int headingAngle;
        private byte detectionFHSS;
        private bool signHeadingAngle;
        private EnumTypeRadioRecon typeRadioRecon;


        [DataMember]
        [Category(RadioReconCategory)]
        [PropertyOrder(1)]
        [Required]
        [Range(1, 20)]
        public int NumberAveragingPhase
        {
            get { return numberAveragingPhase; }
            set
            {
                if (value == numberAveragingPhase)
                    return;
                numberAveragingPhase = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(RadioReconCategory)]
        [PropertyOrder(2)]
        [Required]
        [Range(1, 20)]
        public int NumberAveragingBearing
        {
            get { return numberAveragingBearing; }
            set
            {
                if (value == numberAveragingBearing)
                    return;
                numberAveragingBearing = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(RadioReconCategory)]
        [PropertyOrder(3)]
        [Required]
        [Range(0, 359)]
        public int HeadingAngle
        {
            get { return headingAngle; }
            set
            {
                if (headingAngle == value)
                    return;
                headingAngle = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(RadioReconCategory)]
        [Browsable(false)]
        public byte DetectionFHSS
        {
            get { return detectionFHSS; }
            set
            {
                if (detectionFHSS == value)
                    return;
                detectionFHSS = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(RadioReconCategory)]
        [PropertyOrder(4)]
        public bool SignHeadingAngle
        {
            get { return signHeadingAngle; }
            set
            {
                if (signHeadingAngle == value)
                    return;
                signHeadingAngle = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [Category(RadioReconCategory)]
        [Browsable(false)]
        public EnumTypeRadioRecon TypeRadioRecon
        {
            get => typeRadioRecon;
            set
            {
                if (typeRadioRecon == value) return;
                typeRadioRecon = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region FAHI  РП-AПРЧ
        private int sectorSearch;
        private int timeSearch;

        [DataMember]
        [Category(FAHICategory)]
        [Required]
        [Range(2, 360)]
        public int SectorSearch
        {
            get { return sectorSearch; }
            set
            {
                if (sectorSearch == value)
                    return;
                sectorSearch = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FAHICategory)]
        [Required]
        [Range(1, 18000)]
        public int TimeSearch
        { get { return timeSearch; }
            set
            {
                if (timeSearch == value)
                    return;
                timeSearch = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region FHSS РП-ППРЧ
        private byte fftRsolution;
        private int timeRadiateFHSS;

        [DataMember]
        [Category(FHSSCategory)]
        public byte FFTResolution
        { get { return fftRsolution; }
            set
            {
                if (fftRsolution == value)
                    return;
                fftRsolution = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FHSSCategory)]
        [Required]
        [Range(300, 5000)]
        public int TimeRadiateFHSS
        {
            get { return timeRadiateFHSS; }
            set
            {
                if (timeRadiateFHSS == value)
                    return;
                timeRadiateFHSS = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region FWS РП-ФРЧ
        private int operationTime;
        private int numberChannels;
        private int numberIri;
        private byte priority;
        private int threshold;
        private int timeRadiateFWS;

        [DataMember]
        [Category(FWSCategory)]
        [Required]
        [Range(1, 20)]
        public int OperationTime
        {
            get { return operationTime; }
            set
            {
                if (operationTime == value)
                    return;
                operationTime = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FWSCategory)]
        [Required]
        [Range(1, 4)]
        public int NumberChannels
        {
            get { return numberChannels; }
            set
            {
                if (numberChannels == value)
                    return;
                numberChannels = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FWSCategory)]
        [Required]
        [Range(1, 10)]
        public int NumberIri
        {
            get { return numberIri; }
            set
            {
                if (numberIri == value)
                    return;
                numberIri = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FWSCategory)]
        [Required]
        [Range(1, 9)]
        public byte Priority
        { get { return priority; }
            set
            {
                if (priority == value)
                    return;
                priority = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FWSCategory)]
        [Required]
        [Range(-130, 0)]
        public int Threshold
        {
            get { return threshold; }
            set
            {
                if (threshold == value)
                    return;
                threshold = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(FWSCategory)]
        [Required]
        [RegularExpression("([1][0-7][0-9][0-9][0-9]|18000|0)|[1-9][0-9][0-9][0-9]|[1-9][0-9][0-9]")]
        public int TimeRadiateFWS
        {
            get { return timeRadiateFWS; }
            set
            {
                if (timeRadiateFWS == value)
                    return;
                timeRadiateFWS = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Spoofing

        private double _latitude = -1;
        private double _longitude = -1;
        private bool spoofingVisibility;

        [DataMember]
        [Category(SpoofingCategory)]
        [Required]
        [Range(-90, 90)]
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }
        [DataMember]
        [Category(SpoofingCategory)]
        [Required]
        [Range (-180, 180)]
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        [Category(SpoofingCategory)]
        [YamlIgnore]
        public bool SpoofingVisibility { 
            get { return spoofingVisibility; }
            set
            {
                if (spoofingVisibility == value) return;
                spoofingVisibility = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region UAV БПЛА
        private int detectionAccuracyUAV;
        private int timeRadiateUAV;

        [DataMember]
        [Category(UAVCategory)]
        [Required]
        [Range(0, 100)]
        public int DetectionAccuracyUAV
        {
            get { return detectionAccuracyUAV; }
            set
            {
                if (detectionAccuracyUAV == value)
                    return;
                detectionAccuracyUAV = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(UAVCategory)]
        [Required]
        [Range(0, 60)]
        public int TimeRadiateUAV
        {
            get { return timeRadiateUAV; }
            set
            {
                if (timeRadiateUAV == value)
                    return;
                timeRadiateUAV = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        #region Compare

        public bool Compare(IGlobalCommonProperties data)
        {
            if (!((this as IGlobalCommonProperties).RangeJamming == data.RangeJamming
                && (this as IGlobalCommonProperties).RangeRadioRecon == data.RangeRadioRecon
                && (this as IGlobalCommonProperties).PingIntervalFHSAP == data.PingIntervalFHSAP 
                && (this as IGlobalCommonProperties).TypeRadioSuppr == data.TypeRadioSuppr
                && (this as IGlobalCommonProperties).GnssInaccuracy == data.GnssInaccuracy
                && (this as IGlobalCommonProperties).Amplifiers == data.Amplifiers

                ))
                return false;
            return true;
        }
        public bool Compare(IRadioReconProperties data)
        {
            if (!((this as IRadioReconProperties).DetectionFHSS == data.DetectionFHSS && (this as IRadioReconProperties).HeadingAngle == data.HeadingAngle
                && (this as IRadioReconProperties).NumberAveragingBearing == data.NumberAveragingBearing
                && (this as IRadioReconProperties).NumberAveragingPhase == data.NumberAveragingPhase
                && (this as IRadioReconProperties).SignHeadingAngle == data.SignHeadingAngle 
                && (this as IRadioReconProperties).TypeRadioRecon == data.TypeRadioRecon))
                return false;
            return true;
        }
        public bool Compare(IRadioSupprFAHIProperties data)
        {
            if (!((this as IRadioSupprFAHIProperties).SectorSearch == data.SectorSearch && (this as IRadioSupprFAHIProperties).TimeSearch == data.TimeSearch))
                return false;
            return true;
        }
        public bool Compare(IRadioSupprFHSSProperties data)
        {
            if (!((this as IRadioSupprFHSSProperties).FFTResolution == data.FFTResolution && (this as IRadioSupprFHSSProperties).TimeRadiateFHSS == data.TimeRadiateFHSS))
                return false;
            return true;
        }
        public bool Compare(IRadioSupprFWSProperties data)
        {
            if (!((this as IRadioSupprFWSProperties).NumberChannels == data.NumberChannels && (this as IRadioSupprFWSProperties).NumberIri == data.NumberIri
                && (this as IRadioSupprFWSProperties).OperationTime == data.OperationTime && (this as IRadioSupprFWSProperties).Priority == data.Priority
                && (this as IRadioSupprFWSProperties).Threshold == data.Threshold && (this as IRadioSupprFWSProperties).TimeRadiateFWS == data.TimeRadiateFWS))
                return false;
            return true;
        }

        public bool Compare(ISpoofingProperties data)
        {
            if (!((this as ISpoofingProperties).Latitude == data.Latitude && (this as ISpoofingProperties).Longitude == data.Longitude
                && (this as ISpoofingProperties).SpoofingVisibility == data.SpoofingVisibility ))
                return false;
            return true;
        }

        public bool Compare(IUAVProperties data)
        {
            if (!((this as IUAVProperties).DetectionAccuracyUAV == data.DetectionAccuracyUAV && (this as IUAVProperties).TimeRadiateUAV == data.TimeRadiateUAV))
                return false;
            return true;
        }
        #endregion

        public override void Update(AbstractCommonTable record)
        {

            RangeJamming = ((IGlobalCommonProperties)record).RangeJamming;
            RangeRadioRecon = ((IGlobalCommonProperties)record).RangeRadioRecon;
            PingIntervalFHSAP = ((IGlobalCommonProperties)record).PingIntervalFHSAP;
            TypeRadioSuppr = ((IGlobalCommonProperties)record).TypeRadioSuppr;
            GnssInaccuracy = ((IGlobalCommonProperties)record).GnssInaccuracy;
            Amplifiers = ((IGlobalCommonProperties)record).Amplifiers;

            SectorSearch = ((IRadioSupprFAHIProperties)record).SectorSearch;
            TimeSearch = ((IRadioSupprFAHIProperties)record).TimeSearch;

            FFTResolution = ((IRadioSupprFHSSProperties)record).FFTResolution;
            TimeRadiateFHSS = ((IRadioSupprFHSSProperties)record).TimeRadiateFHSS;

            NumberChannels = ((IRadioSupprFWSProperties)record).NumberChannels;
            NumberIri = ((IRadioSupprFWSProperties)record).NumberIri;
            OperationTime = ((IRadioSupprFWSProperties)record).OperationTime;
            Priority = ((IRadioSupprFWSProperties)record).Priority;
            Threshold = ((IRadioSupprFWSProperties)record).Threshold;
            TimeRadiateFWS = ((IRadioSupprFWSProperties)record).TimeRadiateFWS;

            NumberAveragingPhase = ((IRadioReconProperties)record).NumberAveragingPhase;
            NumberAveragingBearing = ((IRadioReconProperties)record).NumberAveragingBearing;
            HeadingAngle = ((IRadioReconProperties)record).HeadingAngle;
            DetectionFHSS = ((IRadioReconProperties)record).DetectionFHSS;
            SignHeadingAngle = ((IRadioReconProperties)record).SignHeadingAngle;
            TypeRadioRecon = ((IRadioReconProperties)record).TypeRadioRecon;

            Latitude = ((ISpoofingProperties)record).Latitude;
            Longitude = ((ISpoofingProperties)record).Longitude;
            SpoofingVisibility = ((ISpoofingProperties)record).SpoofingVisibility;

            DetectionAccuracyUAV = ((IUAVProperties)record).DetectionAccuracyUAV;
            TimeRadiateUAV = ((IUAVProperties)record).TimeRadiateUAV;
        }

        public GlobalProperties Clone()
        {
            return new GlobalProperties
            {
                Id = this.Id,
                RangeJamming = this.RangeJamming,
                RangeRadioRecon = this.RangeRadioRecon,
                PingIntervalFHSAP = this.PingIntervalFHSAP,
                TypeRadioSuppr = this.TypeRadioSuppr,
                GnssInaccuracy = this.GnssInaccuracy,
                Amplifiers = this.Amplifiers,

                SectorSearch = this.SectorSearch,
                TimeSearch = this.TimeSearch,

                FFTResolution = this.FFTResolution,
                TimeRadiateFHSS = this.TimeRadiateFHSS,

                NumberChannels = this.NumberChannels,
                NumberIri = this.NumberIri,
                OperationTime = this.OperationTime,
                Priority = this.Priority,
                Threshold = this.Threshold,
                TimeRadiateFWS = this.TimeRadiateFWS,

                NumberAveragingBearing = this.NumberAveragingBearing,
                NumberAveragingPhase = this.NumberAveragingPhase,
                HeadingAngle = this.HeadingAngle,
                DetectionFHSS = this.DetectionFHSS,
                SignHeadingAngle = this.SignHeadingAngle,
                TypeRadioRecon = this.TypeRadioRecon,

                Longitude = this.Longitude,
                Latitude = this.Latitude,
                SpoofingVisibility = this.SpoofingVisibility,

                DetectionAccuracyUAV = this.DetectionAccuracyUAV,
                TimeRadiateUAV = this.TimeRadiateUAV
            };

        }

        public bool Compare(GlobalProperties globalProperties)
        {
            if (!(this as IGlobalCommonProperties).Compare(globalProperties as IGlobalCommonProperties)) return false;
            if (!(this as IRadioSupprFAHIProperties).Compare(globalProperties as IRadioSupprFAHIProperties)) return false;
            if (!(this as IRadioSupprFHSSProperties).Compare(globalProperties as IRadioSupprFHSSProperties)) return false;
            if (!(this as IRadioSupprFWSProperties).Compare(globalProperties as IRadioSupprFWSProperties)) return false;
            if (!(this as IRadioReconProperties).Compare(globalProperties as IRadioReconProperties)) return false;
            if (!(this as ISpoofingProperties).Compare(globalProperties as ISpoofingProperties)) return false;
            if (!(this as IUAVProperties).Compare(globalProperties as IUAVProperties)) return false;
            return true;
        }

        #endregion

        #region CategotyName

        public const string CommonCategory = "Common";
        public const string RadioReconCategory = "RadioRecon";
        public const string FAHICategory = "FAHI";
        public const string FHSSCategory = "FHSS";
        public const string FWSCategory = "FWS";
        public const string SpoofingCategory = "Spoofing";
        public const string UAVCategory = "UAV";

        #endregion

        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}
