﻿
namespace ModelsTablesDBLib
{
    public interface IRadioSupprFWSProperties : IMethodCompare<IRadioSupprFWSProperties> //РП-ФРЧ
    {
        int OperationTime { get; set; } //Время работы ДЛРЛ, c (default 15)

        int NumberChannels { get; set; } //Кол-во каналов в литере (default 4)

        int NumberIri { get; set; } //Кол-во ИРИ в литере(default 10)

        byte Priority { get; set; } //Приоритет (default 1)

        int Threshold { get; set; } //Порог  (default -80)

        int TimeRadiateFWS { get; set; } //Время излучения, мс (default 800)
    }
}
