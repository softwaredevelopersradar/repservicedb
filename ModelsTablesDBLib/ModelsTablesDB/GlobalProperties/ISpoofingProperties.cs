﻿
namespace ModelsTablesDBLib
{
  public  interface ISpoofingProperties : IMethodCompare<ISpoofingProperties>
    {
        double Latitude { get; set; } 

        double Longitude { get; set; } 

        bool SpoofingVisibility { get; set; } 
    }
}
