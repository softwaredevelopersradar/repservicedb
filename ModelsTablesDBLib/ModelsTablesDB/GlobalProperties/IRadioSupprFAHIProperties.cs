﻿
namespace ModelsTablesDBLib
{
    public interface IRadioSupprFAHIProperties: IMethodCompare<IRadioSupprFAHIProperties> // РП-AПРЧ
    {
        int SectorSearch { get; set; } //Сектор поиска, град (default 10)

        int TimeSearch { get; set; } //Время поиска, мс (default 300)
    }
}
