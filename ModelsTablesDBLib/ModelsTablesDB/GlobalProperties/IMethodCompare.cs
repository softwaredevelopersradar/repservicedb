﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib
{
    public interface IMethodCompare<T>
    {
        bool Compare(T data);
    }
}
