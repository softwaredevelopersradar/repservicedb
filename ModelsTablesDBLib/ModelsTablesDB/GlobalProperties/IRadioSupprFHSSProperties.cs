﻿

namespace ModelsTablesDBLib
{
    public interface IRadioSupprFHSSProperties: IMethodCompare<IRadioSupprFHSSProperties> //РП-ППРЧ
    {
        byte FFTResolution { get; set; } //Разрешение БПФ (0 – 3052, 1 – 6104, 2 - 12208) (default 1)

        int TimeRadiateFHSS { get; set; } //Время излучения, мкс  (default 900)
    }
}
