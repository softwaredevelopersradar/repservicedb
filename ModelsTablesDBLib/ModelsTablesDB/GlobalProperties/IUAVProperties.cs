﻿

namespace ModelsTablesDBLib
{
    public interface IUAVProperties : IMethodCompare<IUAVProperties>
    {
        int DetectionAccuracyUAV { get; set; }
        int TimeRadiateUAV { get; set; }
    }
}
