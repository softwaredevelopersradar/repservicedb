﻿
namespace ModelsTablesDBLib
{
    public interface IGlobalCommonProperties : IMethodCompare<IGlobalCommonProperties> //Общие
    {

        EnumRangeJamming RangeJamming { get; set; }

        EnumRangeRadioRecon RangeRadioRecon { get; set; }

        int PingIntervalFHSAP { get; set; } //Интервал опроса состояния ФПС и УМб мс (default 5000)

        EnumTypeRadioSuppr TypeRadioSuppr { get; set; } // Тип РП

        short GnssInaccuracy { get; set; }

        EnumAmplifiers Amplifiers { get; set; }


    }

}
