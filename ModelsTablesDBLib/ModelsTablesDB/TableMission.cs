﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Обстановки
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableMission)]
    
    public class TableMission : AbstractCommonTable
    {

        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public string Name { get; set; } = string.Empty;

        [DataMember]
        
        public bool IsCurrent { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Name = ((TableMission)record).Name;
            IsCurrent = ((TableMission)record).IsCurrent;
        }
    }
}
