﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    public class DataBaseInfo
    {
        [DataContract]
        public enum NameTable : byte
        {
            [EnumMember]
            [Description("Обстановки")]
            TableMission,
            [EnumMember]
            [Description("Свои средства")]
            TableOwnResources,
            [EnumMember]
            [Description("Объекты противника")]
            TableEnemyResources,
            [EnumMember]
            [Description("Маршрут")]
            TableRoute,
            [EnumMember]
            [Description("Координаты посадки БПЛА")]
            TableCoordLandCoordUAV,    //(UAV - unmanned areial vehicle) Координаты посадки БПЛА
            [EnumMember]
            [Description("Линия фронта своих войск")]
            TableFrontLineOwnTroops,
            [EnumMember]
            [Description("Линия фронта войск противника")]
            TableFrontLineEnemyTroops,
            [EnumMember]
            [Description("Зона ответственности")]
            TableAreaResponsibility,
            [EnumMember]
            [Description("АСП")]
            TableASP,
            [EnumMember]
            [Description("Сектора и диапазоны РР")]
            TableSectorsRangesRecon,
            [EnumMember]
            [Description("Сектора и диапазоны РП")]
            TableSectorsRangesSuppr,
            [EnumMember]
            [Description("Запрещенные частоты")]
            TableFreqForbidden,
            [EnumMember]
            [Description("Важные частоты")]
            TableFreqImportant,
            [EnumMember]
            [Description("Известные частоты ")]
            TableFreqKnown,
            [EnumMember]
            [Description("ИРИ ФРЧ ЦР")]
            TableReconFWS,
            // [EnumMember]
            //ReconFHSS,               //Таблица ИРИ ППРЧ ЦР (РР)
            [EnumMember]
            [Description(" ИРИ ФРЧ РП")]
            TableSuppressFWS,          //Таблица ИРИ ФРЧ РП 
                                       // [EnumMember]
                                       // SuppressFHSS,            //Таблица ИРИ ППРЧ РП
                                       // [EnumMember]
                                       //Control,                 //Таблица ИРИ КРПУ  
                                       // [EnumMember]
                                       // AirObject,               //Таблица Воздушные объекты 
            [EnumMember]
            [Description("ИРИ ФРЧ")]
            TempFWS,

            [EnumMember]
            [Description("ADSB")]
            TempADSB,

            [EnumMember]
            [Description("РП-ППРЧ Настройки")]
            TableRadioSupprFHSSProperties,

            [EnumMember]
            [Description("РП-АПРЧ Насторйки")]
            TableRadioSupprFAHIProperties,

            [EnumMember]
            [Description("РП-ФРЧ Насторйки")]
            TableRadioSupprFWSProperties,

            [EnumMember]
            [Description("РР Насторйки")]
            TableRadioReconProperties,

            [EnumMember]
            [Description("Общие настройки")]
            TableCommonProperties
        }

        [DataContract]
        public enum NameOperationTable
        {
            [EnumMember]
            Add,
            [EnumMember]
            AddRange,
            [EnumMember]
            Change,
            [EnumMember]
            Delete,
            [EnumMember]
            Clear,
            [EnumMember]
            Load,
            [EnumMember]
            LoadByFilterAsp,
            [EnumMember]
            LoadByFilterRoute
        }

        [DataContract]
        public enum NameChangeOperation : byte
        {
            [EnumMember]
            Delete,   // Удалить запись
            [EnumMember]
            Add,      // Добавить запись 
            [EnumMember]
            AddRange, // Добавить диапазон
            [EnumMember]
            Change   // Изменить запись
        }

        [DataContract]
        public enum Error
        {
            [EnumMember]
            UnknownError,
            [EnumMember]
            RecordExist,
            [EnumMember]
            RecordNotFound,
            [EnumMember]
            SuchASPAbsent,
            [EnumMember]
            SuchMissionAbsent,
            [EnumMember]
            NoColumnAsp,
            [EnumMember]
            EmptyMission,
            [EnumMember]
            NotActiveMission,
            [EnumMember]
            None
        }

        [DataContract]
        public enum SignSpecFreq
        {
            [EnumMember]
            FreqForbidden,
            [EnumMember]
            FreqImportant,
            [EnumMember]
            TableFreqKnown
        }

        [DataContract]
        public enum SignSource
        {
            PC, //ПУ
            RadioRecevier, //КРПУ
            OtherTable,
            Panorama
        }

        [DataMember]
        private static readonly Dictionary<Error, string> DicError = new Dictionary<Error, string>
        {
            { Error.UnknownError, "Error: "},
            { Error.RecordExist, "Error: The table already has the similar record! "},
            { Error.SuchASPAbsent, "Error: There is no such station (ASP)"},
            { Error.SuchMissionAbsent, "Error: There is no such mission "},
            { Error.NoColumnAsp, "Error: The table hasn't column AddrAsp"},
            { Error.EmptyMission, "Error: The table of Mission is empty. Add some mission!"},
            { Error.NotActiveMission, "Error: The table of Mission hasn't any active mission."},
            { Error.RecordNotFound, "Error: The record not found! "},
            { Error.None, ""}
        };

        public static string GetErrorMessage(Error error)
        {
            try
            {
                return DicError[error];
            }
            catch(Exception)
            { return ""; }
        }

    }
}
