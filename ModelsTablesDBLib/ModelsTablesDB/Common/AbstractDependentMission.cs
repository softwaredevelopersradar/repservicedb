﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractDependentMission : AbstractCommonTable
    {
        [DataMember]
        public abstract int IdMission { get; set; }
    }
}
