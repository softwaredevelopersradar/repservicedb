﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;


namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(AbstractDependentFHSS))]
    [KnownType(typeof(TableSectorsRangesRecon))]
    [KnownType(typeof(TableSectorsRangesSuppr))]
    [KnownType(typeof(TableASP))]
    [KnownType(typeof(TableFreqForbidden))]
    [KnownType(typeof(TableFreqImportant))]
    [KnownType(typeof(TableFreqKnown))]
    [KnownType(typeof(TableReconFWS))]
    [KnownType(typeof(TableMission))]
    [KnownType(typeof(TableOwnResources))]
    [KnownType(typeof(TableEnemyResources))]
    [KnownType(typeof(TableLandCoordUAV))]
    [KnownType(typeof(TableFrontLineOwnTroops))]
    [KnownType(typeof(TableFrontLineEnemyTroops))]
    [KnownType(typeof(TableAreaResponsibility))]
    [KnownType(typeof(TableRoute))]
    [KnownType(typeof(GlobalProperties))]
    [KnownType(typeof(TempFWS))]
    [KnownType(typeof(TempADSB))]
    [KnownType(typeof(TableSuppressFWS))]
    [KnownType(typeof(TempSuppressFWS))]
    [KnownType(typeof(TableSuppressFHSS))]
    [KnownType(typeof(TableFHSSExcludedFreq))]
    [KnownType(typeof(TableReconFHSS))]
    [KnownType(typeof(TableSourceFHSS))]
    [KnownType(typeof(TableFHSSReconExcluded))]
    [KnownType(typeof(TempGNSS))]
    [KnownType(typeof(TempSuppressFHSS))]
    [KnownType(typeof(ButtonsNAV))]
    [KnownType(typeof(TableChatMessage))]

    public class ClassDataCommon
    {
        [DataMember]
        public List<AbstractCommonTable> ListRecords { get; set; }
        
        public ClassDataCommon()
        {
            ListRecords = new List<AbstractCommonTable>();
        }
        
        public List<T> ToList<T>() where T:class
        {
            return (from t in ListRecords let c = t as T select c).ToList();
        }

        public static ClassDataCommon ConvertToListAbstractCommonTable<T>(List<T> listRecords) where T : class
        {
            ClassDataCommon objListAbstractData = new ClassDataCommon();
            if (listRecords == null)
                return null;
            if (listRecords.Count == 0)
                return objListAbstractData;
            objListAbstractData.ListRecords = (from t in listRecords let c = t as AbstractCommonTable select c).ToList();
            return objListAbstractData;
        }
    }
}
