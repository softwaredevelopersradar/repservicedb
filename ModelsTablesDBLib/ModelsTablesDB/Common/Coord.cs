﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Координаты
    /// </summary>
    [DataContract]
    public class Coord : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private double latitude = -1;
        private double longitude = -1;
        private double altitude = -1;

        #endregion

        [DataMember]
        [Column("Latitude")]
        [DisplayName("Широта, м")]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;
                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        [DataMember]
        [Column("Longitude")]
        [DisplayName("Долгота, м")]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;
                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        [DataMember]
        [Column("Altitude")]
        [DisplayName("Высота, м")]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;
                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            } }

        public override string ToString()
        {
            if ((Latitude < 0 && Longitude < 0 && Altitude < 0) || (Latitude < 0 || Longitude < 0))
                return " ";
            if (Altitude < 0)
                return $"{latitude} : {longitude} : — ";
            return $"{latitude} : {longitude} : {altitude}";
        }
    }
}
