﻿using System.Runtime.Serialization;
namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractDependentFHSS : AbstractCommonTable
    {
        [DataMember]
        public abstract int IdFHSS { get; set; }
    }
}
