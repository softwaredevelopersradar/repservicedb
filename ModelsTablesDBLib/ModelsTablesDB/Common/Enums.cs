﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    public enum EnumCoordinateSystem:int
    {
        [EnumMember]
        WGS84,
        [EnumMember]
        CK42
    }

    [DataContract]
    public enum DeviceRSD : byte
    {
        [EnumMember]
        RRS_Lincked,
        [EnumMember]
        RRS_PC,
        [EnumMember]
        SSTU,
        [EnumMember]
        LPA_5_10,
        [EnumMember]
        LPA_5_7,
        [EnumMember]
        LPA_5_9,
        [EnumMember]
        LPA_10б
    }

    [DataContract]
    public enum TypeCRRX : byte
    {
        [EnumMember]
        [Description("AR-ONE")]
        AR_ONE = 1,
        [EnumMember]
        [Description("AR 6000")]
        AR_6000 = 2
    }

    [DataContract]
    public enum TypeRSD : byte
    {
        [EnumMember]
        Radant,
        [EnumMember]
        RSD_20
    }

    [DataContract]
    public enum TypePC : byte
    {
        [EnumMember]
        Garant,
        [EnumMember]
        Berezina,
        [EnumMember]
        Ross
    }

    [DataContract]
    public enum TypeOfConection : byte
    {
        [EnumMember]
        APD,
        [EnumMember]
        Tainet,
        [EnumMember]
        PPC,
        [EnumMember]
        _3G,
        [EnumMember]
        Router,
        [EnumMember]
        RadioModem
    }

    [DataContract]
    public enum TypeOfConectionAP_2 : byte
    {        
        [EnumMember]
        PPC,
        [EnumMember]
        _3G
    }

    [DataContract]
    public enum EnumRangeRadioRecon : byte
    {
        [EnumMember]
        [Description("30 - 3000")]
        Range_30_3000 = 0,
        [EnumMember]
        [Description("30 - 6000")]
        Range_30_6000 = 1,
        [EnumMember]
        [Description("0 - 6000")]
        Range_0_6000 = 2
    }

    [DataContract]
    public enum EnumRangeJamming : byte
    {
        [EnumMember]
        [Description("30 - 1215")]
        Range_30_1215 = 7,
        [EnumMember]
        [Description("30 - 3000")]
        Range_30_3000 = 9,
        [EnumMember]
        [Description("30 - 6000")]
        Range_30_6000 = 10,
        [EnumMember]
        [Description("0 - 6000")]
        Range_0_6000 = 11,
    }


    [DataContract]
    public enum EnumTypeRadioRecon: byte
    {
        [EnumMember]
        [Description("Без пеленгования")]
        WithoutBearing,// = 1,
        [EnumMember]
        [Description("С пеленгованием")]
        Bearing// = 2
    }

    [DataContract]
    public enum EnumTypeRadioSuppr:byte
    {
        [EnumMember]
        FWS,// = 0, // ФРЧ
        [EnumMember]
        FAHI,// = 1, //АПРЧ
        [EnumMember]
        Voice,// = 2 //Голос
        [EnumMember]
        FHSS// = 3, //ППРЧ
    }
    [DataContract]
    public enum NameChangeOperation : byte
    {
        [EnumMember]
        Add,      // Добавить запись 
        [EnumMember]
        Delete,   // Удалить запись
        [EnumMember]
        Change   // Изменить запись
    }

    [DataContract]
    public enum EnumFFTResolution:byte
    {
        [EnumMember]
        [Description("3052")]
        Fft_3052,
        [EnumMember]
        [Description("6104")]
        Fft_6104,
        [EnumMember]
        [Description("12208")]
        Fft_12208
    }

    [DataContract]
    public enum NameTableOperation
    {
        [EnumMember]
        Add,
        [EnumMember]
        AddRange,
        [EnumMember]
        RemoveRange,
        [EnumMember]
        Change,
        [EnumMember]
        Delete,
        [EnumMember]
        Clear,
        [EnumMember]
        Load,
        [EnumMember]
        LoadByFilterAsp,
        [EnumMember]
        LoadByFilterRoute,
        [EnumMember]
        ClearByFilter,
        [EnumMember]
        LoadSettings,
        [EnumMember]
        Update,
        [EnumMember]
        None
    }

    [DataContract]
    public enum NameTable : byte
    {
        [EnumMember]
        [Description("Обстановки")]
        TableMission,

        [EnumMember]
        [Description("Свои средства")]
        TableOwnResources,

        [EnumMember]
        [Description("Объекты противника")]
        TableEnemyResources,

        [EnumMember]
        [Description("Маршрут")]
        TableRoute,

        [EnumMember]
        [Description("Координаты посадки БПЛА")]
        TableLandCoordUAV,    //(UAV - unmanned areial vehicle) Координаты посадки БПЛА

        [EnumMember]
        [Description("Линия фронта своих войск")]
        TableFrontLineOwnTroops,

        [EnumMember]
        [Description("Линия фронта войск противника")]
        TableFrontLineEnemyTroops,

        [EnumMember]
        [Description("Зона ответственности")]
        TableAreaResponsibility,

        [EnumMember]
        [Description("АСП")]
        TableASP,

        [EnumMember]
        [Description("Сектора и диапазоны РР")]
        TableSectorsRangesRecon,

        [EnumMember]
        [Description("Сектора и диапазоны РП")]
        TableSectorsRangesSuppr,

        [EnumMember]
        [Description("Запрещенные частоты")]
        TableFreqForbidden,

        [EnumMember]
        [Description("Важные частоты")]
        TableFreqImportant,

        [EnumMember]
        [Description("Известные частоты ")]
        TableFreqKnown,

        [EnumMember]
        [Description("ИРИ ФРЧ ЦР")]
        TableReconFWS,

        [EnumMember]
        [Description(" ИРИ ППРЧ ЦР ")]
        TableReconFHSS,               //Таблица ИРИ ППРЧ ЦР (РР)

        [EnumMember]
        [Description(" Источники ППРЧ")]
        TableSourceFHSS, //Таблица с источниками ППРЧ

        [EnumMember]
        [Description("Исключенные частоты ИРИ ППРЧ ЦР")]
        TableFHSSReconExcluded,

        [EnumMember]
        [Description(" ИРИ ФРЧ РП")]
        TableSuppressFWS,          //Таблица ИРИ ФРЧ РП 

        [EnumMember]
        [Description("Исключенные частоты")]
        TableFHSSExcludedFreq,

        [EnumMember]
        TempSuppressFWS,

        [EnumMember]
        TableSuppressFHSS,            //Таблица ИРИ ППРЧ РП

        // [EnumMember]
        //Control,                 //Таблица ИРИ КРПУ  

        // [EnumMember]
        // AirObject,               //Таблица Воздушные объекты 

        [EnumMember]
        [Description("ИРИ ФРЧ")]
        TempFWS,

        [EnumMember]
        [Description("ADSB")]
        TempADSB,

        [EnumMember]
        [Description("GlobalProperties")]
        GlobalProperties,

        [EnumMember]
        [Description("TempGNSS")]
        TempGNSS,

        [EnumMember]
        [Description("TempSuppressFHSS")]
        TempSuppressFHSS,

        [EnumMember]
        [Description("ButtonsNAV")]
        ButtonsNAV,

        [EnumMember]
        [Description("TableChat")]
        TableChat

    }

    [DataContract]
    public enum SignSpecFreq
    {
        [EnumMember]
        FreqForbidden,
        [EnumMember]
        FreqImportant,
        [EnumMember]
        TableFreqKnown
    }

    [DataContract]
    public enum SignSender: byte
    {
        [EnumMember]
        Hand, 
        [EnumMember]
        PC, //ПУ
        [EnumMember]
        RadioRecevier, //КРПУ
        [EnumMember]
        OtherTable,
        [EnumMember]
        Panorama,
        [EnumMember]
        UAV_433,
        [EnumMember]
        UAV_860,
        [EnumMember]
        UAV_920,
        [EnumMember]
        UAV_2400,
        [EnumMember]
        UAV_5800,
        [EnumMember]
        NAV_L1,
        [EnumMember]
        NAV_L2,
        [EnumMember]
        IR,
        [EnumMember]
        IN,
        [EnumMember]
        GS,
        //[EnumMember]
        //Button,
        [EnumMember]
        Cicada,
        [EnumMember]
        UAV_5200,
    }

    [DataContract]
    public enum Led : byte
    {
        [EnumMember]
        Empty,
        [EnumMember]
        Green,
        [EnumMember]
        Red,
        [EnumMember]
        Blue,
        [EnumMember]
        Yellow,
        [EnumMember]
        Gray,
        [EnumMember]
        White
    }

    [DataContract]
    public enum RoleStation : byte
    {
        [EnumMember]
        [Description("Автономная")]
        Autonomic = 0,
        [EnumMember]
        [Description("Ведущая")]
        Master = 1,
        [EnumMember]
        [Description("Ведомая")]
        Slave = 2
    }

    [DataContract]
    public enum SpeedPorts : int
    {
        [Description("2400")]
        Speed_2400 = 2400,
        [Description("4800")]
        Speed_4800 = 4800,
        [Description("9600")]
        Speed_9600 = 9600,
        [Description("19200")]
        Speed_19200 = 19200,
        [Description("38400")]
        Speed_38400 = 38400,
        [Description("57600")]
        Speed_57600 = 57600,
        [Description("115200")]
        Speed_115200 = 115200
    }

    public enum Languages : byte
    {
        [Description("Русский")]
        Rus,
        [Description("English")]
        Eng,
        [Description("Azerbacyan")]
        Azr
    }

    [DataContract]
    public enum TypeConnection : byte
    {
        [EnumMember]
        Tainet,
        [EnumMember]
        [Description("RRS / Radiomodem")]
        RRS,
        [EnumMember]
        [Description("3G / 4G")]
        Type3G
    }

    [DataContract]
    public enum TypeAccessARM : byte
    {
        [EnumMember]
        Operator,
        [EnumMember]
        Commander,
        [EnumMember]
        Admin
    }


    [DataContract]
    public enum FileType: byte
    {
        [EnumMember]
        Txt,
        [EnumMember]
        Word,
        [EnumMember]
        Excel
    }

    [DataContract]
    public enum EnumAmplifiers : byte
    {
        [EnumMember]
        Standard,
        [EnumMember]
        ModificationUganda
    }

    [DataContract]
    public enum EnumFrequencyUnits : byte
    {
        [EnumMember]
        kHz = 0,
        [EnumMember]
        MHz = 1
    }

    [DataContract]
    public enum ModulationKondor : byte
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        AM,
        [EnumMember]
        FM,
        [EnumMember]
        WFM,
        [EnumMember]
        USB,
        [EnumMember]
        LSB,
        [EnumMember]
        DMR,
        [EnumMember]
        APCO_P25,
        [EnumMember]
        TETRA
    }

    [DataContract]
    public enum ChatMessageStatus : byte
    {
        [EnumMember]
        Empty = 0, 
        [EnumMember]
        Error = 1,
        [EnumMember]
        Sent = 2,
        [EnumMember]
        Delivered = 3
    }
}