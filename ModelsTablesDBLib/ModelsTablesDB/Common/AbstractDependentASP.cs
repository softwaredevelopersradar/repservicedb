﻿using System;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractDependentASP : AbstractDependentMission
    {
        [DataMember]
        public abstract int NumberASP { get; set; }
    }
}
