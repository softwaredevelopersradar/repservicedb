﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(AbstractDependentMission))]
    [KnownType(typeof(TableASP))]
    [KnownType(typeof(TableRoute))]
    [KnownType(typeof(TableReconFWS))]
    [KnownType(typeof(TableOwnResources))]
    [KnownType(typeof(TableEnemyResources))]
    [KnownType(typeof(TableLandCoordUAV))]
    [KnownType(typeof(TableFrontLineOwnTroops))]
    [KnownType(typeof(TableFrontLineEnemyTroops))]
    [KnownType(typeof(TableAreaResponsibility))]
    [KnownType(typeof(TempFWS))]
    [KnownType(typeof(TempADSB))]
    [KnownType(typeof(TableSuppressFHSS))]
    [KnownType(typeof(TableReconFHSS))]
    [KnownType(typeof(TempGNSS))]
    [KnownType(typeof(TempSuppressFHSS))]
    public class ClassDataDependMission
    {
        [DataMember]
        public List<AbstractDependentMission> ListRecords { get; set; }


        public ClassDataDependMission()
        {
            ListRecords = new List<AbstractDependentMission>();
        }

        public List<T> ToList<T>() where T : AbstractDependentMission
        {
            return (from t in ListRecords let c = t as T select c).ToList();
        }

        public static ClassDataDependMission ConvertToDataDependMission<T>(List<T> listRecord) where T : class
        {
            ClassDataDependMission dataDependMission = new ClassDataDependMission();
            if (listRecord == null)
                return null;
            if (listRecord.Count == 0)
                return dataDependMission;
            dataDependMission.ListRecords = (from t in listRecord let c = t as AbstractDependentMission select c).ToList();
            return dataDependMission;
        }
    }
}
