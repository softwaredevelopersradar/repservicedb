﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Соктора 
    /// </summary>
    [DataContract]
    public class Sectors : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private short lpa13 = 120;
        private short lpa24 = 120;
        private short lpa510 = 120;
        private short rrs1 = 7;
        private short rrs2 = 7;

        #endregion

        #region Properties

        [DataMember]
        [NotifyParentProperty(true)]
        public short LPA13
        {
            get => lpa13;
            set
            {
                if (lpa13 == value)
                    return;
                lpa13 = value;
                OnPropertyChanged(nameof(LPA13));
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short LPA24
        {
            get => lpa24;
            set
            {
                if (lpa24 == value)
                    return;
                lpa24 = value;
                OnPropertyChanged(nameof(LPA24));
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short LPA510
        {
            get => lpa510;
            set
            {
                if (lpa510 == value)
                    return;
                lpa510 = value;
                OnPropertyChanged(nameof(LPA510));
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short RRS1
        {
            get => rrs1;
            set
            {
                if (rrs1 == value)
                    return;
                rrs1 = value;
                OnPropertyChanged(nameof(RRS1));
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public short RRS2
        {
            get => rrs2;
            set
            {
                if (rrs2 == value)
                    return;
                rrs2 = value;
                OnPropertyChanged(nameof(RRS2));
            }
        }

        #endregion

        public Sectors Clone()
        {
            return new Sectors()
            {
                LPA13 = lpa13,
                LPA24 = lpa24,
                LPA510 = lpa510,
                RRS1 = rrs1,
                RRS2 = rrs2
            };
        }

        public void Update(Sectors sectors)
        {
            LPA13 = sectors.LPA13;
            LPA24 = sectors.LPA24;
            LPA510 = sectors.LPA510;
            RRS1 = sectors.RRS1;
            RRS2 = sectors.RRS2;
        }
    }
}