﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(TempADSB))]
    [KnownType(typeof(TempFWS))]
    public abstract class AbstractCommonTable
    {
        [DataMember]
        public abstract int Id { get; set; }
        
        public abstract object[] GetKey();

        public abstract void Update(AbstractCommonTable record);
    }
}
