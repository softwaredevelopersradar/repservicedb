﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Направление станции
    /// </summary>
    [DataContract]
    public class JamDirect
    {
        [DataMember]
        [Column(nameof(NumberASP))]
        public int NumberASP { get; set; }

        [DataMember]
        [Column(nameof(Bearing))]
        public float Bearing { get; set; }

        [DataMember]
        [Column(nameof(Level))]
        public short Level { get; set; }

        [DataMember]
        [Column(nameof(DistanceKM))]
        public float DistanceKM { get; set; }

        [DataMember]
        [Column(nameof(Std))]
        public float Std { get; set; }

        [DataMember]
        [Column(nameof(IsOwn))]
        public bool IsOwn { get; set; }
    }
}
