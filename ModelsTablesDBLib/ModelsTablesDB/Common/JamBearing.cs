﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Пеленг АСП (класс, который содержит данные  для определения координат источника)
    /// </summary>
    [DataContract]
    public class JamBearing
    {
        [DataMember]
        public Coord Coordinate { get; set; }

        [DataMember]
        public float Bearing { get; set; }

    }
}
