﻿using System;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Сообщения в чате
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableChat)]
    public class TableChatMessage : AbstractCommonTable
    {

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public int SenderAddress { get; set; }

        [DataMember]
        public int ReceiverAddress { get; set; }

        [DataMember]
        public byte AwsNumber { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public string Text { get; set; } = string.Empty;

        [DataMember]
        public ChatMessageStatus Status { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            var rec = (TableChatMessage)record;
            SenderAddress = rec.SenderAddress;
            ReceiverAddress = rec.ReceiverAddress;
            AwsNumber = rec.AwsNumber;
            Time = rec.Time;
            Text = rec.Text;
            Status = rec.Status;
        }
    }
}
