﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Свои средства
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableOwnResources)]
    [KnownType(typeof(AbstractDependentMission))]
    public class TableOwnResources : AbstractDependentMission
    {
        [DataMember]
        public override int IdMission { get; set; }

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public string Name { get; set; } = string.Empty;

        [DataMember]
        public Coord Coordinates { get; set; }

        [DataMember]
        public string Caption { get; set; } = string.Empty;

        [DataMember]
        public string Image { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id }; //IdMission,
        }

        public override void Update(AbstractCommonTable record)
        {
            Caption = ((TableOwnResources)record).Caption;
            Coordinates = ((TableOwnResources)record).Coordinates;
            Image = ((TableOwnResources)record).Image;
            Name = ((TableOwnResources)record).Name;
        }
    }
}