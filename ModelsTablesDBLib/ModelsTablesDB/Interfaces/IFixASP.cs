﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib.Interfaces
{
    public interface IFixASP
    {
        int Id { get; set; }
        
        int IdMission { get; set; }
        
        byte Mode { get; set; }

        byte[] Letters { get; set; }

        short RRS1 { get; set; }

        short RRS2 { get; set; }

        short LPA13 { get; set; }

        short LPA24 { get; set; }

        short LPA510 { get; set; }

        short BPSS { get; set; }

        short LPA57 { get; set; }

        short LPA59 { get; set; }

        short LPA10 { get; set; }

        Coord Coordinates { get; set; }
    }
}
