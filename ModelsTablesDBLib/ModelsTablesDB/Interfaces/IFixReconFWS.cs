﻿using System;
using System.Collections.ObjectModel;

namespace ModelsTablesDBLib.Interfaces
{
    public interface IFixReconFWS
    {
        int Id { get; set; }

        double FreqKHz { get; set; }       // частота

        float Deviation { get; set; }      // ширина полосы

        Coord Coordinates { get; set; }

        ObservableCollection<TableJamDirect> ListJamDirect { get; set; }

        DateTime Time { get; set; }    // время

        byte Type { get; set; }

        public SignSender? Sender { get; set; } // знак отправителя

        public ModulationKondor Modulation { get; set; }
    }
}
