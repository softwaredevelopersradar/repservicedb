﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib.Interfaces
{
    public interface IFixSectorRanges
    {
        int Id { get; set; }

        double FreqMinKHz { get; set; }  // частота мин.

        double FreqMaxKHz { get; set; }  // частота макс.

        short AngleMin { get; set; } // угол мин.

        short AngleMax { get; set; } // угол макс.
    }
}
