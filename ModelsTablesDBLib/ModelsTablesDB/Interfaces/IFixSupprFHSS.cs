﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib.Interfaces
{
    public interface IFixSupprFHSS
    {
        int Id { get; set; }

        double FreqMinKHz { get; set; }

        double FreqMaxKHz { get; set; }

        double Threshold { get; set; }

        short StepKHz { get; set; }

        InterferenceParam InterferenceParam { get; set; }

        byte[] Letters { get; set; }

        byte[] EPO { get; set; }
    }
}
