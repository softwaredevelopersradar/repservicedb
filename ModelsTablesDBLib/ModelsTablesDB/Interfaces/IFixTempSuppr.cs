﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib.Interfaces
{
    public interface IFixTempSuppr
    {
        int Id { get; set; }

        Led Control { get; set; }

        Led Suppress { get; set; }

        Led Radiation { get; set; }
    }
}
