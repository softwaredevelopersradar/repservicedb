﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib.Interfaces
{
    public interface IFixSupprFWS
    {
        int Id { get; set; } // уникальный номер записи

        Coord Coordinates { get; set; }

        double? FreqKHz { get; set; } // частота

        float? Bearing { get; set; } //пеленг

        byte? Letter { get; set; } // Литера

        short? Threshold { get; set; } // порог

        byte? Priority { get; set; }

        InterferenceParam InterferenceParam { get; set; }

    }
}
