﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    ///  Координаты посадки БПЛА
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableLandCoordUAV)]
    [KnownType(typeof(AbstractDependentMission))]
    
    public class TableLandCoordUAV : AbstractDependentMission
    {
        [DataMember]
        
        public override int IdMission { get; set; }

        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public string Name { get; set; } = string.Empty;

        [DataMember]
        
        public Coord Coordinates { get; set; }

        [DataMember]
        
        public string Caption { get; set; } = string.Empty;

        [DataMember]
        
        public string Image { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id }; // IdMission, 
        }

        public override void Update(AbstractCommonTable record)
        {
            Caption = ((TableLandCoordUAV)record).Caption;
            Coordinates = ((TableLandCoordUAV)record).Coordinates;
            Image = ((TableLandCoordUAV)record).Image;
            Name = ((TableLandCoordUAV)record).Name;
        }
    }
}
