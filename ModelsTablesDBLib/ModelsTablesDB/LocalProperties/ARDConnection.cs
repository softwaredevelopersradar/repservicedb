﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class ARDConnection : INotifyPropertyChanged, IMethod<ARDConnection>, IDataErrorInfo
    {

        public ARDConnection()
        {
            comPort = "COM1";
            PortSpeed = 115200;
            state = false;
            type = TypeRSD.Radant;
            device = DeviceRSD.LPA_5_10;
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string comPort;
        private TypeRSD type;
        private DeviceRSD device;
        private byte level;
        private bool state;
        private int address;
        private int portSpeed;
        private bool isVisible = true;
        #endregion

        #region Properties

        [NotifyParentProperty(true)]
        public TypeRSD Type
        {
            get => type;
            set
            {
                if (type == value) return;
                type = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public DeviceRSD Device
        {
            get => device;
            set
            {
                if (device == value) return;
                device = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public byte Level 
        {
            get { return level; }
            set
            {
                if (level == value)
                    return;
                level = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => comPort;
            set
            {
                if (comPort == value) return;
                comPort = value;
                OnPropertyChanged();
            }
        }
      
        [NotifyParentProperty(true)]
        public int PortSpeed
        {
            get => portSpeed;
            set
            {
                if (portSpeed == value) return;
                portSpeed = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public  bool State //Состояние (вкл/выкл)
        {
            get { return state; }
            set
            {
                if (state == value)
                    return;
                state = value;
                OnPropertyChanged();
            }
        }
        
        [NotifyParentProperty(true)]
        [Required]
        [Range(1, 12)]
        public int Address
        {
            get { return address; }
            set
            {
                if (address == value)
                    return;
                address = value;
                OnPropertyChanged();
            }
        }


        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region IMethod

        public ARDConnection Clone()
        {
            return new ARDConnection()
            {
                Address = this.address,
                Type = this.Type,
                ComPort = this.comPort,
                PortSpeed = this.portSpeed,
                State = this.State,
                Device = this.Device,
                Level = this.Level,
                IsVisible = this.IsVisible
            };
        }

        public void Update(ARDConnection ardConnection)
        {
            Address = ardConnection.Address;
            Type = ardConnection.Type;
            ComPort = ardConnection.comPort;
            PortSpeed = ardConnection.portSpeed;
            State = ardConnection.State;
            Device = ardConnection.Device;
            Level = ardConnection.Level;
            IsVisible = ardConnection.IsVisible;
        }

        public bool Compare(ARDConnection data)
        {
            if (comPort != data.ComPort || state != data.State || PortSpeed != data.PortSpeed || address != data.Address || type != data.Type || device != data.Device|| level != data.Level || IsVisible != data.IsVisible)
                return false;
            return true;
        }

        #endregion

        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion

    }
}
