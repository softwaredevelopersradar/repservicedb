﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
   public class VoiceInterference : INotifyPropertyChanged, IMethod<VoiceInterference>, IDataErrorInfo
    {
        #region Private
        private bool state;
        private bool isVisible = true;

        #endregion
       public VoiceInterference() { }
        #region Properties

        [NotifyParentProperty(true)]
        public bool State
        {
            get { return state; }
            set
            {
                if (state == value)
                    return;
                state = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        public bool Compare(VoiceInterference data)
        {
            if ( state != data.state || isVisible != data.isVisible)
                return false;
            return true;
        }

        public void Update(VoiceInterference data)
        {
            State = data.State;
            IsVisible = data.IsVisible;
        }

        public new VoiceInterference Clone()
        {
            return new VoiceInterference
            {           
                State = State,
                IsVisible = IsVisible
            };
        }
        #endregion

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion


    }
}