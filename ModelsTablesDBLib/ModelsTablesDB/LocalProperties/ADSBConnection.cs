﻿using System.ComponentModel;

namespace ModelsTablesDBLib
{
    public class ADSBConnection : EndPointConnection, INotifyPropertyChanged, IMethod<ADSBConnection>
    {
        #region Private
        private bool state;
        #endregion

        #region Property
        [NotifyParentProperty(true)]
        public bool State
        {
            get => state;
            set
            {
                if (state == value) return;
                state = value;
                OnPropertyChanged();
            }
        }
        #endregion

        public bool Compare(ADSBConnection data)
        {
            if (!Compare(data as EndPointConnection) || state != data.state)
                return false;
            return true;
        }

        public void Update(ADSBConnection data)
        {
            Update(data as EndPointConnection);
            State = data.State;
        }

        public new ADSBConnection Clone()
        {
            return new ADSBConnection
            {
                IpAddress = IpAddress,
                Port = Port,
                State = State
            };
        }

        public ADSBConnection():base()
        {
            IpAddress = "192.168.0.11";
            Port = 30005;
        }
    }
}
