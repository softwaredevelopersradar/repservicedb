﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class EndPointConnection : INotifyPropertyChanged, IMethod<EndPointConnection>, IDataErrorInfo
    {

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string ipAdress;// = "127.0.0.1";
        private int port;
        
        #endregion

        #region Properties
       
        public EndPointConnection()
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            port = 1;
        }

        [NotifyParentProperty(true)]
        [DisplayName("IP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage ="IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get { return ipAdress; }
            set
            {
                if (ipAdress == value)
                    return;
                ipAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1,1000000)]
        public int Port
        {
            get { return port; }
            set
            {
                if (port == value)
                    return;
                port = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        public EndPointConnection Clone()
        {
            return new EndPointConnection()
            {
                IpAddress = this.ipAdress,
                Port = this.port
            };
        }

        public void Update(EndPointConnection endPoint)
        {
            IpAddress = endPoint.IpAddress;
            Port = endPoint.Port;
        }

        public bool Compare(EndPointConnection data)
        {
            if (ipAdress != data.IpAddress || port != data.Port)
                return false;
            return true;
        }

        #endregion

        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion


    }
}
