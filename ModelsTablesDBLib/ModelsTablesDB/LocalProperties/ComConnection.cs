﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ModelsTablesDBLib
{
    public class ComConnection :  INotifyPropertyChanged, IMethod<ComConnection>
    {
        public ComConnection()
        {
            comPort = "COM1";
            PortSpeed = 115200;
            state = false;
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private int  portSpeed;
        private string comPort;
        private bool state;
        private bool isVisible = true;

        #endregion

        #region Properties
        [NotifyParentProperty(true)]
        public string ComPort
        {
            get =>comPort;
            set
            {
                if (comPort == value) return;
                comPort = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public int PortSpeed
        {
            get =>portSpeed;
            set
            {
                if (portSpeed == value) return;
                portSpeed = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool State
        {
            get { return state; }
            set
            {
                if (state == value)
                    return;
                state = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        public ComConnection Clone()
        {
            return new ComConnection()
            {
                ComPort = this.comPort,
                PortSpeed = this.portSpeed,
                State = this.state,
                IsVisible = this.IsVisible
            };
        }

        public void Update(ComConnection comConnection)
        {
            ComPort = comConnection.ComPort;
            PortSpeed = comConnection.PortSpeed;
            State = comConnection.State;
            IsVisible = comConnection.IsVisible;
        }

        public bool Compare(ComConnection data)
        {
            //if (numComPort != data.NumberComPort || state != data.State || PortSpeed != data.PortSpeed)
            if (comPort != data.ComPort || state != data.State || PortSpeed != data.PortSpeed || IsVisible != data.IsVisible )
                return false;
            return true;
        }

        #endregion

    }
}
