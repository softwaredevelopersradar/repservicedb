﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib
{
   public class ODProperties : ADSBConnection, INotifyPropertyChanged, IMethod<ODProperties>
    {
       private bool isVisible = true;
       public ODProperties():base()
        {
            IpAddress = "127.0.0.1";
            Port = 1;
        }

        public bool Compare(ODProperties data)
        {
            if (!Compare(data as ADSBConnection) || isVisible != data.isVisible)
                return false;
            return true;
        }

        public void Update(ODProperties data)
        {
            Update(data as ADSBConnection);
            IsVisible = data.IsVisible;
        }

        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }


        public new ODProperties Clone()
        {
            return new ODProperties
            {
                IpAddress = IpAddress,
                Port = Port,
                State = State
            };
        }
    }
}
