﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class Spoofing : EndPointConnection, INotifyPropertyChanged, IMethod<Spoofing>
    {

        #region Private
        private bool state;
        private bool isVisible = true;

        #endregion

        #region Properties

        [NotifyParentProperty(true)]
        public bool State
        {
            get { return state; }
            set
            {
                if (state == value)
                    return;
                state = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region IMethod

        public bool Compare(Spoofing data)
        {
            if (!Compare(data as EndPointConnection) || state != data.state 
                || isVisible != data.isVisible)
                return false;
            return true;
        }

        public void Update(Spoofing data)
        {
            Update(data as EndPointConnection);
            State = data.State;
            IsVisible = data.IsVisible;
        }

        public new Spoofing Clone()
        {
            return new Spoofing
            {
                IpAddress = IpAddress,
                Port = Port,
                State = State,
                IsVisible = IsVisible
            };
        }

        public Spoofing() : base()
        {
            IpAddress = "127.0.0.1";
            Port = 80;
        }
        #endregion
    }
}