﻿using System.ComponentModel;

namespace ModelsTablesDBLib
{
    public class ARONEProperties : ComConnection, INotifyPropertyChanged, IMethod<ARONEProperties>
    {
        public ARONEProperties()
        {
            currentBank = 1;
            type = TypeCRRX.AR_6000;
        }
        #region Private

        private string path;
        private int currentBank;
        private TypeCRRX type;
        #endregion

        #region Properties

        [NotifyParentProperty(true)]
        public string PathToDataBank
        {
            get => path;
            set
            {
                if (path == value) return;
                path = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public int CurrentDataBank
        {
            get => currentBank;
            set
            {
                if (currentBank == value) return;
                currentBank = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public TypeCRRX Type
        {
            get => type;
            set
            {
                if (type == value) return;
                type = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region IMethod
        public bool Compare(ARONEProperties data)
        {
            if (!Compare(data as ComConnection) || path != data.path || currentBank != data.currentBank || type != data.Type )
                return false;
            return true;
        }

        public void Update(ARONEProperties data)
        {
            Update(data as ComConnection);
            PathToDataBank = data.PathToDataBank;
            CurrentDataBank = data.CurrentDataBank;
            Type = data.Type;
        }

        public new ARONEProperties Clone()
        {
            return new ARONEProperties
            {
                ComPort = this.ComPort,
                PortSpeed = this.PortSpeed,
                State = this.State,
                PathToDataBank = this.PathToDataBank,
                CurrentDataBank = this.CurrentDataBank,
                Type = this.Type,
            };
        }
        #endregion
    }
}
