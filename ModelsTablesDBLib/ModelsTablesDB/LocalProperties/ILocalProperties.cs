﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib
{
   
    public interface IARDProperties
    {
        string ComPort { get; set; } //Com порт

        int PortSpeed { get; set; } //Скорость

        int Address { get; set; } //Адрес

        bool State { get; set; } //Состояние (вкл/выкл)
    }

    public interface IARD1Properties : IARDProperties
    {
    }

    public interface IARD2Properties : IARDProperties
    {
    }

    public interface IARD3Properties : IARDProperties
    {
    }    
    
}
