﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class PCProperties : INotifyPropertyChanged, IMethod<PCProperties>, IDataErrorInfo
    {

       public PCProperties() { }

        #region IModelMethods
        public PCProperties Clone()
        {
            return new PCProperties
            {
                ComPortAPD = ComPortAPD,
                ComPortTainet = ComPortTainet,
                Type = Type,
                IpAddress3G4GRouter = IpAddress3G4GRouter,
                IpAddressPPC = IpAddressPPC,
                IpAddressRouter = IpAddressRouter,
                Port3G4GRouter = Port3G4GRouter,
                PortPPC = PortPPC,
                PortRouter = PortRouter,
                TypeOfConection = TypeOfConection,       
                State = State,
                IpAddressRadioModem = IpAddressRadioModem,
                PortRadioModem = PortRadioModem,
                IsBerezinaVisible = IsBerezinaVisible,
                IsGarantVisible = IsGarantVisible,
                IsRossVisible = IsRossVisible,
            };
        }

        public bool Compare(PCProperties data)
        {
            if (comPortAPD != data.ComPortAPD || comPortTainet != data.ComPortTainet
                || type != data.Type || ipAddress3G4GRouter != data.IpAddress3G4GRouter
                || ipAddressPPC != data.IpAddressPPC || port3G4GRouter != data.Port3G4GRouter
                || ipAddressRouter != data.IpAddressRouter || portRouter != data.PortRouter
                || portPPC != data.PortPPC || typeOfConection != data.TypeOfConection
                || state != data.State || isBerezinaVisible != data.IsBerezinaVisible
                || isGarantVisible != data.IsGarantVisible || isRossVisible != data.IsRossVisible
                || portRadioModem != data.PortRadioModem || ipAddressRadioModem != data.IpAddressRouter)
                return false;
            return true;
        }

        public void Update(PCProperties model)
        {
            ComPortAPD = model.ComPortAPD;
            ComPortTainet = model.ComPortTainet;
            Type = model.Type;
            IpAddress3G4GRouter = model.IpAddress3G4GRouter;
            IpAddressPPC = model.IpAddressPPC;
            IpAddressRouter = model.IpAddressRouter;
            Port3G4GRouter = model.Port3G4GRouter;
            PortPPC = model.PortPPC;
            PortRouter = model.PortRouter;
            TypeOfConection = model.TypeOfConection;
            State = model.State;
            IpAddressRadioModem = model.IpAddressRadioModem;
            PortRadioModem = model.PortRadioModem;
            IsGarantVisible = model.IsGarantVisible;
            IsBerezinaVisible = model.IsBerezinaVisible;
            IsRossVisible = model.IsRossVisible;    
        }
        #endregion

        private string comPortAPD = "COM1";
        private string comPortTainet = "COM1";
        private TypePC type = TypePC.Garant;
        private TypeOfConection typeOfConection = TypeOfConection.APD;
        private bool state;

        private string ipAddress3G4GRouter = "127.0.0.1";
        private string ipAddressPPC = "127.0.0.1";
        private string ipAddressRouter = "127.0.0.1";
        private string ipAddressRadioModem = "127.0.0.1";

        private int port3G4GRouter = 80;
        private int portPPC = 80;
        private int portRouter = 80;
        private int portRadioModem = 80;

        private bool isGarantVisible = true;
        private bool isBerezinaVisible = true;
        private bool isRossVisible = true;

        [NotifyParentProperty(true)]
        public TypePC Type
        {
            get => type;
            set
            {
                if (value == type) return;
                type = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public TypeOfConection TypeOfConection
        {
            get => typeOfConection;
            set
            {
                if (value == typeOfConection) return;
                typeOfConection = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPortAPD
        {
            get => comPortAPD;
            set
            {
                if (value == comPortAPD) return;
                comPortAPD = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPortTainet
        {
            get => comPortTainet;
            set
            {
                if (value == comPortTainet) return;
                comPortTainet = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress3G4GRouter
        {
            get => ipAddress3G4GRouter;
            set
            {
                if (ipAddress3G4GRouter == value) return;
                ipAddress3G4GRouter = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port3G4GRouter
        {
            get => port3G4GRouter;
            set
            {
                if (port3G4GRouter == value) return;
                port3G4GRouter = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressPPC
        {
            get => ipAddressPPC;
            set
            {
                if (ipAddressPPC == value) return;
                ipAddressPPC = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int PortPPC
        {
            get => portPPC;
            set
            {
                if (portPPC == value) return;
                portPPC = value;
                OnPropertyChanged();
            }
        }


        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressRouter
        {
            get => ipAddressRouter;
            set
            {
                if (ipAddressRouter == value) return;
                ipAddressRouter = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int PortRouter
        {
            get => portRouter;
            set
            {
                if (portRouter == value) return;
                portRouter = value;
                OnPropertyChanged();
            }
        }


        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressRadioModem
        {
            get => ipAddressRadioModem;
            set
            {
                if (ipAddressRadioModem == value) return;
                ipAddressRadioModem = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int PortRadioModem
        {
            get => portRadioModem;
            set
            {
                if (portRadioModem == value) return;
                portRadioModem = value;
                OnPropertyChanged();
            }
        }


        public bool State
        {
            get => state;
            set
            {
                if (value == state) return;
                state = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsGarantVisible
        {
            get => isGarantVisible;
            set
            {
                if (value == isGarantVisible) return;
                isGarantVisible = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsBerezinaVisible
        {
            get => isBerezinaVisible;
            set
            {
                if (value == isBerezinaVisible) return;
                isBerezinaVisible = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsRossVisible
        {
            get => isRossVisible;
            set
            {
                if (value == isRossVisible) return;
                isRossVisible = value;
                OnPropertyChanged();
            }
        }


        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

    }
}

