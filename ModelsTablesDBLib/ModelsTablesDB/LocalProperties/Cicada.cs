﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsTablesDBLib
{
   public class Cicada : EndPointConnection, INotifyPropertyChanged, IMethod<Cicada>
    {
        #region Private
        private bool state = false;
        private bool isVisible = false;

        #endregion

        #region Properties

        [NotifyParentProperty(true)]
        public bool State
        {
            get { return state; }
            set
            {
                if (state == value)
                    return;
                state = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region IMethod

        public bool Compare(Cicada data)
        {
            if (!Compare(data as EndPointConnection) || state != data.state
                || isVisible != data.isVisible)
                return false;
            return true;
        }

        public void Update(Cicada data)
        {
            Update(data as EndPointConnection);
            State = data.State;
            IsVisible = data.IsVisible;
        }

        public new Cicada Clone()
        {
            return new Cicada
            {
                IpAddress = IpAddress,
                Port = Port,
                State = State,
                IsVisible = IsVisible
            };
        }

        public Cicada() : base()
        {
            IpAddress = "127.0.0.1";
            Port = 18081;
        }
        #endregion
    }
}

