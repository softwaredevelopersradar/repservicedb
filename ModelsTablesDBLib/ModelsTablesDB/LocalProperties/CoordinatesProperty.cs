﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ModelsTablesDBLib
{
    public class CoordinatesProperty: INotifyPropertyChanged, IMethod<CoordinatesProperty>
    {
        public CoordinatesProperty()
        {
            view = "DD.dddddd";
            coordSystem = EnumCoordinateSystem.WGS84;
            coordGPS = new Coord();
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IMethod

        public CoordinatesProperty Clone()
        {
            return new CoordinatesProperty
            {
                View = view,
                CoordinateSystem = coordSystem
            };
        }

        public bool Compare(CoordinatesProperty data)
        {
            if (CoordinateSystem != data.CoordinateSystem || View != data.View)
                return false;
            return true;
        }

        public void Update(CoordinatesProperty data)
        {
            CoordinateSystem = data.CoordinateSystem;
            View = data.View;
        }
        #endregion

        #region Private
        private EnumCoordinateSystem coordSystem;
        private string view;
        private static Coord coordGPS;
        private static double? compassRR;
        private static double? compassPA;
        #endregion

        #region Properties
        [NotifyParentProperty(true)]
        public EnumCoordinateSystem CoordinateSystem
        {
            get => coordSystem;
            set
            {
                if (coordSystem == value) return;
                coordSystem = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string View
        {
            get => view;
            set
            {
                if (view == value) return;
                view = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public Coord CoordGPS
        {
            get => coordGPS;
            set
            {
                if (coordGPS == value) return;
                coordGPS = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double? CompassRR
        {
            get => compassRR;
            set
            {
                if (compassRR == value) return;
                compassRR = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double? CompassPA
        {
            get => compassPA;
            set
            {
                if (compassPA == value) return;
                compassPA = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
