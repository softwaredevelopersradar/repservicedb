﻿
namespace ModelsTablesDBLib
{
    public interface IMethod<T> where T : class
    {
        T Clone();
        void Update(T data);
        bool Compare(T data);
    }
}
