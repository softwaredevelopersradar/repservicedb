﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class AP_2Properties : INotifyPropertyChanged, IMethod<AP_2Properties>, IDataErrorInfo
    {
        public AP_2Properties() { }

        #region IModelMethods
        public AP_2Properties Clone()
        {
            return new AP_2Properties
            {             
                IpAddress3G4GRouter = IpAddress3G4GRouter,
                IpAddressPPC = IpAddressPPC,
                Port3G4GRouter = Port3G4GRouter,
                PortPPC = PortPPC,
                TypeOfConection = TypeOfConection,
                State = State, 
                IsVisible = IsVisible
            };
        }

        public bool Compare(AP_2Properties data)
        {
            if ( ipAddress3G4GRouter != data.IpAddress3G4GRouter
                || ipAddressPPC != data.IpAddressPPC || port3G4GRouter != data.Port3G4GRouter
                || portPPC != data.PortPPC || typeOfConection != data.TypeOfConection
                || state != data.State || isVisible != data.IsVisible)
                return false;
            return true;
        }

        public void Update(AP_2Properties model)
        {         
            IpAddress3G4GRouter = model.IpAddress3G4GRouter;
            IpAddressPPC = model.IpAddressPPC;
            Port3G4GRouter = model.Port3G4GRouter;
            PortPPC = model.PortPPC;
            TypeOfConection = model.TypeOfConection;
            State = model.State;
            IsVisible = model.IsVisible;
        }
        #endregion

        private TypeOfConectionAP_2 typeOfConection = TypeOfConectionAP_2.PPC;
        private bool state;

        private string ipAddress3G4GRouter = "127.0.0.1";
        private string ipAddressPPC = "127.0.0.1";

        private int port3G4GRouter = 80;
        private int portPPC = 80;
        private bool isVisible = true;

        [NotifyParentProperty(true)]
        public TypeOfConectionAP_2 TypeOfConection
        {
            get => typeOfConection;
            set
            {
                if (value == typeOfConection) return;
                typeOfConection = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress3G4GRouter
        {
            get => ipAddress3G4GRouter;
            set
            {
                if (ipAddress3G4GRouter == value) return;
                ipAddress3G4GRouter = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port3G4GRouter
        {
            get => port3G4GRouter;
            set
            {
                if (port3G4GRouter == value) return;
                port3G4GRouter = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressPPC
        {
            get => ipAddressPPC;
            set
            {
                if (ipAddressPPC == value) return;
                ipAddressPPC = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int PortPPC
        {
            get => portPPC;
            set
            {
                if (portPPC == value) return;
                portPPC = value;
                OnPropertyChanged();
            }
        }
        public bool State
        {
            get => state;
            set
            {
                if (value == state) return;
                state = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisible
        {
            get => isVisible;
            set
            {
                if (value == isVisible) return;
                isVisible = value;
                OnPropertyChanged();
            }
        }

        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}

