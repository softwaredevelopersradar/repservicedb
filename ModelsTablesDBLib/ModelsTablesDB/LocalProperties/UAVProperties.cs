﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class UAVProperties : INotifyPropertyChanged, IMethod<UAVProperties>, IDataErrorInfo
    {
        private bool isExistance = false;

        public bool IsExistance
        {
            get => isExistance;
            set
            {
                if (isExistance == value) return;
                isExistance = value;
                OnPropertyChanged();
            }
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IMethod

        public UAVProperties Clone()
        {
            return new UAVProperties()
            {
                IsExistance = IsExistance,
            };
        }

        public void Update(UAVProperties model)
        {
            IsExistance = model.IsExistance;
        }

        public bool Compare(UAVProperties model)
        {
            if (isExistance != model.IsExistance )
                return false;
            return true;
        }

        #endregion

        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion



    }
}
