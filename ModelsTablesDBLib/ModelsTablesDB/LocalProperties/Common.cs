﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;

namespace ModelsTablesDBLib
{
    public class CommonLocal : INotifyPropertyChanged, IMethod<CommonLocal>
    {
        public CommonLocal()
        {
            accessARM = 0;
            numberARM = 1;
            language= 0;
            pathData = "";
            autoReport = false;
            fileType = FileType.Txt;
            frequencyUnits = EnumFrequencyUnits.MHz;
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private TypeAccessARM accessARM;// = 0;
        private byte numberARM;// = 0;
        private Languages language = 0;
        private string pathData;
        private bool autoReport;
        private FileType fileType;
        private EnumFrequencyUnits frequencyUnits = EnumFrequencyUnits.MHz;
        private bool en = true;
        private bool ru = true;
        private bool az = true;


        #endregion

        #region Properties

        [NotifyParentProperty(true)]
        public TypeAccessARM AccessARM //АРМ (командир, оператор)
        {
            get { return accessARM; }
            set
            {
                if (accessARM == value)
                    return;
                accessARM = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public byte NumberARM //Номер АРМ (1,2,3
        {
            get { return numberARM; }
            set
            {
                if (numberARM == value)
                    return;
                numberARM = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public Languages Language
        {
            get { return language; }
            set
            {
                if (language == value)
                    return;
                language = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        public string PathData
        {
            get => pathData;
            set
            {
                if (pathData == value) return;
                pathData = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool AutoReport
        {
            get => autoReport;
            set
            {
                if (autoReport == value) return;
                autoReport = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public FileType FileType
        {
            get => fileType;
            set
            {
                if (fileType == value) return;
                fileType = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public EnumFrequencyUnits FrequencyUnits
        {
            get => frequencyUnits;
            set
            {
                if (frequencyUnits == value) return;
                frequencyUnits = value;
                OnPropertyChanged();
            }
        }



        [Browsable(false)]
        public bool IsVisibleEN
        {
            get => en;
            set
            {
                if (en == value) return;
                en = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleRU
        {
            get => ru;
            set
            {
                if (ru == value) return;
                ru = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleAZ
        {
            get => az;
            set
            {
                if (az == value) return;
                az = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        public CommonLocal Clone()
        {
            return new CommonLocal()
            {
                AccessARM = accessARM,
                Language = language,
                NumberARM = numberARM,
                AutoReport = autoReport,
                PathData = pathData,
                FileType = fileType,
                FrequencyUnits = frequencyUnits,
                IsVisibleEN = IsVisibleEN,
                IsVisibleAZ = IsVisibleAZ,
                IsVisibleRU = IsVisibleRU
            };
        }

        public void Update(CommonLocal commonLocal)
        {
            AccessARM = commonLocal.AccessARM;
            Language = commonLocal.Language;
            NumberARM = commonLocal.NumberARM;
            AutoReport = commonLocal.AutoReport;
            PathData = commonLocal.PathData;
            FileType = commonLocal.FileType;
            FrequencyUnits = commonLocal.FrequencyUnits;
            IsVisibleEN = commonLocal.IsVisibleEN;
            IsVisibleAZ = commonLocal.IsVisibleAZ;
            IsVisibleRU = commonLocal.IsVisibleRU;
        }

        public bool Compare(CommonLocal data)
        {
            if (accessARM != data.AccessARM || numberARM != data.numberARM
                || language != data.language || autoReport != data.autoReport
                || pathData != data.pathData || fileType != data.fileType || en != data.en|| ru != data.ru|| az != data.az || frequencyUnits != data.frequencyUnits)
                return false;
            return true;
        }

        #endregion

    }
}
