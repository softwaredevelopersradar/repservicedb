﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace ModelsTablesDBLib
{
    public class CompasRI : ComConnection, INotifyPropertyChanged, IMethod<CompasRI>, IDataErrorInfo
    {
        private int compassСorrection = 0;

        [NotifyParentProperty(true)]
        [Range(0, 360)]
        public int CompassСorrection
        {
            get { return compassСorrection; }
            set
            {
                if (compassСorrection == value)
                    return;
                compassСorrection = value;
                OnPropertyChanged();
            }
        }

        public bool Compare(CompasRI data)
        {
            if (CompassСorrection != data.CompassСorrection || ComPort != data.ComPort || PortSpeed != data.PortSpeed || State != data.State)
                return false;
            return true;
        }

        public void Update(CompasRI data)
        {
            CompassСorrection = data.CompassСorrection;
            ComPort = data.ComPort;
            PortSpeed = data.PortSpeed;
            State = data.State;
        }

        public CompasRI Clone()
        {
            return new CompasRI
            {
                CompassСorrection = CompassСorrection,
                ComPort = ComPort,
                PortSpeed = PortSpeed,
                State = State
            };
        }


        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion

    }
}
