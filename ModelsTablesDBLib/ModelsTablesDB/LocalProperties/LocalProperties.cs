﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls.WpfPropertyGrid;

namespace ModelsTablesDBLib
{
    #region CategoryOrder
    [CategoryOrder(nameof(Common),1)]
    [CategoryOrder(nameof(DspServer), 2)]
    [CategoryOrder(nameof(DbServer), 3)]
    [CategoryOrder(nameof(EdServer), 4)]
    [CategoryOrder(nameof(PCProperties), 5)]
    [CategoryOrder(nameof(AP_2Properties), 6)]
    [CategoryOrder(nameof(ADSB), 7)]
    [CategoryOrder(nameof(CmpRR), 8)]
    [CategoryOrder(nameof(CmpPA), 9)]
    [CategoryOrder(nameof(ARONE1), 10)]
    [CategoryOrder(nameof(ARONE2), 11)]
    [CategoryOrder(nameof(Tuman), 12)]
    [CategoryOrder(nameof(ARD1), 13)]
    [CategoryOrder(nameof(ARD2), 14)]
    [CategoryOrder(nameof(ARD3), 15)]
    [CategoryOrder(nameof(CoordinatesProperty), 16)]
    [CategoryOrder(nameof(UAVProperties), 17)]
    [CategoryOrder(nameof(VoiceInterference), 18)]
    [CategoryOrder(nameof(Spoofing), 19)]
    [CategoryOrder(nameof(Cicada), 20)]
    #endregion
    public class LocalProperties : INotifyPropertyChanged, IMethod<LocalProperties>
    {
        public LocalProperties()
        {
            common = new CommonLocal();
            dspServer = new EndPointConnection();
            dbServer = new EndPointConnection();
            edServer = new ODProperties();
            adsb = new ADSBConnection();
            cmpRR = new CompasRI();
            cmpPA = new ComConnection();
            arone1 = new ARONEProperties();
            arone2 = new ARONEProperties();
            tuman = new ComConnection();
            ard1 = new ARDConnection();
            ard2 = new ARDConnection();
            ard3 = new ARDConnection();
            coordinatesProperty = new CoordinatesProperty();
            voiceInterference = new VoiceInterference();
            spoofingProperty = new Spoofing();
            pCProperties = new PCProperties();
            aP_2Properties = new AP_2Properties();
            cicada = new Cicada();
            uAVProperties = new UAVProperties();
        }
        
        #region INotify 
        
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        
        #endregion

        #region Private
        private CommonLocal common;
        private EndPointConnection dspServer;
        private EndPointConnection dbServer;
        private ODProperties edServer;
        private ADSBConnection adsb;
        private CompasRI cmpRR;
        private ComConnection cmpPA;
        private ARONEProperties arone1; //private ComConnection arone;
        private ComConnection tuman;
        private ARDConnection ard1;
        private ARDConnection ard2;
        private ARDConnection ard3;
        private CoordinatesProperty coordinatesProperty;
        private ARONEProperties arone2;
        private VoiceInterference voiceInterference;
        private Spoofing spoofingProperty;
        private PCProperties pCProperties;
        private AP_2Properties aP_2Properties;
        private Cicada cicada;
        private UAVProperties uAVProperties;
        #endregion

        #region Properties

        [Category("Common")]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CommonLocal Common
        {
            get => common;
            set
            {
                if (common == value)
                    return;
                common = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DspServer)), PropertyOrder(2)]
        [DisplayName(" ")]
        public EndPointConnection DspServer
        {
            get => dspServer;
            set
            {
                if (dspServer == value)
                    return;
                dspServer = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DbServer))]
        [DisplayName(" ")]
        public EndPointConnection DbServer
        {
            get => dbServer;
            set
            {
                if (dbServer == value)
                    return;
                dbServer = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(EdServer))]
        [DisplayName(" ")]
        public ODProperties EdServer
        {
            get => edServer;
            set
            {
                if (edServer == value)
                    return;
                edServer = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(PCProperties))]
        [DisplayName(" ")]
        public PCProperties PCProperties
        {
            get => pCProperties;
            set
            {
                if (pCProperties == value) return;
                pCProperties = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(AP_2Properties))]
        [DisplayName(" ")]
        public AP_2Properties AP_2Properties
        {
            get => aP_2Properties;
            set
            {
                if (aP_2Properties == value) return;
                aP_2Properties = value;
                OnPropertyChanged();
            }
        }


        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(ADSB))]
        [DisplayName(" ")]
        public ADSBConnection ADSB
        {
            get => adsb;
            set
            {
                if (adsb == value)
                    return;
                adsb = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(CmpRR))]
        [DisplayName(" ")]
        public CompasRI CmpRR
        {
            get => cmpRR;
            set
            {
                if (cmpRR == value)
                    return;
                cmpRR = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(CmpPA))]
        [DisplayName(" ")]
        public ComConnection CmpPA
        {
            get => cmpPA;
            set
            {
                if (cmpPA == value)
                    return;
                cmpPA = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(ARONE1))]
        [DisplayName(" ")]
        public ARONEProperties ARONE1
        {
            get => arone1;
            set
            {
                if (arone1 == value)
                    return;
                arone1 = value;
                OnPropertyChanged();
            }
        }


        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(ARONE2))]
        [DisplayName(" ")]
        public ARONEProperties ARONE2
        {
            get => arone2;
            set
            {
                if (arone2 == value)
                    return;
                arone2 = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Tuman))]
        [DisplayName(" ")]
        public ComConnection Tuman
        {
            get => tuman;
            set
            {
                if (tuman == value)
                    return;
                tuman = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category("ARD1")]
        [DisplayName(" ")]
        public ARDConnection ARD1
        {
            get => ard1;
            set
            {
                if (ard1 == value)
                    return;
                ard1 = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category("ARD2")]
        [DisplayName(" ")]
        public ARDConnection ARD2
        {
            get => ard2;
            set
            {
                if (ard2 == value)
                    return;
                ard2 = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category("ARD3")]
        [DisplayName(" ")]
        public ARDConnection ARD3
        {
            get => ard3;
            set
            {
                if (ard3 == value)
                    return;
                ard3 = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(CoordinatesProperty))]
        [DisplayName(" ")]
        public CoordinatesProperty CoordinatesProperty
        {
            get => coordinatesProperty;
            set
            {
                if (coordinatesProperty == value) return;
                coordinatesProperty = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(VoiceInterference))]
        [DisplayName(" ")]
        public VoiceInterference VoiceInterference
        {
            get => voiceInterference;
            set
            {
                if (voiceInterference == value) return;
                voiceInterference = value;
                OnPropertyChanged();
            }
        }


        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Spoofing))]
        [DisplayName(" ")]
        public Spoofing Spoofing
        {
            get => spoofingProperty;
            set
            {
                if (spoofingProperty == value) return;
                spoofingProperty = value;
                OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Cicada))]
        [DisplayName(" ")]
        public Cicada Cicada
        {
            get => cicada;
            set
            {
                if (cicada == value) return;
                cicada = value;
                OnPropertyChanged();
            }
        }


        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(UAVProperties))]
        [DisplayName(" ")]
        public UAVProperties UAVProperties
        {
            get => uAVProperties;
            set
            {
                if (uAVProperties == value) return;
                uAVProperties = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region IMethod

        public void Update(LocalProperties properties)
        {
            ADSB.Update(properties.ADSB);
            PCProperties.Update(properties.PCProperties);
            AP_2Properties.Update(properties.AP_2Properties);
            ARD1.Update(properties.ARD1);
            ARD2.Update(properties.ARD2);
            ARD3.Update(properties.ARD3);
            ARONE1.Update(properties.ARONE1);
            ARONE2.Update(properties.ARONE2);
            CmpRR.Update(properties.CmpRR);
            CmpPA.Update(properties.CmpPA);
            Common.Update(properties.Common);
            DbServer.Update(properties.DbServer);
            DspServer.Update(properties.DspServer);
            EdServer.Update(properties.EdServer);
            Tuman.Update(properties.Tuman);
            CoordinatesProperty.Update(properties.CoordinatesProperty);
            VoiceInterference.Update(properties.VoiceInterference);
            Spoofing.Update(properties.Spoofing);
            Cicada.Update(properties.Cicada);
            UAVProperties.Update(properties.UAVProperties);
        }        

        public LocalProperties Clone()
        {
            return new LocalProperties()
            {
                Tuman = Tuman.Clone(),
                DspServer = DspServer.Clone(),
                DbServer = DbServer.Clone(),
                EdServer = EdServer.Clone(),
                Common = Common.Clone(),
                ADSB = ADSB.Clone(),
                ARD1 = ARD1.Clone(),
                ARD2 = ARD2.Clone(),
                ARD3 = ARD3.Clone(),
                ARONE1 = ARONE1.Clone(),
                ARONE2 = ARONE2.Clone(),
                CmpRR = CmpRR.Clone(),
                CmpPA = CmpPA.Clone(),
                CoordinatesProperty = CoordinatesProperty.Clone(),
                VoiceInterference = VoiceInterference.Clone(),
                Spoofing = Spoofing.Clone(),
                PCProperties = PCProperties.Clone(),
                AP_2Properties = AP_2Properties.Clone(),
                Cicada = Cicada.Clone(),
                UAVProperties = UAVProperties.Clone(),
            };
        }

        public bool Compare(LocalProperties local)
        {
            if (!common.Compare(local.Common)) return false;
            if (!(dbServer.Compare(local.dbServer))
                || !(adsb.Compare(local.adsb))
                || !(edServer.Compare(local.edServer))
                || !(dspServer.Compare(local.dspServer))) return false;
            if (!cmpRR.Compare(local.cmpRR) || !cmpPA.Compare(local.cmpPA)) return false;
            if (!arone1.Compare(local.arone1) || !arone2.Compare(local.arone2) || !tuman.Compare(local.tuman)) return false;
            if (!ard1.Compare(local.ard1) || !ard2.Compare(local.ard2) || !ard3.Compare(local.ard3) || !UAVProperties.Compare(local.uAVProperties)) return false;
            if (!coordinatesProperty.Compare(local.coordinatesProperty) || !spoofingProperty.Compare(local.spoofingProperty) || !cicada.Compare(local.cicada)
                || !voiceInterference.Compare(local.voiceInterference) || !pCProperties.Compare(local.pCProperties) || !aP_2Properties.Compare(local.aP_2Properties)) return false;
            return true;
        }

        #endregion

    }
}
