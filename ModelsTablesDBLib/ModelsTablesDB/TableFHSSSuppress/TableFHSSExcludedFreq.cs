﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace ModelsTablesDBLib
{
    [DataContract]
    [InfoTable(NameTable.TableFHSSExcludedFreq)]
    [KnownType(typeof(AbstractDependentFHSS))]
    public class TableFHSSExcludedFreq : AbstractDependentFHSS
    {
        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Browsable(false)]
        public override int IdFHSS { get; set; }

        [DataMember]
        [Category("ФРЧ")]
        [DisplayName("F, кГц")]
        [PropertyOrder(1)]
        public double FreqKHz { get; set; }

        [DataMember]
        [Category("ФРЧ")]
        [DisplayName("Δf, кГц")]
        [PropertyOrder(2)]
        public float Deviation { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Deviation = ((TableFHSSExcludedFreq)record).Deviation;
            FreqKHz = ((TableFHSSExcludedFreq)record).FreqKHz;
        }

        public TableFHSSExcludedFreq Clone()
        {
            return new TableFHSSExcludedFreq()
            {
                Deviation = Deviation,
                FreqKHz = FreqKHz,
                Id = Id,
                IdFHSS = IdFHSS
            };
        }
        
    }
}
