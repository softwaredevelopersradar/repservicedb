﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using System.Linq;
using System.Collections.ObjectModel;
using ModelsTablesDBLib.Interfaces;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// ИРИ ППРЧ РП
    /// </summary>
    [DataContract]
    [CategoryOrder(nameof(InterferenceParam), 2)]
    [InfoTable(NameTable.TableSuppressFHSS)]
    [KnownType(typeof(AbstractDependentASP))]
    
    public class TableSuppressFHSS : AbstractDependentASP, IFixSupprFHSS
    {
        #region Properties

        [DataMember]
        [Browsable(false)]
        
        public override int Id { get; set; }

        [DataMember]
        [Browsable(false)]
        
        public override int IdMission { get; set; }

        [DataMember]
        [Browsable(false)]
        
        public override int NumberASP { get; set; } // номер станции

        [DataMember]
        [DisplayName("F min, кГц")]
        [PropertyOrder(1)]
        
        public double FreqMinKHz { get; set; }

        [DataMember]
        [DisplayName("F max, кГц")]
        [PropertyOrder(2)]
        
        public double FreqMaxKHz { get; set; }

        [DataMember]
        [DisplayName("U, дБ")]
        [PropertyOrder(3)]
        
        public double Threshold { get; set; }

        [DataMember]
        [DisplayName("Шаг сетки, кГц")]
        [PropertyOrder(4)]
        
        public short StepKHz { get; set; }

        [DataMember]
        [Category(nameof(InterferenceParam))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        
        public InterferenceParam InterferenceParam { get; set; } = new InterferenceParam();

        [DataMember]
        [Browsable(false)]
        
        public byte[] Letters { get; set; }

        [DataMember]
        [Browsable(false)]
        
        public byte[] EPO { get; set; }


        #endregion

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            EPO = ((TableSuppressFHSS)record).EPO;
            FreqMaxKHz = ((TableSuppressFHSS)record).FreqMaxKHz;
            FreqMinKHz = ((TableSuppressFHSS)record).FreqMinKHz;
            InterferenceParam.Update(((TableSuppressFHSS)record).InterferenceParam);
            Letters = ((TableSuppressFHSS)record).Letters;
            Threshold = ((TableSuppressFHSS)record).Threshold;
            StepKHz = ((TableSuppressFHSS)record).StepKHz;
        }

        public TableSuppressFHSS Clone()
        {
            return new TableSuppressFHSS()
            {
                Id = Id,
                EPO = EPO,
                FreqMaxKHz = FreqMaxKHz,
                FreqMinKHz = FreqMinKHz,
                IdMission = IdMission,
                InterferenceParam = InterferenceParam.Clone(),
                Letters = Letters,
                NumberASP = NumberASP,
                StepKHz = StepKHz,
                Threshold = Threshold
            };
        }
    }
}
