﻿using System;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Запрещенные частоты
    /// </summary>
   // [DataContract]
    [KnownType(typeof(TableFreqSpec))]
    [InfoTable(NameTable.TableFreqForbidden)]
    
    public class TableFreqForbidden : TableFreqSpec
    {
    }
}
