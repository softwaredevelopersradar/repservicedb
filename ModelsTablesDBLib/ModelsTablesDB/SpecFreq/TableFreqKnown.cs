﻿using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Известные частоты
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableFreqSpec))]
    [InfoTable(NameTable.TableFreqKnown)]
    
    public class TableFreqKnown : TableFreqSpec
    {
    }
}
