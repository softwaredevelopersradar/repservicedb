﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Controls.WpfPropertyGrid;
using ModelsTablesDBLib.Interfaces;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Специальные частоты
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(TableFreqForbidden))]
    [KnownType(typeof(TableFreqImportant))]
    [KnownType(typeof(TableFreqKnown))]
    
    public class TableFreqSpec : AbstractDependentASP, IFixSpecFreq
    {
        [DataMember]
        [Category("ID")]
        [DisplayName("ID"), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("ID")]
        [DisplayName("№ АСП"), ReadOnly(true), Browsable(true)]
        public override int NumberASP { get; set; }  // адрес станции

        [DataMember]
        [Browsable(false)]
        public override int IdMission { get; set; }

        [DataMember]
        [Category("Диапазон")]
        [DisplayName("F min, кГц")]
        [PropertyOrder(1)]
        [Browsable(true)]
        
        public double FreqMinKHz { get; set; }  // частота мин.

        [DataMember]
        [Category("Диапазон")]
        [DisplayName("F max, кГц")]
        [PropertyOrder(2)]
        [Browsable(true)]
        
        public double FreqMaxKHz { get; set; }  // частота макс.

        [DataMember]
        [DisplayName("Примечание")]
        
        public string Note { get; set; } = string.Empty;// примечание

        public TableFreqSpec Clone()
        {
            return new TableFreqSpec
            {
                Id = this.Id,
                FreqMinKHz = this.FreqMinKHz,
                FreqMaxKHz = this.FreqMaxKHz,
                NumberASP = this.NumberASP,
                Note = this.Note
            };
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableFreqForbidden ToFreqForbidden()
        {
            TableFreqForbidden table = new TableFreqForbidden()
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                IdMission = this.IdMission,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note
            };
            return table;
        }

        public TableFreqImportant ToFreqImportant()
        {
            TableFreqImportant table = new TableFreqImportant()
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                IdMission = this.IdMission,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note
            };
            return table;
        }

        public TableFreqKnown ToFreqKnown()
        {
            TableFreqKnown table = new TableFreqKnown()
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                IdMission = this.IdMission,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note
            };
            return table;
        }

        public override void Update(AbstractCommonTable table)
        {
            //IdMission = ((TableFreqSpec)table).IdMission;
            //NumberASP = ((TableFreqSpec)table).NumberASP;
            FreqMaxKHz = ((TableFreqSpec)table).FreqMaxKHz;
            FreqMinKHz = ((TableFreqSpec)table).FreqMinKHz;
            Note = ((TableFreqSpec)table).Note;
        }
    }
}
