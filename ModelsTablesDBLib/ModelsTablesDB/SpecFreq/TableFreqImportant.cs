﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Важные частоты
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableFreqImportant)]
    
    public class TableFreqImportant : TableFreqSpec
    {
    }
}
