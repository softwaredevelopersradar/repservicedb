﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// Объекты противника
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableEnemyResources)]
    [KnownType(typeof(AbstractDependentMission))]
    
    public class TableEnemyResources : AbstractDependentMission
    {
        [DataMember]
        
        public override int IdMission { get; set; }

        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public string Name { get; set; } = string.Empty;

        [DataMember]
        
        public Coord Coordinates { get; set; }

        [DataMember]
        
        public int FreqMin { get; set; }

        [DataMember]
        
        public int FreqMax { get; set; }

        [DataMember]
        
        public string Caption { get; set; } = string.Empty;

        [DataMember]
        
        public string Image { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id }; // IdMission,
        }

        public override void Update(AbstractCommonTable record)
        {
            Caption = ((TableEnemyResources)record).Caption;
            Coordinates = ((TableEnemyResources)record).Coordinates;
            Image = ((TableEnemyResources)record).Image;
            Name = ((TableEnemyResources)record).Name;
            FreqMin = ((TableEnemyResources)record).FreqMin;
            FreqMax = ((TableEnemyResources)record).FreqMax;
        }
    }
}
