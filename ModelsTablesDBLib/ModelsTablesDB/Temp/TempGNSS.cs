﻿using System;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// TempGNSS
    /// </summary>
    [DataContract]
    [KnownType(typeof(Coord))]
    [KnownType(typeof(AbstractDependentMission))]
    [InfoTable(NameTable.TempGNSS)]
    
    public class TempGNSS : AbstractDependentMission
    {
        [DataMember]        
        public override int IdMission { get; set; }

        [DataMember]        
        public override int Id { get; set; }

        [DataMember]        
        public DateTime UtcTime { get; set; }

        [DataMember]        
        public DateTime LocalTime { get; set; }

        [DataMember]
        public Coord Location { get; set; } = new Coord();

        [DataMember]        
        public int NumberOfSatellites { get; set; }

        [DataMember]        
        public double AntennaHeight { get; set; }

        [DataMember]        
        public double CmpRR { get; set; }

        [DataMember]        
        public double CmpPA { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            UtcTime = ((TempGNSS)record).UtcTime;
            LocalTime = ((TempGNSS)record).LocalTime;
            Location.Altitude = ((TempGNSS)record).Location.Altitude;
            Location.Latitude = ((TempGNSS)record).Location.Latitude;
            Location.Longitude = ((TempGNSS)record).Location.Longitude;
            NumberOfSatellites = ((TempGNSS)record).NumberOfSatellites;
            CmpRR = ((TempGNSS)record).CmpRR;
            CmpPA = ((TempGNSS)record).CmpPA;


        }
    }
}
