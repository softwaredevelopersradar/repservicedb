﻿using ModelsTablesDBLib.Interfaces;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractDependentMission))]
    [InfoTable(NameTable.TempSuppressFHSS)]
    public class TempSuppressFHSS : AbstractDependentMission, INotifyPropertyChanged, IFixTempSuppr
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private Led control;
        private Led suppress;
        private Led radiation;

        #endregion

        [DataMember]
        public override int IdMission { get; set; }

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public Led Control
        {
            get => control;
            set
            {
                if (control == value) return;
                control = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        public Led Suppress
        {
            get => suppress;
            set
            {
                if (suppress == value) return;
                suppress = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        public Led Radiation
        {
            get => radiation;
            set
            {
                if (radiation == value) return;
                radiation = value;
                OnPropertyChanged();
            }
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Control = ((TempSuppressFHSS)record).Control;
            Radiation = ((TempSuppressFHSS)record).Radiation;
            Suppress = ((TempSuppressFHSS)record).Suppress;
        }
    }
}
