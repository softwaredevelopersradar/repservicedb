﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// ИРИ ФРЧ
    /// </summary>
    [DataContract]
    [KnownType(typeof(JamDirect))]
    [KnownType(typeof(Coord))]
    [KnownType(typeof(AbstractDependentMission))]
    [InfoTable(NameTable.TempFWS)]
    
    public class TempFWS : AbstractDependentMission
    {
        #region Properies

        [DataMember]
        
        public override int Id { get; set; }

        [DataMember]
        
        public override int IdMission { get; set; }

        [DataMember]
        
        public double FreqKHz{ get; set; }

        [DataMember]
        
        public float Deviation { get; set; }         //Ширина полосы

        [DataMember]
        
        public DateTime Time { get; set; }              //Время обнаружения

        [DataMember]
        
        public ObservableCollection<JamDirect> ListQ { get; set; } = new ObservableCollection<JamDirect>();

        [DataMember]
        
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        
        public byte Type { get; set; }

        [DataMember]
        
        public bool Checking { get; set; }

        [DataMember]
        
        public byte SignSpecFreq { get; set; } //Тип специальной частоты

        [DataMember]
        
        public Led Control { get; set; }

        [DataMember]
        
        public bool? IsSelected { get; set; }

        #endregion

        public override object[] GetKey()
        {
            return new object[] { Id }; //, IdMission
        }

        public override void Update(AbstractCommonTable record) //(ICommonTable record)
        {
            Coordinates.Altitude = ((TempFWS)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TempFWS)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TempFWS)record).Coordinates.Longitude;
            Deviation = ((TempFWS)record).Deviation;
            FreqKHz = ((TempFWS)record).FreqKHz;
            //ListQ = ((TempFWS)record).ListQ;
            Time = ((TempFWS)record).Time;
            Type = ((TempFWS)record).Type;
            Control = ((TempFWS)record).Control;
            ListQ.Clear();

            if (((TempFWS)record).IsSelected.HasValue)
                IsSelected = ((TempFWS)record).IsSelected;

            foreach (var t in ((TempFWS)record).ListQ)
                ListQ.Add(t);
        }
    }
}
