﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace ModelsTablesDBLib
{ /// <summary>
  /// ADSB
  /// </summary>
    [DataContract]
    [KnownType(typeof(Coord))]
    [KnownType(typeof(AbstractDependentMission))]
    [InfoTable(NameTable.TempADSB)]
    public class TempADSB : AbstractDependentMission
    {
        [DataMember]
        public override int IdMission { get; set; }

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public string ICAO { get; set; } = string.Empty;

        [DataMember]
        public Coord Coordinates { get; set; }

        [DataMember]
        public string Time { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }
        
        public override void Update(AbstractCommonTable record)
        {
            Coordinates = ((TempADSB)record).Coordinates;
            ICAO = ((TempADSB)record).ICAO;
            Time = ((TempADSB)record).Time;
        }
    }
}
