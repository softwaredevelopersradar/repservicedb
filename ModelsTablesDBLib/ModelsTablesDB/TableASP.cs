﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using ModelsTablesDBLib.Interfaces;

namespace ModelsTablesDBLib
{
    /// <summary>
    /// АСП
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder("Антенны", 3)]
    [CategoryOrder(nameof(Coordinates), 4)]
    [CategoryOrder("Карта", 5)]
    [CategoryOrder("Прочее", 6)]
    [InfoTable(NameTable.TableASP)]
    [KnownType(typeof(AbstractDependentMission))]
    public class TableASP : AbstractDependentMission, IFixASP
    {
        [DataMember]
        [Category("ID")]
        [DisplayName("№ АСП"), ReadOnly(false)]
        [Browsable(true)]
        public override int Id { get; set; }

        [DataMember]
        [Category("ID")]
        [DisplayName("Своя")]
        public bool ISOwn { get; set; }

        [DataMember]
        [Browsable(false)]
        //Номер Обстановки
        public override int IdMission { get; set; }
        
        [DataMember]
        [Category("Карта")]
        [DisplayName("Подпись")]
        public string Caption { get; set; } = string.Empty;

        [DataMember]
        [Category("Карта")]
        [DisplayName("Знак")]
        public string Image { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName("Адрес IP")]
        public string AddressIP { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName("Порт")]
        public int AddressPort { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Подключена"), Browsable(false)]
        public Led IsConnect { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Позывной")]
        public string CallSign { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName("Режим"), Browsable(false)]
        public byte Mode { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Роль")]
        public RoleStation Role { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Тип"), Browsable(false)]
        public byte Type { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("Высота РП, м")]
        public byte AntHeightSup { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("Высота РР, м")]
        public byte AntHeightRec { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Литеры"), Browsable(false)]
        public byte[] Letters { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("РРС 1,°")]
        public short RRS1 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("РРС 2,°")]
        public short RRS2 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 1,3,°")]
        public short LPA13 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 2,4,°")]
        public short LPA24 { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 5-10,°")]
        public short LPA510 { get; set; }
        [DataMember]
        [Category("Антенны")]
        [DisplayName("БПСС,°")]
        public short BPSS { get; set; }

        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 5-7,°")]
        public short LPA57 { get; set; }
        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 5-9,°")]
        public short LPA59 { get; set; }
        [DataMember]
        [Category("Антенны")]
        [DisplayName("ЛПА 10,°")]
        public short LPA10 { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Browsable(false)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Sectors Sectors { get; set; } = new Sectors();


        [DataMember]
        [Category("Общие")]
        [DisplayName("Тип связи")]
        public TypeConnection TypeConnection { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName("СНС")]
        public bool IsGnssUsed { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("№ сопряженной станции")]
        public int MatedStationNumber { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName("Адрес IP 3G/4G")]
        public string AddressIp3G4G { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName("Порт 3G/4G")]
        public int AddressPort3G4G { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id, IdMission };
        }

        public override void Update(AbstractCommonTable record)
        {
            var rec = (TableASP)record;
            IsConnect = rec.IsConnect;
            CallSign = rec.CallSign;
            Letters = rec.Letters;
            LPA13 = rec.LPA13;
            LPA24 = rec.LPA24;
            LPA510 = rec.LPA510;
            Mode = rec.Mode;
            RRS1 = rec.RRS1;
            RRS2 = rec.RRS2;
            Coordinates.Altitude = rec.Coordinates.Altitude;
            Coordinates.Latitude = rec.Coordinates.Latitude;
            Coordinates.Longitude = rec.Coordinates.Longitude;
            AddressIP = rec.AddressIP;
            AddressPort = rec.AddressPort;
            AntHeightRec = rec.AntHeightRec;
            AntHeightSup = rec.AntHeightSup;
            Caption = rec.Caption;
            Image = rec.Image;
            ISOwn = rec.ISOwn;
            Role = rec.Role;
            Type = rec.Type;
            Sectors.Update(rec.Sectors);
            TypeConnection = rec.TypeConnection;
            IsGnssUsed = rec.IsGnssUsed;
            BPSS = rec.BPSS;
            LPA57 = rec.LPA57;
            LPA59 = rec.LPA59;
            LPA10 = rec.LPA10;
            MatedStationNumber = rec.MatedStationNumber;
            AddressIp3G4G = rec.AddressIp3G4G;
            AddressPort3G4G = rec.AddressPort3G4G;
        }

        public TableASP Clone()
        {
            return new TableASP
            {
                Id = this.Id,
                AddressIP = AddressIP,
                AddressPort = AddressPort,
                AntHeightRec = AntHeightRec,
                AntHeightSup = AntHeightSup,
                CallSign = CallSign,
                Caption = Caption,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                IdMission = IdMission,
                Image = Image,
                IsConnect = IsConnect,
                Letters = new byte[] { },
                LPA13 = LPA13,
                LPA24 = LPA24,
                LPA510 = LPA510,
                Mode = Mode,
                ISOwn = ISOwn,
                Role = Role,
                RRS1 = RRS1,
                RRS2 = RRS2,
                Type = Type,
                Sectors = Sectors.Clone(),
                TypeConnection = TypeConnection,
                IsGnssUsed = IsGnssUsed,
                BPSS = BPSS,
                LPA10 = LPA10,
                LPA57 = LPA57,
                LPA59 = LPA59,
                MatedStationNumber = MatedStationNumber,
                AddressIp3G4G = AddressIp3G4G,
                AddressPort3G4G = AddressPort3G4G
            };
        }
    }
}