﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Errors
{
    public class ServerErrors
    {
        public static string GetDefenition(EnumServerError error)
        {
            try { return DicError[error]; }
            catch (Exception) { return ""; }
        }

        private static Dictionary<EnumServerError, string> DicError = new Dictionary<EnumServerError, string>
        {
            { EnumServerError.UnknownError, "Error: "},
            { EnumServerError.ClientAbsent, "Error: Client is absent! "}
        };
    }
}
