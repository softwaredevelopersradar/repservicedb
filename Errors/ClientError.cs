﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Errors
{
    public class ClientError
    {
        public static string GetDefenition(EnumClientError error)
        {
            try { return DicError[error]; }
            catch (Exception) { return ""; }
        }
        
        private static Dictionary<EnumClientError, string> DicError = new Dictionary<EnumClientError, string>
        {
            { EnumClientError.UnknownError, "Error: "},
            { EnumClientError.NoConnection, "Error: No Connection! "},
            { EnumClientError.IncorrectEndpoint, "Error: Endpoint is incorrect!"}
        };
    }
}
