﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Errors
{
    [DataContract]
    public class DBError 
    {
        public static string GetDefenition(EnumDBError error)
        {
            try { return DicError[error]; }
            catch (Exception) { return ""; }
        }

        [DataMember]
        private static readonly Dictionary<EnumDBError, string> DicError = new Dictionary<EnumDBError, string>
        {
            { EnumDBError.UnknownError, "Error: "},
            { EnumDBError.RecordExist, "Error: The table already has the similar record! "},
            { EnumDBError.SuchASPAbsent, "Error: There is no such station (ASP)"},
            { EnumDBError.SuchMissionAbsent, "Error: There is no such mission "},
            { EnumDBError.NoColumnAsp, "Error: The table hasn't column AddrAsp"},
            { EnumDBError.EmptyMission, "Error: The table of Mission is empty. Add some mission!"},
            { EnumDBError.NotActiveMission, "Error: The table of Mission hasn't any active mission."},
            { EnumDBError.RecordNotFound, "Error: The record not found! "},
            { EnumDBError.None, ""}
        };
    }
}
