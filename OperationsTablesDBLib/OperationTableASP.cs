﻿using Microsoft.EntityFrameworkCore;
using ModelsTablesDBLib;
using System;
using System.Linq;

namespace OperationsTablesDBLib
{
    public class OperationTableASP : OperationTableDependMissionDb<TableASP>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableASP>(Name);

                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordExist);
                    }

                    if ((record as TableASP).ISOwn)
                    {
                        var OtherRecs = Table.Where(rec => (rec as TableASP).ISOwn == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableASP).ISOwn = false;
                    }
                    DataBase.SaveChanges();
                }

                base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                bool hasChanged = false;

                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableASP>(Name);

                    TableASP rec = Table.Find(record.GetKey());

                    if (rec == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                    }

                    hasChanged = (record as TableASP).ISOwn && !rec.ISOwn;

                    if (hasChanged)
                    {
                        var OtherRecs = Table.Where(t => (t as TableASP).ISOwn == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableASP).ISOwn = false;
                    }

                    DataBase.SaveChanges();
                }
                base.Change(record, idClient);

                return;

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}