﻿using Errors;
using InheritorsEventArgs;
using Microsoft.EntityFrameworkCore;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;


namespace OperationsTablesDBLib
{
    public class OperationTableSourcseFHSS : OperationTableDependTableFHSS<TableSourceFHSS>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<TableSourceFHSS> Table = DataBase.GetTable<TableSourceFHSS>(Name);

                    TableSourceFHSS rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    Table.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    Table.Add(rec);
                    DataBase.SaveChanges();
                    //if (rec.ListJamDirect != null)
                    //{
                    //    DbSet<TableJamDirect> tableJamDirects = DataBase.TJamDirects;
                    //    tableJamDirects.UpdateRange(rec.ListJamDirect);
                    //}
                    
                }

                //SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(ChangeRange)));
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {

                List<TableSourceFHSS> AddRange = new List<TableSourceFHSS>();
                lock (DataBase)
                {
                    DbSet<TableSourceFHSS> Table = DataBase.GetTable<TableSourceFHSS>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableSourceFHSS).ListJamDirect != null)
                            {
                                DbSet<TableJamDirect> tableJamDirects = DataBase.TJamDirects;
                                tableJamDirects.UpdateRange(rec.ListJamDirect);
                            }
                            AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableSourceFHSS);
                            AddRange.Add(record as TableSourceFHSS);
                        }
                    }
                    DataBase.SaveChanges();
                }
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
                //UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
