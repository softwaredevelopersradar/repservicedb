﻿using System;
using System.Linq;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace OperationsTablesDBLib
{
    public class OperationTableDependMissionDb<T> : OperationTableDb<T> where T : AbstractDependentMission
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

            (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке

                base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                foreach (var t in data.ListRecords)
                    (t as AbstractDependentMission).IdMission = CurrentMission.Id;

                base.AddRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                foreach (var t in data.ListRecords)
                    (t as AbstractDependentMission).IdMission = CurrentMission.Id;

                base.RemoveRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

               (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке

                base.Delete(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);


                if (error != Errors.EnumDBError.None && error != Errors.EnumDBError.EmptyMission)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (error == Errors.EnumDBError.EmptyMission)
                        return ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList());
                    // проверка на наличие вложенной таблицы 
                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                        {
                            // если найдена, то испльзуем жадную загрузку связных таблиц
                            return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(rec => rec.IdMission == CurrentMission.Id).Include(property.Name).ToList());
                        }
                    }

                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(rec => rec.IdMission == CurrentMission.Id).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Clear(int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);
                if (error != Errors.EnumDBError.None && error != Errors.EnumDBError.EmptyMission)
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);

                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (error == Errors.EnumDBError.EmptyMission)
                        Table.RemoveRange(Table.ToList());
                    else
                        Table.RemoveRange(Table.Where(t => t.IdMission == CurrentMission.Id).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

            (record as AbstractDependentMission).IdMission = CurrentMission.Id;
                base.Change(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
