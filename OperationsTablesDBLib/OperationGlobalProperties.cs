﻿using System;
using System.Linq;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesDBLib
{
    public class OperationGlobalProperties : OperationTableDb<GlobalProperties>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

            (record as AbstractCommonTable).Id = CurrentMission.Id; // добавляем все в текущей обстановке

                GlobalProperties recFind;
                lock (DataBase)
                {
                    DbSet<GlobalProperties> Table = DataBase.GetTable<GlobalProperties>(Name);
                    recFind = Table.Find(record.GetKey());
                }
                if (recFind != null)
                    base.Change(record, idClient);
                else
                    base.Add(record, idClient);


            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }

        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);
                lock (DataBase)
                {
                    DbSet<GlobalProperties> Table = DataBase.GetTable<GlobalProperties>(Name);

                    if (error == Errors.EnumDBError.EmptyMission)
                        return ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList());

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(rec => rec.Id == CurrentMission.Id).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

            (record as AbstractCommonTable).Id = CurrentMission.Id;

                base.Change(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }

        public override void Clear(int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);
                if (CurrentMission == null)
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                lock (DataBase)
                {
                    DbSet<GlobalProperties> Table = DataBase.GetTable<GlobalProperties>(Name);

                    Table.RemoveRange(Table.Where(t => t.Id == CurrentMission.Id).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }

}
