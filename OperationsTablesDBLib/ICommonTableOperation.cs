﻿using System.ServiceModel;
using ModelsTablesDBLib;
using InheritorsEventArgs;
using InheritorsException;

namespace OperationsTablesDBLib
{
    [ServiceContract(CallbackContract = typeof(ICommonCallback))]
    #region KnownType
    [ServiceKnownType(typeof(ClassDataCommon))]
    [ServiceKnownType(typeof(ClassDataDependASP))]
    [ServiceKnownType(typeof(AbstractCommonTable))]
    [ServiceKnownType(typeof(AbstractDependentASP))]
    [ServiceKnownType(typeof(TableMission))]
    [ServiceKnownType(typeof(TableFreqKnown))]
    [ServiceKnownType(typeof(TableSectorsRanges))]
    [ServiceKnownType(typeof(TableASP))]
    [ServiceKnownType(typeof(TableFreqForbidden))]
    [ServiceKnownType(typeof(TableFreqImportant))]
    [ServiceKnownType(typeof(TableReconFWS))]
    [ServiceKnownType(typeof(TableFreqSpec))]
    [ServiceKnownType(typeof(TableSectorsRangesRecon))]
    [ServiceKnownType(typeof(TableSectorsRangesSuppr))]
    [ServiceKnownType(typeof(TableRoute))]
    [ServiceKnownType(typeof(TableAreaResponsibility))]
    [ServiceKnownType(typeof(TableLandCoordUAV))]
    [ServiceKnownType(typeof(TableEnemyResources))]
    [ServiceKnownType(typeof(TableFrontLineEnemyTroops))]
    [ServiceKnownType(typeof(TableFrontLineOwnTroops))]
    [ServiceKnownType(typeof(TableFrontLine))]
    [ServiceKnownType(typeof(TableOwnResources))]
    [ServiceKnownType(typeof(GlobalProperties))]
    [ServiceKnownType(typeof(TableSuppressFWS))]
    [ServiceKnownType(typeof(TableSuppressFHSS))]
    [ServiceKnownType(typeof(TableFHSSExcludedFreq))]
    [ServiceKnownType(typeof(TableReconFHSS))]
    [ServiceKnownType(typeof(TableSourceFHSS))]
    [ServiceKnownType(typeof(TempFWS))]
    [ServiceKnownType(typeof(TempADSB))]
    [ServiceKnownType(typeof(TempGNSS))]
    [ServiceKnownType(typeof(TempSuppressFHSS))]
    [ServiceKnownType(typeof(TempSuppressFWS))]
    [ServiceKnownType(typeof(ButtonsNAV))]
    [ServiceKnownType(typeof(TableChatMessage))]


    [ServiceKnownType(typeof(NameTable))]
    [ServiceKnownType(typeof(EnumCoordinateSystem))]
    [ServiceKnownType(typeof(TypeCRRX))]
    [ServiceKnownType(typeof(TypeRSD))]
    [ServiceKnownType(typeof(EnumFFTResolution))]
    [ServiceKnownType(typeof(EnumTypeRadioRecon))]
    [ServiceKnownType(typeof(EnumTypeRadioSuppr))]
    [ServiceKnownType(typeof(Languages))]
    [ServiceKnownType(typeof(Led))]
    [ServiceKnownType(typeof(NameChangeOperation))]
    [ServiceKnownType(typeof(NameTableOperation))]
    [ServiceKnownType(typeof(RoleStation))]
    [ServiceKnownType(typeof(SignSender))]
    [ServiceKnownType(typeof(SignSpecFreq))]
    [ServiceKnownType(typeof(SpeedPorts))]
    [ServiceKnownType(typeof(TypeConnection))]
    [ServiceKnownType(typeof(EnumAmplifiers))]
    [ServiceKnownType(typeof(ModulationKondor))]
    [ServiceKnownType(typeof(ChatMessageStatus))]

    [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
    //[ServiceKnownType(typeof(Errors.DBError))]
    //[ServiceKnownType(typeof(InheritorsEventArgs.DataEventArgs))]
    //[ServiceKnownType(typeof(InheritorsEventArgs.AStandardEventArgs))]
    //[ServiceKnownType(typeof(InheritorsEventArgs.OperationTableEventArgs))]
    #endregion

    public interface ICommonTableOperation
    {
        [OperationContract(IsOneWay = true)]
       // [FaultContract(typeof(ExceptionWCF))]
        void ChangeRecord(NameTable nameTable, NameChangeOperation nameAction, AbstractCommonTable record, int IdClient);

        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ExceptionWCF))]
        void AddRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ExceptionWCF))]
        void RemoveRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ExceptionWCF))]
        void ClearTable(NameTable nameTable, int IdClient);
        
        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
        ///<summary>
        ///Загрузка данных для выбранной обстановки
        ///</summary>
        ClassDataCommon LoadData(NameTable nameTable, int IdClient);

        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
        ///<summary>
        ///Загрузка данных из связных таблиц с TableAsp по фильтру - номер АСП
        /// </summary>
        ClassDataDependASP LoadDataFilterASP(NameTable nameTable, int NumberASP,  int IdClient);

        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
        ///<summary>
        ///Загрузка данных из TableRoute по фильтру - номер маршрута
        /// </summary>
        ClassDataCommon LoadDataFilterRoute(int IdClient, int NumberRoute);
        
        [OperationContract(IsOneWay = true)]
        ///<summary>
        ///Очистка данных по фильтру
        /// </summary>
        void ClearTableByFilter(NameTable nameTable, int IdFilter, int IdClient);
    }

    public interface ICommonCallback
    {
        /// <summary>
        /// Получены данные из БД
        /// </summary>
        /// <param name="lData">Данные из таблицы</param>
        [OperationContract(IsOneWay = true)]//(IsOneWay = true)]
                                            //[FaultContract(typeof(ExceptionWCF))]
        void DataCallback(DataEventArgs dataEventArgs);

        [OperationContract(IsOneWay = true)]
        void RecordCallBack(RecordEventArgs recordEventArgs);

        [OperationContract(IsOneWay = true)]
        void RangeCallBack(DataEventArgs dataEventArgs);
    }
}
