﻿using System;
using System.Linq;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesDBLib
{

    public class OperationTableRoute : OperationTableDependMissionDb<TableRoute>, IRouteTable
    {
        //public override ClassDataCommon Load(int idClient)
        //{
        //    ClassDataCommon data = new ClassDataCommon();
        //    try
        //    {
        //        var (CurrentMission, error) = base.CurrentMission(idClient);

        //        if(error == Errors.EnumDBError.EmptyMission)
        //            return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.ListCoordinates).ToList());

        //        if (CurrentMission == null)
        //        {
        //            throw new InheritorsException.ExceptionLocalDB(idClient, error);
        //        }

        //        return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(rec => rec.IdMission == CurrentMission.Id).Include(t => t.ListCoordinates).ToList());
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        public ClassDataCommon LoadByFilter(int idClient, int NumberRoute)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);
                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }
                lock (DataBase)
                {

                    var Table = DataBase.GetTable<TableRoute>(Name);

                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(t => t.Id == NumberRoute).Include(t => t.ListCoordinates).ToList());
                }
                
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
