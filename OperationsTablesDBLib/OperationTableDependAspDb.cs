﻿using System;
using System.Linq;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace OperationsTablesDBLib
{
    public class OperationTableDependAspDb<T> : OperationTableDb<T>, IDependentAsp where T : AbstractDependentASP
    {
        public ClassDataDependASP LoadByFilter(int idClient, int NumberASP)
        {

            ClassDataDependASP data = new ClassDataDependASP();
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);
                if (CurrentMission == null)
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (DataBase.GetTable<TableASP>(NameTable.TableASP).Find(NumberASP, CurrentMission.Id) == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchASPAbsent);

                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                        {
                            // если найдена, то испльзуем жадную загрузку связных таблиц
                            return ClassDataDependASP.ConvertToDataDependASP(Table.Where(t => (t as T).NumberASP == NumberASP && (t as T).IdMission == CurrentMission.Id).Include(property.Name).ToList());
                        }
                    }
                    return ClassDataDependASP.ConvertToDataDependASP(Table.Where(t => (t as T).NumberASP == NumberASP && (t as T).IdMission == CurrentMission.Id).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {

                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

            (record as AbstractDependentASP).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке

                lock (DataBase)
                {
                    var recordAsp = DataBase.GetTable<TableASP>(NameTable.TableASP).Find((record as AbstractDependentASP).NumberASP, (record as AbstractDependentASP).IdMission);

                    if (recordAsp == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchASPAbsent);
                    }
                }
                base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                foreach (var t in data.ListRecords)
                    (t as AbstractDependentASP).IdMission = CurrentMission.Id;

                base.AddRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                foreach (var t in data.ListRecords)
                    (t as AbstractDependentASP).IdMission = CurrentMission.Id;

                base.RemoveRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

            (record as AbstractDependentASP).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке

                lock (DataBase)
                {
                    var recordAsp = DataBase.GetTable<TableASP>(NameTable.TableASP).Find((record as AbstractDependentASP).NumberASP, (record as AbstractDependentASP).IdMission);

                    if (recordAsp == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchASPAbsent);
                    }
                }
                base.Delete(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);
                lock (DataBase)
                {

                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    if (error == Errors.EnumDBError.EmptyMission)
                        return ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList());

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }

                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                        {
                            // если найдена, то испльзуем жадную загрузку связных таблиц
                            return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(rec => rec.IdMission == CurrentMission.Id).Include(property.Name).ToList());
                        }
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(rec => rec.IdMission == CurrentMission.Id).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void ClearByFilter(int idClient, int NumberASP)
        {
            try
            {

                var (CurrentMission, error) = base.CurrentMission(idClient);
                if (CurrentMission == null)
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (DataBase.GetTable<TableASP>(NameTable.TableASP).Find(NumberASP, CurrentMission.Id) == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchASPAbsent);


                    Table.RemoveRange(Table.Where(t => t.NumberASP == NumberASP && t.IdMission == CurrentMission.Id).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Clear(int idClient)
        {
            try
            {

                var (CurrentMission, error) = base.CurrentMission(idClient);
                if (CurrentMission == null)
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);

                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    Table.RemoveRange(Table.Where(t => t.IdMission == CurrentMission.Id).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
