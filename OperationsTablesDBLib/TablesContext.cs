﻿using System;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesDBLib
{
    public class TablesContext : DbContext
    {
        #region DbSet
        public DbSet<TableASP> TASP { get; set; }
        public DbSet<TableSectorsRangesRecon> TSectorsRangesRecon { get; set; }
        public DbSet<TableSectorsRangesSuppr> TSectorsRangesSuppr { get; set; }
        public DbSet<TableFreqForbidden> TFreqForbidden { get; set; }
        public DbSet<TableFreqKnown> TFreqKnown { get; set; }
        public DbSet<TableFreqImportant> TFreqImportant { get; set; }
        public DbSet<TableReconFWS> TReconFWS { get; set; }
        public DbSet<TableMission> TMission { get; set; }
        public DbSet<TableOwnResources> TOwnResources { get; set; }
        public DbSet<TableEnemyResources> TEnemyResources { get; set; }
        public DbSet<TableLandCoordUAV> TLandCoordUAV { get; set; }
        public DbSet<TableFrontLineOwnTroops> TFrontLineOwnTroops { get; set; }
        public DbSet<TableFrontLineEnemyTroops> TFrontLineEnemyTroops { get; set; }
        public DbSet<TableAreaResponsibility> TAreaResponsibility { get; set; }
        public DbSet<TableRoute> TRoute { get; set; }
        public DbSet<GlobalProperties> TProperty { get; set; }
        public DbSet<TableSuppressFWS> TSuppressFWS { get; set; }
        public DbSet<TableSuppressFHSS> TSuppressFHSS { get; set; }
        public DbSet<TableFHSSExcludedFreq> TFHSSExcludedFreq { get; set; }
        public DbSet<TableReconFHSS> TReconFHSS { get; set; }
        public DbSet<TableSourceFHSS> TSourceFHSS { get; set; }
        public DbSet<TableFHSSReconExcluded> TFHSSReconExcluded { get; set; }
        public DbSet<TableJamDirect> TJamDirects { get; set; }
        public DbSet<ButtonsNAV> TButtonsNAV { get; set; }
        public DbSet<TableChatMessage> TChatMessage { get; set; }
        #endregion

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.TableMission:
                    return TMission as DbSet<T>;
                case NameTable.TableASP:
                    return TASP as DbSet<T>;
                case NameTable.TableRoute:
                    return TRoute as DbSet<T>;
                case NameTable.TableOwnResources:
                    return TOwnResources as DbSet<T>;
                case NameTable.TableEnemyResources:
                    return TEnemyResources as DbSet<T>;
                case NameTable.TableLandCoordUAV:
                    return TLandCoordUAV as DbSet<T>;
                case NameTable.TableFrontLineOwnTroops:
                    return TFrontLineOwnTroops as DbSet<T>;
                case NameTable.TableFrontLineEnemyTroops:
                    return TFrontLineEnemyTroops as DbSet<T>;
                case NameTable.TableAreaResponsibility:
                    return TAreaResponsibility as DbSet<T>;
                case NameTable.TableFreqForbidden:
                    return TFreqForbidden as DbSet<T>;
                case NameTable.TableFreqImportant:
                    return TFreqImportant as DbSet<T>;
                case NameTable.TableFreqKnown:
                    return TFreqKnown as DbSet<T>;
                case NameTable.TableSectorsRangesRecon:
                    return TSectorsRangesRecon as DbSet<T>;
                case NameTable.TableSectorsRangesSuppr:
                    return TSectorsRangesSuppr as DbSet<T>;
                case NameTable.TableReconFWS:
                   return TReconFWS as DbSet<T>;
                case NameTable.GlobalProperties:
                    return TProperty as DbSet<T>;
                case NameTable.TableSuppressFWS:
                    return TSuppressFWS as DbSet<T>;
                case NameTable.TableSuppressFHSS:
                    return TSuppressFHSS as DbSet<T>;
                case NameTable.TableFHSSExcludedFreq:
                    return TFHSSExcludedFreq as DbSet<T>;
                case NameTable.TableReconFHSS:
                    return TReconFHSS as DbSet<T>;
                case NameTable.TableSourceFHSS:
                    return TSourceFHSS as DbSet<T>;
                case NameTable.TableFHSSReconExcluded:
                    return TFHSSReconExcluded as DbSet<T>;
                case NameTable.ButtonsNAV:
                    return TButtonsNAV as DbSet<T>;
                case NameTable.TableChat:
                    return TChatMessage as DbSet<T>;
                default:
                    return null;
            }
        }

        public TablesContext()
        {
            try
            {
                SQLitePCL.Batteries.Init();
            }
            catch (Exception ex)
            {
                var a = ex;
                Console.Write($"Error: {ex.Message}");
            }

            // SQLitePCL.raw.SetProvider(new SQLitePCL.SQLite3Provider_e_sqlite3());

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging(true);
            var temp = System.IO.Directory.GetCurrentDirectory();
            var location = System.Reflection.Assembly.GetExecutingAssembly();
            string path = "Filename=" + System.IO.Directory.GetCurrentDirectory() + "\\UniversalDB.db";
            //string path = "Filename=" + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\UniversalDB.db";
            optionsBuilder.UseSqlite(path);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Coord>().Property(rec => rec.Altitude).HasDefaultValue(-1);
            modelBuilder.Entity<Coord>().Property(rec => rec.Latitude).HasDefaultValue(-1);
            modelBuilder.Entity<Coord>().Property(rec => rec.Longitude).HasDefaultValue(-1);

            modelBuilder.Entity<CoordRoute>().OwnsOne(rec => rec.Coordinates);

            modelBuilder.Entity<TableSectorsRangesRecon>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableSectorsRangesSuppr>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableFreqForbidden>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableFreqImportant>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableFreqKnown>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ButtonsNAV>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableSuppressFWS>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableSuppressFWS>().OwnsOne(rec => rec.InterferenceParam);
            modelBuilder.Entity<TableSuppressFWS>().OwnsOne(rec => rec.Coordinates);

            modelBuilder.Entity<TableSuppressFHSS>().HasOne<TableASP>().WithMany().HasForeignKey(rec => new { rec.NumberASP, rec.IdMission }).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableSuppressFHSS>().OwnsOne(rec => rec.InterferenceParam);
            modelBuilder.Entity<TableSuppressFHSS>().Property(rec => rec.StepKHz).HasDefaultValue(100);
            modelBuilder.Entity<TableSuppressFHSS>().HasMany<TableFHSSExcludedFreq>().WithOne().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFHSSExcludedFreq>().HasOne<TableSuppressFHSS>().WithMany()
                .HasForeignKey(t => t.IdFHSS)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<TableASP>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableASP>().HasKey(rec => new { rec.Id, rec.IdMission });
            modelBuilder.Entity<TableASP>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableASP>().OwnsOne(t => t.Sectors);
            modelBuilder.Entity<TableASP>().Property(rec => rec.AntHeightRec).HasDefaultValue(20);
            modelBuilder.Entity<TableASP>().Property(rec => rec.AntHeightSup).HasDefaultValue(10);

            modelBuilder.Entity<TableReconFWS>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableReconFWS>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableReconFWS>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableReconFWS>().HasMany(rec => rec.ListJamDirect);
            modelBuilder.Entity<TableReconFWS>().HasMany(rec => rec.ListJamDirect).WithOne().OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableOwnResources>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableOwnResources>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableOwnResources>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableEnemyResources>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableEnemyResources>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableEnemyResources>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableLandCoordUAV>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableLandCoordUAV>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableLandCoordUAV>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableFrontLineOwnTroops>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableFrontLineOwnTroops>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFrontLineOwnTroops>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableFrontLineEnemyTroops>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableFrontLineEnemyTroops>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFrontLineEnemyTroops>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableAreaResponsibility>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableAreaResponsibility>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableAreaResponsibility>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableReconFHSS>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableReconFHSS>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableReconFHSS>().HasMany<TableSourceFHSS>().WithOne().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableReconFHSS>().HasMany<TableFHSSReconExcluded>().WithOne().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFHSSReconExcluded>().HasOne<TableReconFHSS>().WithMany()
                .HasForeignKey(rec => rec.IdFHSS).HasPrincipalKey(rec => rec.Id);
            modelBuilder.Entity<TableSourceFHSS>().HasOne<TableReconFHSS>().WithMany()
                .HasForeignKey(rec => rec.IdFHSS).HasPrincipalKey(rec => rec.Id);
            modelBuilder.Entity<TableSourceFHSS>().HasMany(rec => rec.ListJamDirect).WithOne().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableSourceFHSS>().OwnsOne(rec => rec.Coordinates);

            modelBuilder.Entity<TableRoute>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.IdMission);
            modelBuilder.Entity<TableRoute>().HasMany(rec => rec.ListCoordinates);
            modelBuilder.Entity<TableRoute>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableRoute>().HasMany(rec => rec.ListCoordinates).WithOne().OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.RangeJamming).HasDefaultValue(EnumRangeJamming.Range_30_1215);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.RangeRadioRecon).HasDefaultValue(EnumRangeRadioRecon.Range_30_3000);
            modelBuilder.Entity<GlobalProperties>().HasOne<TableMission>().WithMany().HasForeignKey(rec => rec.Id);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.PingIntervalFHSAP).HasDefaultValue(5000);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Amplifiers).HasDefaultValue(EnumAmplifiers.Standard);

            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberAveragingPhase).HasDefaultValue(3);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberAveragingBearing).HasDefaultValue(3);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.HeadingAngle).HasDefaultValue(0);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.DetectionFHSS).HasDefaultValue(0);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.SignHeadingAngle).HasDefaultValue(1);

            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.OperationTime).HasDefaultValue(15);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberChannels).HasDefaultValue(4);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberIri).HasDefaultValue(10);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Priority).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Threshold).HasDefaultValue(-80);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeRadiateFWS).HasDefaultValue(800);

            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.SectorSearch).HasDefaultValue(10);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeSearch).HasDefaultValue(300);

            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.FFTResolution).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeRadiateFHSS).HasDefaultValue(900);

            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Latitude).HasDefaultValue(-1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Longitude).HasDefaultValue(-1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.SpoofingVisibility).HasDefaultValue(false);

            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.DetectionAccuracyUAV).HasDefaultValue(10);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeRadiateUAV).HasDefaultValue(10);

            modelBuilder.Entity<TableMission>().Property(rec => rec.IsCurrent).HasDefaultValue(false);

            modelBuilder.Entity<TableJamDirect>().OwnsOne(t => t.JamDirect);
            modelBuilder.Entity<JamDirect>().Property(rec => rec.Bearing).HasDefaultValue(-1);

            modelBuilder.Entity<TableMission>().HasData(new TableMission() { Id = 1, Name = "First", IsCurrent = true });
            modelBuilder.Entity<GlobalProperties>().HasData(new GlobalProperties()
            {
                RangeJamming = EnumRangeJamming.Range_30_1215,
                RangeRadioRecon = EnumRangeRadioRecon.Range_30_3000,
                Id = 1,
                PingIntervalFHSAP = 5000,
                NumberAveragingPhase = 3,
                NumberAveragingBearing = 3,
                HeadingAngle = 0,
                DetectionFHSS = 0,
                SignHeadingAngle = true,
                OperationTime = 15,
                NumberChannels = 4,
                NumberIri = 10,
                Priority = 1,
                Threshold = -80,
                TimeRadiateFWS = 800,
                SectorSearch = 10,
                TimeSearch = 300,
                FFTResolution = 1,
                TimeRadiateFHSS = 900,
                TypeRadioRecon = EnumTypeRadioRecon.WithoutBearing,
                TypeRadioSuppr = EnumTypeRadioSuppr.FWS,
                Latitude = -1,
                Longitude = -1,
                SpoofingVisibility = false,
                Amplifiers = EnumAmplifiers.Standard,
                DetectionAccuracyUAV = 10,
                TimeRadiateUAV = 10
            });

            //modelBuilder.Entity<ButtonsNAV>().HasData(new ButtonsNAV()
            //{
            //    Id = 1,
            //    BIridium = false,
            //    BInmarsat = false,
            //    B2_4 = false,
            //    B5_8 = false,
            //    BGS = false,
            //    B433 = false,
            //    B868 = false,
            //    B920 = false,
            //    BBPLA = false,
            //    BNAV = false

            //});
        }

    }
}
