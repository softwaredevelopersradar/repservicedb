﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModelsTablesDBLib;
using InheritorsEventArgs;

namespace OperationsTablesDBLib
{
    public class OperationTempTable<T> : Operation, ITableAction where T : AbstractCommonTable
    {
        protected static Dictionary<int, T> TempTable;

        public OperationTempTable() : base()
        {
            IsTemp = true;
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
            TempTable = new Dictionary<int, T>();
        }

        public virtual void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                if (record is AbstractDependentMission)
                {
                    var (CurrentMission, error) = base.CurrentMission(idClient);

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                }

                if (TempTable.ContainsKey((int)record.GetKey().FirstOrDefault()))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordExist);
                }
                TempTable.Add((int)record.GetKey().FirstOrDefault(), record as T);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    if (rec is AbstractDependentMission)
                    {
                        var (CurrentMission, error) = base.CurrentMission(idClient);

                        if (CurrentMission == null)
                        {
                            throw new InheritorsException.ExceptionLocalDB(idClient, error);
                        }

                       (rec as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                    }

                    int key = (int)rec.GetKey().FirstOrDefault();
                    if (TempTable.ContainsKey(key))
                        TempTable[key].Update(rec as T);
                    else
                        TempTable.Add(key, rec as T);
                }
                UpDate(idClient);

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    if (rec is AbstractDependentMission)
                    {
                        var (CurrentMission, error) = base.CurrentMission(idClient);

                        if (CurrentMission == null)
                        {
                            throw new InheritorsException.ExceptionLocalDB(idClient, error);
                        }

                       (rec as AbstractDependentMission).IdMission = CurrentMission.Id; // все операции проводим все в текущей обстановке
                    }

                    int key = (int)rec.GetKey().FirstOrDefault();
                    if (TempTable.ContainsKey(key))
                        TempTable.Remove(key);
                }
                UpDate(idClient);

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                if (record is AbstractDependentMission)
                {
                    var (CurrentMission, error) = base.CurrentMission(idClient);

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                }

                int key = (int)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                TempTable[key].Update(record as T);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void Clear(int idClient)
        {
            try
            {
                TempTable.Clear();
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void Delete(AbstractCommonTable record, int idClient) //(ICommonTable record, int idClient)
        {
            try
            {
                if (record is AbstractDependentMission)
                {
                    var (CurrentMission, error) = base.CurrentMission(idClient);

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                }

                int key = (int)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                TempTable.Remove(key);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                return ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Values.ToList());
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpDate(int idClient)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Values.ToList())));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpAddRange(int idClient, ClassDataCommon records)
        {
            try
            {
                base.SendRange(this, new DataEventArgs(Name, records));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
