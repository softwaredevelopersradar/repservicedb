﻿using System;
using System.Linq;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesDBLib
{
    public class OperationTableMission : OperationTableDb<TableMission>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableMission>(Name);

                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordExist);
                    }

                    if ((record as TableMission).IsCurrent)
                    {
                        var OtherRecs = Table.Where(rec => (rec as TableMission).IsCurrent == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableMission).IsCurrent = false;
                    }
                    DataBase.SaveChanges();
                }

                base.Add(record, idClient);

                if ((record as TableMission).IsCurrent)
                    base.SendMissionChanged(this, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                bool hasChanged = false;

                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableMission>(Name);

                    TableMission rec = Table.Find(record.GetKey());

                    if (rec == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                    }

                    hasChanged = (record as TableMission).IsCurrent && !rec.IsCurrent;

                    if (hasChanged)
                    {
                        var OtherRecs = Table.Where(t => (t as TableMission).IsCurrent == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableMission).IsCurrent = false;
                    }

                    DataBase.SaveChanges();
                }
                base.Change(record, idClient);

                if (hasChanged)
                    SendMissionChanged(this, idClient);
                return;

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                bool hasChanged = false;

                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableMission>(Name);


                    TableMission rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);

                    hasChanged = rec.IsCurrent;

                    if (hasChanged)
                    {
                        var CurrentRecs = Table.Where(t => t.IsCurrent == true);
                        foreach (var currentRec in CurrentRecs)
                            currentRec.IsCurrent = false;
                        Table.First().IsCurrent = true;
                    }

                    DataBase.SaveChanges();
                }
                base.Delete(record, idClient);

                if (hasChanged)
                    SendMissionChanged(this, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Clear(int idClient)
        {
            try
            {
                base.Clear(idClient);
                SendMissionChanged(this, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }
    }
}
