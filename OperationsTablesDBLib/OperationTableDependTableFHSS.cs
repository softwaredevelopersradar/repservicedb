﻿using Microsoft.EntityFrameworkCore;
using ModelsTablesDBLib;
using System;
using System.Linq;

namespace OperationsTablesDBLib
{
    public class OperationTableDependTableFHSS<T> : OperationTableDb<T>, IDependTableFHSS where T : AbstractDependentFHSS
    {
        public void ClearByFilter(int idClient, int FhssId)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    Table.RemoveRange(Table.Where(t => t.IdFHSS == FhssId).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
