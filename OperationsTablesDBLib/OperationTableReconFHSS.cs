﻿using System;
using System.Linq;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;
using Errors;
using System.Collections.Generic;
using InheritorsEventArgs;

namespace OperationsTablesDBLib
{
    public class OperationTableReconFHSS : OperationTableDependMissionDb<TableReconFHSS>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id;

                List<TableReconFHSS> ChangeRange = new List<TableReconFHSS>();
                lock (DataBase)
                {
                    DbSet<TableReconFHSS> Table = DataBase.GetTable<TableReconFHSS>(Name);

                    TableReconFHSS rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                    if ((record as TableReconFHSS).IsSelected.HasValue && (record as TableReconFHSS).IsSelected.Value)
                    {
                        foreach (var temp in Table.Where(t => t.IsSelected.HasValue && t.IsSelected.Value))
                        {
                            temp.IsSelected = false;
                            ChangeRange.Add(temp);
                        }
                    }
                    (rec as AbstractCommonTable).Update(record);
                    ChangeRange.Add(rec);
                    Table.Update(rec);
                    DataBase.SaveChanges();
                }
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(ChangeRange)));
                //UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                foreach (var t in data.ListRecords)
                    (t as AbstractDependentMission).IdMission = CurrentMission.Id;
                lock (DataBase)
                {
                    DbSet<TableReconFHSS> Table = DataBase.GetTable<TableReconFHSS>(Name);
                    for (int i = 0; i < data.ListRecords.Count; i++)
                    {
                        TableReconFHSS rec = Table.Find(data.ListRecords[i].GetKey());
                        if (rec == null)
                            Table.Add(data.ListRecords[i] as TableReconFHSS);
                        else
                        {
                            Table.Update(data.ListRecords[i] as TableReconFHSS);
                            data.ListRecords[i].Update(Table.Find(rec.GetKey()));
                        }
                    }
                    DataBase.SaveChanges();
                }
                SendRange(this, new DataEventArgs(Name, data));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }
}
