﻿using System;
using ModelsTablesDBLib;
using InheritorsEventArgs;
using Errors;
using System.Linq;
using System.Threading.Tasks;

namespace OperationsTablesDBLib
{
    public class Operation
    {
        protected static TablesContext DataBase;

        protected NameTable Name;

        public bool IsTemp { get; protected set; }

        public Operation()
        {
            IsTemp = false;
            if (DataBase != null) return;
            DataBase = new TablesContext();
        }

        //~Operation()
        //{
        //    if (DataBase == null) return;
        //    DataBase.Dispose();
        //    DataBase = null;
        //}



        private static TableMission currentMission = null; //Для сокращения обращений в базу добавлен данный объект

        /// <summary>
        /// Возвращает текущую обстановку
        /// </summary>
        /// <returns></returns>
        protected (TableMission, EnumDBError) CurrentMission(int IdClient)
        {
            try
            {
                if (currentMission != null) return (currentMission, EnumDBError.None);
                lock (DataBase)
                {
                    var listMission = DataBase.GetTable<TableMission>(NameTable.TableMission).ToList();
                    if (listMission.Count == 0)
                    {
                        return (null, EnumDBError.EmptyMission);
                    }
                    currentMission = listMission.Where(rec => rec.IsCurrent == true).First();
                    if (currentMission == null)
                    {
                        return (null, EnumDBError.NotActiveMission);
                    }
                    return (currentMission, EnumDBError.None);
                }
            }
            catch (Exception)
            {
                return (null, EnumDBError.UnknownError);
            }
        }

        public static event EventHandler<DataEventArgs> OnReceiveData = (obj, eventArgs) => { };

        public static event EventHandler<DataEventArgs> OnAddRange = (obj, eventArgs) => { };

        public static event EventHandler<RecordEventArgs> OnReceiveRecord = (obj, eventArgs) => { };

        public delegate void DelTMissionChanged(object obj, int IdClient);

        public static event DelTMissionChanged OnTableMissionChanged = (object obj, int IdClient) => { };

        public static event EventHandler<string> OnSendMessToHost = (obj, mes) => { };


        internal void SendUpData(object obj, DataEventArgs data)
        {
            Task.Run(()=> OnReceiveData(obj, data));
        }

        internal void SendRange(object obj, DataEventArgs data)
        {
            Task.Run(() => OnAddRange(obj, data));
        }

        internal void SendUpRecord(object obj, RecordEventArgs eventArgs)
        {
            Task.Run(() => OnReceiveRecord(obj, eventArgs));
        }

        internal void SendMissionChanged(object obj, int IdClient)
        {
            currentMission = null; // при любимых изменениях Обстановки зануляем данный объект, т.к. он должен хранить актуальную информацию о текущей обстановке 
            Task.Run(() => OnTableMissionChanged(obj, IdClient));
        }

        internal void SendMessToHost(object obj, string mess)
        {
            Task.Run(() => OnSendMessToHost(obj, mess));
        }

    }
}
