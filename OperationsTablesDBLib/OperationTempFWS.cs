﻿using System;
using System.Collections.Generic;
using System.Linq;
using InheritorsEventArgs;
using ModelsTablesDBLib;

namespace OperationsTablesDBLib
{
    public class OperationTempFWS:OperationTempTable<TempFWS>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке

                int key = (int)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                List<TempFWS> ChangeRange = new List<TempFWS>();
                if((record as TempFWS).IsSelected.HasValue && (record as TempFWS).IsSelected.Value)
                {
                    foreach (var temp in TempTable.Values.Where(rec => rec.IsSelected.HasValue && rec.IsSelected.Value))
                    {
                        temp.IsSelected = false;
                        ChangeRange.Add(temp);
                        //UpRecord(idClient, temp, NameChangeOperation.Change);
                    }
                }
                TempTable[key].Update(record as TempFWS);
                ChangeRange.Add(TempTable[key] as TempFWS);
                //UpRecord(idClient, record, NameChangeOperation.Change);
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(ChangeRange)));
                //UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                for(int i = 0; i< data.ListRecords.Count; i++)
                {
                    if (data.ListRecords[i] is AbstractDependentMission)
                    {

                        (data.ListRecords[i] as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                    }

                    int key = (int)data.ListRecords[i].GetKey().FirstOrDefault();
                    if (TempTable.ContainsKey(key))
                    {
                        TempTable[key].Update(data.ListRecords[i] as TempFWS);
                        data.ListRecords[i].Update(TempTable[key]);
                        //UpRecord(idClient, rec, NameChangeOperation.Change);
                    }
                    else
                    {
                        TempTable.Add(key, data.ListRecords[i] as TempFWS);
                        //UpRecord(idClient, rec, NameChangeOperation.Add);
                    }
                }
                UpAddRange(idClient, data);
                //UpDate(idClient);

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                if (record is AbstractDependentMission)
                {
                    var (CurrentMission, error) = base.CurrentMission(idClient);

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                }

                if (TempTable.ContainsKey((int)record.GetKey().FirstOrDefault()))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordExist);
                }
                TempTable.Add((int)record.GetKey().FirstOrDefault(), record as TempFWS);
                UpRecord(idClient, record, NameChangeOperation.Add);
                //UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                base.SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
