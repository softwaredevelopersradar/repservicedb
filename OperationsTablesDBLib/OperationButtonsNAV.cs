﻿using System;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace OperationsTablesDBLib
{
    public class OperationButtonsNAV : OperationTableDependAspDb<ButtonsNAV>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                var table = DataBase.GetTable<ButtonsNAV>(Name);
                var asp = (record as AbstractDependentASP).NumberASP; 
                var temp = (ButtonsNAV)record;
                ButtonsNAV recFind;

                lock (DataBase)
                {
                    recFind = table.Where(t => (t as ButtonsNAV).NumberASP == asp).FirstOrDefault();
                }

                if (recFind != null)
                {
                    record.Id = recFind.Id;
                    base.Change(record, idClient);
                }
                else
                    base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }


        //public override void Clear(int idClient)
        //{
        //    try
        //    {
        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

    }
}
