﻿using System;
using System.Collections.Generic;
using System.Linq;
using InheritorsEventArgs;
using ModelsTablesDBLib;

namespace OperationsTablesDBLib
{
    public class OperationTempADSB : Operation, ITableAction// OperationTempTable<TempADSB>
    {
        static Dictionary<int, Queue<TempADSB>> TempTable = new Dictionary<int, Queue<TempADSB>>();
        object locker = new object();

        public OperationTempADSB():base()
        {
            Name = NameTable.TempADSB;
        }

        public void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (locker)
                {
                    if (record is AbstractDependentMission)
                    {
                        var (CurrentMission, error) = base.CurrentMission(idClient);

                        if (CurrentMission == null)
                        {
                            throw new InheritorsException.ExceptionLocalDB(idClient, error);
                        }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                        (record as TempADSB).Id = (record as TempADSB).ICAO.GetHashCode();
                    }

                    if (!TempTable.ContainsKey((int)record.GetKey().FirstOrDefault()))
                    {
                        TempTable.Add((int)record.GetKey().First(), new Queue<TempADSB>());
                    }
                    TempTable[(int)record.GetKey().First()].Enqueue(record as TempADSB);

                    if (TempTable[(int)record.GetKey().First()].Count > 50)
                        TempTable[(int)record.GetKey().First()].Dequeue();
                    UpRecord(idClient, record, NameChangeOperation.Add);
                    //UpDate(idClient, TempTable[(int)record.GetKey().First()]);
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void AddRange(ClassDataCommon data, int idClient)
        {
            throw new NotImplementedException();
        }

        public void RemoveRange(ClassDataCommon data, int idClient)
        {
            throw new NotImplementedException();
        }

        public void Change(AbstractCommonTable record, int idClient)
        {
            throw new NotImplementedException();
        }

        public void Clear(int idClient)
        {
            try
            {
                TempTable.Clear();
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                if (record is AbstractDependentMission)
                {
                    var (CurrentMission, error) = base.CurrentMission(idClient);

                    if (CurrentMission == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, error);
                    }

                    (record as AbstractDependentMission).IdMission = CurrentMission.Id; // добавляем все в текущей обстановке
                }

                int key = (int)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                TempTable.Remove(key);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                List<TempADSB> FullTable = new List<TempADSB>();
                foreach(var temps in TempTable.Values)
                {
                    FullTable.AddRange(temps.ToList());

                }
                return ClassDataCommon.ConvertToListAbstractCommonTable(FullTable);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpDate(int idClient)
        {
            try
            {
                List<TempADSB> FullTable = new List<TempADSB>();
                foreach (var temps in TempTable.Values)
                {
                    FullTable.AddRange(temps.ToList());

                }
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(FullTable)));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpDate(int idClient, Queue<TempADSB> tempADSBs)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(tempADSBs.ToList())));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                base.SendUpRecord(this, new RecordEventArgs( Name, record, action));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }
}
