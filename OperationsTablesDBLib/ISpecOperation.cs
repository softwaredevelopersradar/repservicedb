﻿using ModelsTablesDBLib;

namespace OperationsTablesDBLib
{
    public interface IDependentAsp
    {
        ClassDataDependASP LoadByFilter(int idClient, int NumberASP);

        void ClearByFilter(int idClient, int NumberASP);
    }
       
    public interface IRouteTable
    {
        ClassDataCommon LoadByFilter(int idClient, int NumRoute);
    }

    public interface IDependTableFHSS
    {
        void ClearByFilter(int idClient, int FhssId);
    }
}
