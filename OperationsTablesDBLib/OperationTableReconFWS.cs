﻿using System;
using ModelsTablesDBLib;
using Microsoft.EntityFrameworkCore;
using Errors;
using InheritorsEventArgs;
using System.Collections.Generic;

namespace OperationsTablesDBLib
{
    public class OperationTableReconFWS : OperationTableDependMissionDb<TableReconFWS>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                List<TableReconFWS> ChangeRange = new List<TableReconFWS>();

                lock (DataBase)
                {
                    DbSet<TableReconFWS> Table = DataBase.GetTable<TableReconFWS>(Name);

                    TableReconFWS rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    Table.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    Table.Add(rec);
                    DataBase.SaveChanges();

                    //(rec as AbstractCommonTable).Update(record);
                    ChangeRange.Add(rec);
                    //Table.Update(rec);
                    //if (rec.ListJamDirect != null)
                    //{
                    //    DbSet<TableJamDirect> tableJamDirects = DataBase.TJamDirects;
                    //    tableJamDirects.UpdateRange(rec.ListJamDirect);
                    //}
                    //DataBase.SaveChanges();
                }
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(ChangeRange)));
                //UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var (CurrentMission, error) = base.CurrentMission(idClient);

                if (CurrentMission == null)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, error);
                }

                foreach (var t in data.ListRecords)
                    (t as AbstractDependentMission).IdMission = CurrentMission.Id;

                List<TableReconFWS> AddRange = new List<TableReconFWS>();
                lock (DataBase)
                {
                    DbSet<TableReconFWS> Table = DataBase.GetTable<TableReconFWS>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableReconFWS).ListJamDirect != null)
                            {
                                DbSet<TableJamDirect> tableJamDirects = DataBase.TJamDirects;
                                tableJamDirects.UpdateRange(rec.ListJamDirect);
                            }
                            AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableReconFWS);
                            AddRange.Add(record as TableReconFWS);
                        }
                    }
                    DataBase.SaveChanges();
                }
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
                //UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
