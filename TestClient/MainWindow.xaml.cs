﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using ClientDataBase;
using ModelsTablesDBLib;
using InheritorsEventArgs;
using SectorsRangesControl;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;

namespace TestClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";

            UserControlSectorsRanges.OnAddSectorRange += UserControlSectorsRanges_OnAddSectorRange;
            UserControlSectorsRanges.OnDeleteSectorRange += UserControlSectorsRanges_OnDeleteSectorRange;
            UserControlSectorsRanges.OnClearSectorsRanges += UserControlSectorsRanges_OnClearSectorsRanges;
            UserControlSectorsRanges.OnChangeSectorRange += UserControlSectorsRanges_OnChangeSectorRange;

        }

        private async void UserControlSectorsRanges_OnChangeSectorRange(TableSectorsRanges sectorRange)
        {
            await clientDB.Tables[NameTable.TableSectorsRangesRecon].ChangeAsync(sectorRange.ToRangesRecon());
        }

        private async void UserControlSectorsRanges_OnClearSectorsRanges()
        {
            await clientDB.Tables[NameTable.TableSectorsRangesRecon].CLearAsync();
        }

        private async void UserControlSectorsRanges_OnDeleteSectorRange(TableSectorsRanges sectorRange)
        {
            await clientDB.Tables[NameTable.TableSectorsRangesRecon].DeleteAsync(sectorRange.ToRangesRecon());
        }

        private async void UserControlSectorsRanges_OnAddSectorRange(TableSectorsRanges sectorRange)
        {
            await clientDB.Tables[NameTable.TableSectorsRangesRecon].AddAsync(sectorRange.ToRangesRecon());
        }

        private void ButConnDisconn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
                else
                {
                    clientDB = new ClientDB(this.Name, endPoint);
                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exceptClient.Message);
            }
        }

        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
            (clientDB.Tables[NameTable.TableAreaResponsibility] as ITableUpdate<TableAreaResponsibility>).OnUpTable += MainWindow_OnUpTable;
            (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord += MainWindow_OnAddRecord;
            (clientDB.Tables[NameTable.TempFWS] as ITableUpRecord<TempFWS>).OnAddRecord += HandlerAddTempFWS;
            (clientDB.Tables[NameTable.TempFWS] as ITableUpRecord<TempFWS>).OnChangeRecord += HandlerChangeFWS;
            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += HandlerAddRange;
            (clientDB.Tables[NameTable.TableReconFWS] as ITableAddRange<TableReconFWS>).OnAddRange += HandlerAddRange_ReconFWS;
            (clientDB.Tables[NameTable.ButtonsNAV] as ITableUpdate<ButtonsNAV>).OnUpTable += MainWindow_OnUpTable1; ;
        }

        private void MainWindow_OnUpTable1(object sender, TableEventArs<ButtonsNAV> e)
        {
            var t = 0;
        }

        private void HandlerAddRange_ReconFWS(object sender, TableEventArs<TableReconFWS> e)
        {

        }

        private void HandlerAddRange(object sender, TableEventArs<TempFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                foreach (var rec in e.Table)
                {
                    tboxResponce.AppendText($"Change TempFWS: Id: {rec.Id}, Selected: {rec.IsSelected}\n");
                }
            });
        }

        private void HandlerChangeFWS(object sender, TempFWS e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tboxResponce.AppendText($"Change TempFWS: Id: {e.Id}, Selected: {e.IsSelected}\n");
            });
        }

        private void HandlerAddTempFWS(object sender, TempFWS e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tboxResponce.AppendText($"Add TempFWS: Id: {e.Id}, Selected: {e.IsSelected}\n");
            });
        }

        Dictionary<string, List<Coord>> DicADSBCoord = new Dictionary<string, List<Coord>>();
        private async void MainWindow_OnAddRecord(object sender, TempADSB e)
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (!DicADSBCoord.ContainsKey(e.ICAO))
                    {
                    //tboxResponce.AppendText($"Add NEW ICAO: {e.ICAO} ");

                    DicADSBCoord.Add(e.ICAO, new List<Coord>());
                    }

                    DicADSBCoord[e.ICAO].Add(e.Coordinates);
                    tboxResponce.AppendText($"{DateTime.Now.ToLongTimeString()} : Callback TemAdsb: {e.ICAO}, Counts: {DicADSBCoord[e.ICAO].Count}\n");
                });
            });
        }

        private void MainWindow_OnUpTable(object sender, TableEventArs<TableAreaResponsibility> e)
        {
            tboxResponce.Foreground = Brushes.Brown;

            tboxResponce.AppendText($"{DateTime.Now.ToLongTimeString()} : Callback data: {NameTable.TableAreaResponsibility} : - {e.Table.Count} \n");
            // MessageBox.Show($"It works!!!{e.Table.Count}");
            //throw new NotImplementedException();
        }

        async void HandlerConnect(object obj, ClientEventArgs eventArgs)
        {
            butConnDisconn.Content = "Disconnect";
            tboxResponce.AppendText(eventArgs.GetMessage + "\n");

            dynamic table = await clientDB.Tables[NameTable.TableAreaResponsibility].LoadAsync<ModelsTablesDBLib.TableAreaResponsibility>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableAreaResponsibility).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableASP].LoadAsync<ModelsTablesDBLib.TableASP>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableASP).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableEnemyResources].LoadAsync<ModelsTablesDBLib.TableEnemyResources>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableEnemyResources).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFHSSExcludedFreq].LoadAsync<ModelsTablesDBLib.TableFHSSExcludedFreq>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFHSSExcludedFreq).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<ModelsTablesDBLib.TableFreqForbidden>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFreqForbidden).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFreqImportant].LoadAsync<ModelsTablesDBLib.TableFreqImportant>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFreqImportant).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<ModelsTablesDBLib.TableFreqKnown>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFreqKnown).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFrontLineEnemyTroops].LoadAsync<ModelsTablesDBLib.TableFrontLineEnemyTroops>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFrontLineEnemyTroops).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFrontLineOwnTroops].LoadAsync<ModelsTablesDBLib.TableFrontLineOwnTroops>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFrontLineOwnTroops).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableLandCoordUAV].LoadAsync<ModelsTablesDBLib.TableLandCoordUAV>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableLandCoordUAV).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableMission].LoadAsync<ModelsTablesDBLib.TableLandCoordUAV>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableMission).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableOwnResources].LoadAsync<ModelsTablesDBLib.TableOwnResources>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableOwnResources).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<ModelsTablesDBLib.TableReconFHSS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableReconFHSS).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<ModelsTablesDBLib.TableReconFWS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableReconFWS).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableRoute].LoadAsync<ModelsTablesDBLib.TableRoute>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableRoute).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableSectorsRangesRecon].LoadAsync<ModelsTablesDBLib.TableSectorsRangesRecon>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableSectorsRangesRecon).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableSectorsRangesSuppr].LoadAsync<ModelsTablesDBLib.TableSectorsRangesSuppr>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableSectorsRangesSuppr).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<ModelsTablesDBLib.TableSourceFHSS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableSourceFHSS).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableFHSSReconExcluded].LoadAsync<TableFHSSReconExcluded>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableFHSSReconExcluded).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableSuppressFHSS].LoadAsync<ModelsTablesDBLib.TableSuppressFHSS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableSuppressFHSS).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TableSuppressFWS].LoadAsync<ModelsTablesDBLib.TableSuppressFWS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableSuppressFWS).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TempADSB].LoadAsync<ModelsTablesDBLib.TempADSB>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TempADSB).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.TempFWS].LoadAsync<ModelsTablesDBLib.TempFWS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TempFWS).ToString()} count records - {table.Count} \n");

            string testStr = JsonConvert.SerializeObject(table);
            byte[] testBytes = Encoding.ASCII.GetBytes(testStr);

            table = await clientDB.Tables[NameTable.TempSuppressFWS].LoadAsync<ModelsTablesDBLib.TempSuppressFWS>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TempSuppressFWS).ToString()} count records - {table.Count} \n");

            table = await clientDB.Tables[NameTable.ButtonsNAV].LoadAsync<ModelsTablesDBLib.ButtonsNAV>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.ButtonsNAV).ToString()} count records - {table.Count} \n");


            table = await clientDB.Tables[NameTable.TableChat].LoadAsync<ModelsTablesDBLib.TableChatMessage>();
            tboxResponce.AppendText($"Load data from Db. {(NameTable.TableChat).ToString()} count records - {table.Count} \n");
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                butConnDisconn.Content = "Connect";
            if (eventArgs.GetMessage != "")
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(eventArgs.GetMessage);
            }
            clientDB = null;
            });
        }

        async void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {

                    tboxResponce.Foreground = Brushes.Black;

                    tboxResponce.AppendText($"{DateTime.Now.ToLongTimeString()} : Callback data: {eventArgs.Name.ToString()} : {eventArgs.AbstractData.ListRecords.Count} \n");
                    try
                    {
                        string testStr = JsonConvert.SerializeObject(eventArgs.AbstractData.ListRecords);
                        byte[] testBytes = Encoding.ASCII.GetBytes(testStr);
                        //var testObj = JsonConvert.DeserializeObject<List<TableASP>>(testStr);
                    }
                    catch
                    { }
                    if (eventArgs.Name == NameTable.TableSectorsRangesRecon)
                    { ucSectorsRangesRecon.AddSectorsRangesToControl(eventArgs.AbstractData.ToList<TableSectorsRanges>()); }
                });
            });
        }

        void HandlerErrorDataBase(object obj, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(eventArgs.GetMessage);
            });
        }

        int index = 1;

        private void ButAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
                var NumMission = Convert.ToInt32(FiltrMission.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TempADSB:
                        record = new TempADSB
                        {
                            Time = DateTime.Now.ToLongTimeString(),
                            Coordinates = new Coord
                            {
                                Altitude = rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            },
                            ICAO = AddrAsp.ToString(),
                            Id = AddrAsp.ToString().GetHashCode()

                        };
                        break;
                    case NameTable.TempGNSS:
                        record = new TempGNSS()
                        {
                            Id = 1,
                            Location = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            CmpPA = rand.Next(0, 360),
                            CmpRR = rand.Next(0, 360),
                            AntennaHeight = rand.NextDouble(),
                            LocalTime = DateTime.Now,
                            UtcTime = DateTime.UtcNow,
                            NumberOfSatellites = rand.Next()
                        };
                        break;
                    case NameTable.TempSuppressFHSS:
                        record = new TempSuppressFHSS()
                        {
                            Id = rand.Next(1, 10000),
                            Control = Led.Blue,
                            Radiation = Led.Green,
                            Suppress = Led.Red
                        };
                        break;
                    case NameTable.TableSectorsRangesRecon:
                        record = new TableSectorsRangesRecon
                        {
                            Id = rand.Next(1, 10000),
                            NumberASP = AddrAsp,
                            IdMission = NumMission,
                            AngleMax = (short)rand.Next(101, 360),
                            AngleMin = (short)rand.Next(0, 100),
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableSectorsRangesSuppr:
                        record = new TableSectorsRangesSuppr
                        {
                            Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            NumberASP = AddrAsp,
                            AngleMax = (short)rand.Next(101, 360),
                            AngleMin = (short)rand.Next(0, 100),
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            NumberASP = AddrAsp,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableFreqImportant:
                        record = new TableFreqImportant
                        {
                            Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            NumberASP = AddrAsp,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            Id = rand.Next(1, 10000),
                            IdMission = NumMission,
                            NumberASP = AddrAsp,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableReconFWS:
                        record = new TableReconFWS
                        {
                            //Id = rand.Next(1, 100000),
                            IdMission = NumMission,
                            ASPSuppr = rand.Next(0, 100),
                            Deviation = rand.Next(0, 100),
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 100),
                                Latitude = rand.Next(0, 100),
                                Longitude = rand.Next(0, 100),
                            },
                            FreqKHz = rand.Next(0, 100),
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } }
                            },
                            Time = DateTime.Now,
                        };
                        break;
                    case NameTable.TempFWS:
                        record = new TempFWS
                        {
                            Id = rand.Next(1, 100),
                            IdMission = NumMission,
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },
                            Control = (Led)((byte)rand.Next(0, 5)),
                            Deviation = rand.Next(0, 250),
                            FreqKHz = rand.Next(150000, 250000),
                            ListQ = new ObservableCollection<JamDirect>()
                        {
                            { new JamDirect() { Bearing = rand.Next(0,360), DistanceKM =rand.Next(1,1000), Level = (short)rand.Next(-120,0), NumberASP = rand.Next(1,100), IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))), Std = rand.Next(0,1)/10.0f} },

                            { new JamDirect() { Bearing = rand.Next(0,360), DistanceKM =rand.Next(1,1000), Level = (short)rand.Next(-120,0), NumberASP = rand.Next(1,100), IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))), Std = rand.Next(0,1)/10.0f} }
                        },
                            Time = DateTime.Now,
                            Type = (byte)rand.Next(0, 255),
                            IsSelected = false
                        };
                        index++;
                        break;
                    case NameTable.TableASP:
                        record = new TableASP
                        {
                            Id = AddrAsp,
                            IdMission = NumMission,
                            ISOwn = true,
                            CallSign = rand.Next(1, 4).ToString() + "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        };
                        break;
                    case NameTable.TableMission:
                        record = new TableMission
                        {
                            Id = NumMission,
                            Name = NumMission.ToString(),
                            IsCurrent = true
                        };
                        break;
                    case NameTable.TableRoute:
                        record = new TableRoute
                        {
                            //Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Caption = "",

                            ListCoordinates = new List<CoordRoute>()
                    {
                        { new CoordRoute() {Coordinates= new Coord(){Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360)} } },

                        { new CoordRoute(){Coordinates= new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360)} } },
                        { new CoordRoute(){Coordinates= new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360)} } },
                        { new CoordRoute(){Coordinates= new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360)} } },
                        { new CoordRoute(){Coordinates= new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360)} } }
                    }
                        };
                        break;
                    case NameTable.TableAreaResponsibility:
                        record = new TableAreaResponsibility()
                        {
                            // Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Num = (byte)rand.Next(0, 255),
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) }
                        };
                        break;
                    case NameTable.TableLandCoordUAV:
                        record = new TableLandCoordUAV()
                        {
                            // Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Name = rand.Next(1, 1000).ToString(),
                            Image = "Image",
                            Caption = "Caption",
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) }
                        };
                        break;
                    case NameTable.TableEnemyResources:
                        record = new TableEnemyResources()
                        {
                            //Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Name = rand.Next(1, 1000).ToString(),
                            Image = "Image",
                            Caption = "Caption",
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            FreqMax = rand.Next(1, 1000),
                            FreqMin = rand.Next(1, 1000)
                        };
                        break;
                    case NameTable.TableFrontLineEnemyTroops:
                        record = new TableFrontLineEnemyTroops()
                        {
                            // Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            Num = (byte)rand.Next(0, 255)
                        };
                        break;
                    case NameTable.TableFrontLineOwnTroops:
                        record = new TableFrontLineOwnTroops()
                        {
                            //Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            Num = (byte)rand.Next(0, 255)
                        };
                        break;
                    case NameTable.TableOwnResources:
                        record = new TableOwnResources()
                        {
                            //Id = rand.Next(1, 1000),
                            IdMission = NumMission,
                            Name = rand.Next(1, 1000).ToString(),
                            Image = "Image",
                            Caption = "Caption",
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) }
                        };
                        break;
                    case NameTable.TableSuppressFWS:
                        record = new TableSuppressFWS()
                        {
                            Id = rand.Next(1, 10000),
                            NumberASP = AddrAsp,
                            IdMission = NumMission,
                            Bearing = (short)rand.Next(0, 359),
                            FreqKHz = rand.Next(500, 300000000),
                            Letter = (byte)rand.Next(1, 10),
                            Threshold = (byte)rand.Next(0, 150),
                            Sender = SignSender.OtherTable,
                            Priority = (byte)rand.Next(0, 255),
                            InterferenceParam = new InterferenceParam()
                            {
                                Deviation = (byte)rand.Next(0, 255),
                                Duration = (byte)rand.Next(0, 255),
                                Manipulation = (byte)rand.Next(0, 255),
                                Modulation = (byte)rand.Next(0, 255)
                            }
                        };
                        break;
                    case NameTable.TempSuppressFWS:
                        record = new TempSuppressFWS()
                        {
                            Id = rand.Next(1, 10000),
                            NumberASP = AddrAsp,
                            IdMission = NumMission,
                            Control = (Led)(byte)rand.Next(0, 5),
                            Radiation = (Led)(byte)rand.Next(0, 5),
                            Suppress = (Led)(byte)rand.Next(0, 5)
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties();
                        break;
                    case NameTable.TableSuppressFHSS:
                        record = new TableSuppressFHSS()
                        {
                            Id = rand.Next(1, 100000),
                            EPO = new byte[] { 1, 2 },
                            NumberASP = AddrAsp,
                            FreqMaxKHz = rand.Next(100000, 100000000) / 10.0d,
                            FreqMinKHz = rand.Next(100000, 100000000) / 10.0d,
                            InterferenceParam = new InterferenceParam()
                            {
                                Deviation = (byte)rand.Next(0, 255),
                                Duration = (byte)rand.Next(0, 255),
                                Manipulation = (byte)rand.Next(0, 255),
                                Modulation = (byte)rand.Next(0, 255)
                            },
                            Letters = new byte[] { 3, 4 },
                            Threshold = (-1) * rand.Next(0, 1800) / 10.0d
                            //ListExcludedFreq = new ObservableCollection<TableFHSSExcludedFreq>
                            //{
                            //     new TableFHSSExcludedFreq
                            //     {FreqKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            //     Deviation = rand.Next(10, 1000) / 10.0f
                            //     },
                            //     new TableFHSSExcludedFreq
                            //     {FreqKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            //     Deviation = rand.Next(10, 1000) / 10.0f
                            //     },
                            //     new TableFHSSExcludedFreq
                            //     {FreqKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            //     Deviation = rand.Next(10, 1000) / 10.0f
                            //     }
                            //}
                        };
                        break;
                    case NameTable.TableFHSSExcludedFreq:
                        record = new TableFHSSExcludedFreq()
                        {
                            IdFHSS = rand.Next(1, 100000),
                            FreqKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            Deviation = rand.Next(10, 1000) / 10.0f
                        };
                        break;
                    case NameTable.TableReconFHSS:
                        record = new TableReconFHSS()
                        {
                            Deviation = rand.Next(10, 1000) / 10.0f,
                            FreqMaxKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            FreqMinKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            ImpulseDuration = (ushort)rand.Next(1, 100000),
                            Modulation = (byte)rand.Next(0, 255),
                            QuantitySignal = (short)rand.Next(1, 100000),
                            StepKHz = (byte)rand.Next(0, 255),
                            Time = DateTime.Now
                        };
                        break;
                    case NameTable.TableSourceFHSS:
                        record = new TableSourceFHSS()
                        {
                            Coordinates = new Coord
                            {
                                Altitude = rand.NextDouble() * rand.Next(0, 3600),
                                Latitude = rand.NextDouble() * rand.Next(0, 3600),
                                Longitude = rand.NextDouble() * rand.Next(0, 3600)
                            },
                            ListJamDirect = new ObservableCollection<TableJamDirect>
                            {
                                 new TableJamDirect()
                                {
                                     JamDirect = new JamDirect()
                                     {
                                         Bearing = rand.Next(10, 1000) / 10.0f,
                                         DistanceKM = rand.Next(10, 1000) / 10.0f,
                                         Std = rand.Next(10, 1000) / 10.0f,
                                         IsOwn = true,
                                         Level = (short)rand.Next(-120, 0),
                                         NumberASP = rand.Next(1, 999)
                                     }
                                 },
                                new TableJamDirect()
                                {
                                    JamDirect = new JamDirect()
                                    {
                                        Bearing = rand.Next(10, 1000) / 10.0f,
                                        DistanceKM = rand.Next(10, 1000) / 10.0f,
                                        Std = rand.Next(10, 1000) / 10.0f,
                                        IsOwn = true,
                                        Level = (short)rand.Next(-120, 0),
                                        NumberASP = rand.Next(1, 999)
                                     }
                                 },

                                 new TableJamDirect()
                                {
                                     JamDirect = new JamDirect()
                                     {
                                         Bearing = rand.Next(10, 1000) / 10.0f,
                                         DistanceKM = rand.Next(10, 1000) / 10.0f,
                                         Std = rand.Next(10, 1000) / 10.0f,
                                         IsOwn = true,
                                         Level = (short)rand.Next(-120, 0),
                                         NumberASP = rand.Next(1, 999)
                                     }
                                 }
                            }
                        };
                        break;
                    case NameTable.TableFHSSReconExcluded:
                        record = new TableFHSSReconExcluded()
                        {
                            FreqKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                            Deviation = rand.Next(10, 1000) / 10.0f
                        };
                        break;
                    case NameTable.ButtonsNAV:
                        record = new ButtonsNAV()
                        {
                            Id = 1,
                            NumberASP = AddrAsp,
                            IdMission = NumMission,
                            B2_4 = true,
                            BNAV = true
                        };
                        break;
                }
                if (record != null)
                    clientDB?.Tables[(NameTable)Tables.SelectedItem].AddAsync(record);
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exceptClient.Message);
            }
        }

        private void ButDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
                var NumMission = Convert.ToInt32(FiltrMission.Text);

                if ((NameTable)Tables.SelectedItem == NameTable.TableASP)
                {
                    TableASP tableASP = new TableASP
                    {
                        Id = AddrAsp,
                        IdMission = NumMission
                    };
                    clientDB.Tables[(NameTable)Tables.SelectedItem].DeleteAsync(tableASP);
                }
                if ((NameTable)Tables.SelectedItem == NameTable.TableMission)
                {
                    TableMission tableMission = new TableMission
                    {
                        Id = NumMission
                    };
                    clientDB.Tables[(NameTable)Tables.SelectedItem].DeleteAsync(tableMission);
                }
                if ((NameTable)Tables.SelectedItem == NameTable.TableReconFWS)
                {
                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = 4
                    };
                    clientDB.Tables[(NameTable)Tables.SelectedItem].DeleteAsync(tableReconFWS);
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exceptClient.Message);
            }
        }

        private void ButClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clientDB.Tables[(NameTable)Tables.SelectedItem].CLearAsync();
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exceptClient.Message);
            }
        }

        class ClassAir
        {
            public Coord Coordinates { get; set; } = new Coord();
        }

        private async void ButLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tboxResponce.Foreground = Brushes.Black;
                if ((NameTable)Tables.SelectedItem == NameTable.TableSectorsRangesRecon)
                {
                        var records = await clientDB.Tables[(NameTable)Tables.SelectedItem].LoadAsync<TableSectorsRanges>();
                    ucSectorsRangesRecon.AddSectorsRangesToControl(records);
                    tboxResponce.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {records.Count} \n");
                    return;
                }

                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)Tables.SelectedItem;
                if (nameTable == NameTable.TempADSB)
                {
                    List<TempADSB> FullTable = await clientDB.Tables[nameTable].LoadAsync<TempADSB>();
                    Dictionary<string, List<ClassAir>> dictAir = new Dictionary<string, List<ClassAir>>();
                    var icaos = (from t in FullTable let icao = t.ICAO select icao).Distinct().ToList();
                    tboxResponce.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()}\n");
                    foreach (var icao in icaos)
                    {
                        dictAir.Add(icao, new List<ClassAir>());
                        dictAir[icao].AddRange(FullTable.Where(rec => rec.ICAO == icao).Select(rec => new ClassAir() { Coordinates = rec.Coordinates }).ToList());

                        tboxResponce.AppendText($" ICAO: {icao} count records - {dictAir[icao].Count} \n");
                    }
                    //{ table = clientDB.Tables[nameTable].Load<AbstractCommonTable>(); });
                    return;
                }
                table = await clientDB.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                //await Task.Run(() =>
                //{ table = clientDB.Tables[nameTable].Load<AbstractCommonTable>(); });
                tboxResponce.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {table.Count} \n");

            }
            catch (ClientDataBase.Exceptions.ExceptionClient exeptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exeptClient.Message);
            }
            catch (ClientDataBase.Exceptions.ExceptionDatabase excpetService)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(excpetService.Message);
            }
        }

        private void butLoadFilter_Click(object sender, RoutedEventArgs e)
        {

            if (FiltrAsp.Text == "")
                return;
            try
            {
                var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
                var NumMission = Convert.ToInt32(FiltrMission.Text);
                var NumRoute = Convert.ToInt32(NumberRoute.Text);
                tboxResponce.Foreground = Brushes.Black;
                if ((NameTable)Tables.SelectedItem == NameTable.TableRoute)
                {
                    var route = (clientDB.Tables[(NameTable)Tables.SelectedItem] as ITableRoute).LoadByFilter(NumRoute);
                    tboxResponce.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {route.Count} \n");
                    return;
                }

                if (clientDB.Tables[(NameTable)Tables.SelectedItem] is IDependentAsp)
                {
                    if ((NameTable)Tables.SelectedItem == NameTable.TableSectorsRangesRecon)
                    {
                        var records = (clientDB.Tables[(NameTable)Tables.SelectedItem] as IDependentAsp).LoadByFilter<TableSectorsRanges>(AddrAsp);
                        ucSectorsRangesRecon.AddSectorsRangesToControl(records);
                        tboxResponce.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {records.Count} \n");
                        return;
                    }
                    var tablDepenAsp = (clientDB.Tables[(NameTable)Tables.SelectedItem] as IDependentAsp).LoadByFilter<AbstractDependentASP>(AddrAsp);

                    tboxResponce.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {tablDepenAsp.Count} \n");
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exeptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exeptClient.Message);
            }
            catch (ClientDataBase.Exceptions.ExceptionDatabase excpetService)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(excpetService.Message);
            }
        }

        private void butClearFilter_Click(object sender, RoutedEventArgs e)
        {
            if (FiltrAsp.Text == "")
                return;
            try
            {
                var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
                tboxResponce.Foreground = Brushes.Black;

                if (clientDB.Tables[(NameTable)Tables.SelectedItem] is IDependentAsp)
                {
                    (clientDB.Tables[(NameTable)Tables.SelectedItem] as IDependentAsp).ClearByFilter(AddrAsp);
                }
                if (clientDB.Tables[(NameTable)Tables.SelectedItem] is IDependentFHSS)
                {
                    (clientDB.Tables[(NameTable)Tables.SelectedItem] as IDependentFHSS).ClearByFilter(AddrAsp);
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exeptClient)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(exeptClient.Message);
            }
            catch (ClientDataBase.Exceptions.ExceptionDatabase excpetService)
            {
                tboxResponce.Foreground = Brushes.Red;
                tboxResponce.AppendText(excpetService.Message);
            }
        }

        private void SendRange_Click(object sender, RoutedEventArgs e)
        {
            var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
            Random rand = new Random();
            if ((NameTable)Tables.SelectedItem == NameTable.TempFWS)
            {
                List<TempFWS> listFWS = new List<TempFWS>();
                for (int i = 0; i < 8; i++)
                {
                    TempFWS record = new TempFWS
                    {
                        Id = rand.Next(1, 100),
                        Coordinates = new Coord(),
                        Deviation = rand.Next(0, 250),
                        FreqKHz = rand.Next(150000, 250000),
                        ListQ = new ObservableCollection<JamDirect>()
                        {
                            { new JamDirect() { Bearing = 1, DistanceKM =1, Level = 1, NumberASP = 1, IsOwn = true, Std = 1.00f} },

                            { new JamDirect() { Bearing = 1, DistanceKM =1, Level = 1, NumberASP = 1, IsOwn = true, Std = 1.00f} }
                        },
                        Time = DateTime.Now,
                        Type = (byte)rand.Next(0, 255)
                    };
                    listFWS.Add(record);
                    index++;
                }
                clientDB.Tables[NameTable.TempFWS].AddRangeAsync(listFWS);
                return;
            }
            if ((NameTable)Tables.SelectedItem == NameTable.TableSectorsRangesRecon)
            {
                List<TableSectorsRangesRecon> lsect = new List<TableSectorsRangesRecon>()
            {
                { new TableSectorsRangesRecon(){ Id=0, NumberASP = AddrAsp, FreqMinKHz = rand.Next(30000,100000), FreqMaxKHz=rand.Next(110000,1100000)} },
                { new TableSectorsRangesRecon(){ Id=0, NumberASP = AddrAsp, FreqMinKHz = rand.Next(30000,100000), FreqMaxKHz=rand.Next(110000,1100000)} },
                { new TableSectorsRangesRecon(){ Id=0, NumberASP = AddrAsp, FreqMinKHz = rand.Next(30000,100000), FreqMaxKHz=rand.Next(110000,1100000)} },
                { new TableSectorsRangesRecon(){ Id=0, NumberASP = AddrAsp, FreqMinKHz = rand.Next(30000,100000), FreqMaxKHz=rand.Next(110000,1100000)} }
            };
                clientDB.Tables[NameTable.TableSectorsRangesRecon].AddRangeAsync(lsect);
                return;
            }

            if ((NameTable)Tables.SelectedItem == NameTable.TableReconFWS)
            {
                List<TableReconFWS> lsect = new List<TableReconFWS>()
            {
                {
                        new TableReconFWS
                        {
                            ASPSuppr = rand.Next(0, 100),
                            Deviation = rand.Next(0, 100),
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 100),
                                Latitude = rand.Next(0, 100),
                                Longitude = rand.Next(0, 100),
                            },
                            FreqKHz = rand.Next(0, 100),
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } }
                            },
                            Time = DateTime.Now,
                        }
                    },
                {
                        new TableReconFWS
                        {
                            ASPSuppr = rand.Next(0, 100),
                            Deviation = rand.Next(0, 100),
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 100),
                                Latitude = rand.Next(0, 100),
                                Longitude = rand.Next(0, 100),
                            },
                            FreqKHz = rand.Next(0, 100),
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } }
                            },
                            Time = DateTime.Now,
                        }
                    },
                {
                        new TableReconFWS
                        {
                            ASPSuppr = rand.Next(0, 100),
                            Deviation = rand.Next(0, 100),
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 100),
                                Latitude = rand.Next(0, 100),
                                Longitude = rand.Next(0, 100),
                            },
                            FreqKHz = rand.Next(0, 100),
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } }
                            },
                            Time = DateTime.Now,
                        }
                    },
                {
                        new TableReconFWS
                        {
                            ASPSuppr = rand.Next(0, 100),
                            Deviation = rand.Next(0, 100),
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0, 100),
                                Latitude = rand.Next(0, 100),
                                Longitude = rand.Next(0, 100),
                            },
                            FreqKHz = rand.Next(0, 100),
                            ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = (short)rand.Next(0, 100), DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } }
                            },
                            Time = DateTime.Now,
                        }
                    }
                };
                clientDB.Tables[NameTable.TableReconFWS].AddRangeAsync(lsect);
                return;
            }
            if ((NameTable)Tables.SelectedItem == NameTable.TableReconFHSS)
            {
                List<TableReconFHSS> lFHSS = new List<TableReconFHSS>()
                {
                    new TableReconFHSS()
                    {
                        Deviation = rand.Next(10, 1000) / 10.0f,
                        FreqMaxKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        FreqMinKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        ImpulseDuration = (ushort)rand.Next(1, 100000),
                        Modulation = (byte)rand.Next(0, 255),
                        QuantitySignal = (short)rand.Next(1, 100000),
                        StepKHz = (byte)rand.Next(0, 255),
                        Time = DateTime.Now
                    },
                    new TableReconFHSS()
                    {
                        Deviation = rand.Next(10, 1000) / 10.0f,
                        FreqMaxKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        FreqMinKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        ImpulseDuration = (ushort)rand.Next(1, 100000),
                        Modulation = (byte)rand.Next(0, 255),
                        QuantitySignal = (short)rand.Next(1, 100000),
                        StepKHz = (byte)rand.Next(0, 255),
                        Time = DateTime.Now
                    },
                    new TableReconFHSS()
                    {
                        Deviation = rand.Next(10, 1000) / 10.0f,
                        FreqMaxKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        FreqMinKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        ImpulseDuration = (ushort)rand.Next(1, 100000),
                        Modulation = (byte)rand.Next(0, 255),
                        QuantitySignal = (short)rand.Next(1, 100000),
                        StepKHz = (byte)rand.Next(0, 255),
                        Time = DateTime.Now
                    },
                    new TableReconFHSS()
                    {
                        Deviation = rand.Next(10, 1000) / 10.0f,
                        FreqMaxKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        FreqMinKHz = rand.NextDouble() * rand.Next(10000, 10000000),
                        ImpulseDuration = (ushort)rand.Next(1, 100000),
                        Modulation = (byte)rand.Next(0, 255),
                        QuantitySignal = (short)rand.Next(1, 100000),
                        StepKHz = (byte)rand.Next(0, 255),
                        Time = DateTime.Now
                    }
            };
                clientDB.Tables[NameTable.TableReconFHSS].AddRangeAsync(lFHSS);

                
                
            }
            if ((NameTable)Tables.SelectedItem == NameTable.TempSuppressFWS)
                {
                    List<TempSuppressFWS> lFHSStt = new List<TempSuppressFWS>();
                for (int i=0; i<3; i++)
                {
                    lFHSStt.Add(
                         new TempSuppressFWS()
                         {
                             Id = rand.Next(1, 10000),
                             Control = Led.Blue,
                             Radiation = Led.Green,
                             Suppress = Led.Red
                         });
                }
                    



                    clientDB.Tables[NameTable.TempSuppressFWS].AddRangeAsync(lFHSStt);

             }
        }


        Task task;
        bool flag = false;
        private void SendCycle_Click(object sender, RoutedEventArgs e)
        {
            flag = SendCycle.IsChecked.Value;
            if (flag)
            {
                task = new Task(funcTask);
                task.Start();
                return;
            }

        }
        async void funcTask()
        {
            Random rand = new Random();
            while (flag)
            {
                Thread.Sleep(100);
                var t = new TableSectorsRangesRecon() { Id = 0, NumberASP = 1, FreqMinKHz = rand.Next(30000, 100000), FreqMaxKHz = rand.Next(110000, 1100000) };
                await clientDB.Tables[NameTable.TableSectorsRangesRecon].CLearAsync();
                await clientDB.Tables[NameTable.TableSectorsRangesRecon].AddAsync(t);

            }
        }

        private void ChangeFWS_Click(object sender, RoutedEventArgs e)
        {
            var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
            if ((NameTable)Tables.SelectedItem != NameTable.TempFWS) return;

            Random rand = new Random();
            TempFWS record = new TempFWS
            {
                Id = AddrAsp,
                Coordinates = new Coord(),
                Deviation = rand.Next(0, 250),
                FreqKHz = rand.Next(150000, 250000),
                ListQ = new ObservableCollection<JamDirect>()
                        {
                            { new JamDirect() { Bearing = 1, DistanceKM =1, Level = 1, NumberASP = 1, IsOwn = true, Std = 1.00f} },

                            { new JamDirect() { Bearing = 1, DistanceKM =1, Level = 1, NumberASP = 1, IsOwn = true, Std = 1.00f} }
                        },
                Time = DateTime.Now,
                Type = (byte)rand.Next(0, 255),
                IsSelected = true
            };
            clientDB.Tables[NameTable.TempFWS].ChangeAsync(record);
        }

        private async void butChange_Click(object sender, RoutedEventArgs e)
        {
            var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
            Random rand = new Random();
            if ((NameTable)Tables.SelectedItem == NameTable.TableReconFWS)
            {
                TableReconFWS record = new TableReconFWS
                {
                    Id = AddrAsp,
                    ASPSuppr = rand.Next(0, 100),
                    Deviation = rand.Next(0, 100),
                    Coordinates = new Coord()
                    {
                        Altitude = rand.Next(0, 100),
                        Latitude = rand.Next(0, 100),
                        Longitude = rand.Next(0, 100),
                    },
                    FreqKHz = rand.Next(0, 100),
                    ListJamDirect = new ObservableCollection<TableJamDirect>()
                            {
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = 15, DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = 15, DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = 15, DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = 15, DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = 15, DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } },
                                { new TableJamDirect() { JamDirect = new JamDirect(){ Bearing = 15, DistanceKM = rand.Next(1, 1000)/10.0f, IsOwn = Convert.ToBoolean(((byte)rand.Next(0,1))),Level = (short)( -1* rand.Next(0, 100)), NumberASP = rand.Next(1,100)} } }
                            },
                    Time = DateTime.Now,
                };
                await clientDB.Tables[NameTable.TableReconFWS].ChangeAsync(record);

            }
            if ((NameTable)Tables.SelectedItem == NameTable.TableSuppressFWS)
            {
                TableSuppressFWS record = new TableSuppressFWS
                {
                    Id = AddrAsp,
                    Coordinates = new Coord(),
                    FreqKHz = rand.Next(150000, 250000),
                    Bearing = null,
                    Priority = null,
                    Letter = null,
                    Threshold = -876,
                    Sender = null
                };
                await clientDB.Tables[NameTable.TableSuppressFWS].ChangeAsync(record);
            }

            if ((NameTable)Tables.SelectedItem == NameTable.TempGNSS)
            {
                var record = (await clientDB.Tables[NameTable.TempGNSS].LoadAsync<TempGNSS>()).FirstOrDefault();
                if (record != null)
                {
                    record.CmpPA = 112;
                    record.CmpRR = 112;
                    record.Location = new Coord()
                    {
                        Altitude = 112,
                        Latitude = 112,
                        Longitude = 112
                    };
                    await clientDB.Tables[NameTable.TempGNSS].ChangeAsync(record);
                }

            }

            if ((NameTable)Tables.SelectedItem == NameTable.GlobalProperties)
            {
                var record = new GlobalProperties() { Latitude = 45, Longitude = 20 };

                await clientDB.Tables[NameTable.GlobalProperties].ChangeAsync(record);

            }

        }

        private void ButRemove_Click(object sender, RoutedEventArgs e)
        {
            var AddrAsp = Convert.ToInt32(FiltrAsp.Text);
            Random rand = new Random();
            dynamic lsect = new object();
            switch (Tables.SelectedItem)
            {
                case NameTable.TableSectorsRangesRecon:

                    lsect = new List<TableSectorsRangesRecon>()
                        {
                            { new TableSectorsRangesRecon(){ Id=1, NumberASP = AddrAsp} },
                            { new TableSectorsRangesRecon(){ Id=2, NumberASP = AddrAsp} },
                            { new TableSectorsRangesRecon(){ Id=3, NumberASP = AddrAsp} },
                            { new TableSectorsRangesRecon(){ Id=4, NumberASP = AddrAsp} }
                        };
                    break;

                case NameTable.TableReconFWS:

                    lsect = new List<TableReconFWS>()
                        {
                            { new TableReconFWS(){ Id=1} },
                            { new TableReconFWS(){ Id=2} },
                            { new TableReconFWS(){ Id=3} },
                            { new TableReconFWS(){ Id=4} }
                        };
                    break;
                case NameTable.TempFWS:
                    lsect = new List<TempFWS>()
                        {
                            { new TempFWS(){ Id=1} },
                            { new TempFWS(){ Id=2} },
                            { new TempFWS(){ Id=3} },
                            { new TempFWS(){ Id=4} }
                        };
                    break;
            }

            clientDB.Tables[(NameTable)Tables.SelectedItem].RemoveRangeAsync(lsect);
        }

        private void ADSBCycle_Click(object sender, RoutedEventArgs e)
        {
            flagADSB = ADSBCycle.IsChecked.Value;
            if (flagADSB)
            {
                taskADSB = new Task(sendADSBCycle);
                taskADSB.Start();
                return;
            }

        }
        bool flagADSB = false;
        Task taskADSB;

        async void sendADSBCycle()
        {
            Random rand = new Random();
            while (flagADSB)
            {
                Thread.Sleep(20);
                var rec = new TempADSB()
                {
                    Time = DateTime.Now.ToLongTimeString(),
                    ICAO = $"StrTest{rand.Next(0, 10)}",
                    Coordinates = new Coord()
                    {
                        Altitude = rand.Next(0, 360),
                        Latitude = rand.Next(0, 360),
                        Longitude = rand.Next(0, 360)
                    },
                };
                await clientDB.Tables[NameTable.TempADSB].AddAsync(rec);
            }

        }
    }
}
