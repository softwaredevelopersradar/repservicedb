﻿using System;
using System.Collections.Generic;
using ModelsTablesDBLib;
using OperationsTablesDBLib;
using InheritorsEventArgs;
using System.ServiceModel;

namespace ServerDBLib
{
    partial class ServiceDB : IServiceDB
    {
        void ICommonTableOperation.AddRangeRecord(NameTable nameTable, ClassDataCommon data, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TempSuppressFWS)
                    {
                        foreach (var rec in data.ListRecords)
                        {
                        
                            SendMessToHost(IdClient, NameTableOperation.AddRange, nameTable.ToString() + ", Id=" + (rec as TempSuppressFWS).Id + ", \n Radiation=" + (rec as TempSuppressFWS).Radiation + ", Suppress=" + (rec as TempSuppressFWS).Suppress + ", Control=" + (rec as TempSuppressFWS).Control + ", FreqKHz=" + (rec as TempSuppressFWS).FreqKHz + ", Threshold=" + (rec as TempSuppressFWS).Threshold);
                        }
                    }

                    SendMessToHost(IdClient, NameTableOperation.AddRange, nameTable.ToString());

                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }
                    dicOperTables[nameTable].AddRange(data, IdClient);
                    SendMessToHost(IdClient, NameTableOperation.AddRange, "Ok");

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.AddRange);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }

        void ICommonTableOperation.RemoveRangeRecord(NameTable nameTable, ClassDataCommon data, int IdClient)
        {

            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TempSuppressFWS)
                    {
                        foreach (var rec in data.ListRecords)
                        {
                            SendMessToHost(IdClient, NameTableOperation.RemoveRange, nameTable.ToString() + ", Id=" + (rec as TempSuppressFWS).Id + ", Radiation=" + (rec as TempSuppressFWS).Radiation + ", Suppress=" + (rec as TempSuppressFWS).Suppress + ", Control=" + (rec as TempSuppressFWS).Control + ", FreqKHz=" + (rec as TempSuppressFWS).FreqKHz + ", Threshold=" + (rec as TempSuppressFWS).Threshold);
                        }
                    }

                    SendMessToHost(IdClient, NameTableOperation.RemoveRange, nameTable.ToString());
                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }
                    ///Собираю данные о кол-ве записей в таблицах до удаления
                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();
                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }

                    dicOperTables[nameTable].RemoveRange(data, IdClient);


                    ///И сравниваю данные о кол-ве записей во всех таблицах после удаления
                    ///Это сделано по той причине, что при удалении записей из таблицы, которая имеет связную с ней таблицу, в связной автом. удаляться все зависимые записи
                    if (nameTable != NameTable.TableMission)
                    {

                        foreach (var table in keyValuePairs)
                        {
                            if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                dicOperTables[table.Key].UpDate(IdClient);
                        }
                    }
                    SendMessToHost(IdClient, NameTableOperation.RemoveRange, "Ok");

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.RemoveRange);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }

        void ICommonTableOperation.ChangeRecord(NameTable nameTable, NameChangeOperation nameOperation, AbstractCommonTable record, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TempSuppressFWS)
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString() + ", Id=" + (record as TempSuppressFWS).Id + ", Radiation=" + (record as TempSuppressFWS).Radiation + ", Suppress=" + (record as TempSuppressFWS).Suppress + ", Control=" + (record as TempSuppressFWS).Control + ", FreqKHz=" + (record as TempSuppressFWS).FreqKHz + ", Threshold=" + (record as TempSuppressFWS).Threshold);
                    }

                    SendMessToHost(IdClient, nameOperation, nameTable.ToString());
                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }

                    switch (nameOperation)
                    {
                        case NameChangeOperation.Add:
                            dicOperTables[nameTable].Add(record, IdClient);
                            break;
                        case NameChangeOperation.Change:
                            dicOperTables[nameTable].Change(record, IdClient);
                            break;
                        case NameChangeOperation.Delete:
                            Action actionDelete = () => dicOperTables[nameTable].Delete(record, IdClient);

                            if (dicOperTables[nameTable].IsTemp)
                            {
                                actionDelete();
                                break;
                            }

                            //хранит кол-во записей
                            Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                            foreach (var table in dicOperTables)
                            {
                                if (table.Key == nameTable)
                                    continue;
                                keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                            }

                            actionDelete();


                            if (nameTable == NameTable.TableMission) break;

                            foreach (var table in keyValuePairs)
                            {
                                if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                    dicOperTables[table.Key].UpDate(IdClient);
                            }

                            break;
                    }
                    SendMessToHost(IdClient, nameOperation, "Ok");

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, nameOperation);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }

        void ICommonTableOperation.ClearTable(NameTable nameTable, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.Clear, nameTable.ToString());
                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }

                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }
                    dicOperTables[nameTable].Clear(IdClient);


                    if (nameTable != NameTable.TableMission)
                    {

                        foreach (var table in keyValuePairs)
                        {
                            if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                dicOperTables[table.Key].UpDate(IdClient);
                        }
                    }
                    SendMessToHost(IdClient, NameTableOperation.Clear, "Ok");


                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.Clear);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }

        ClassDataCommon ICommonTableOperation.LoadData(NameTable nameTable, int IdClient)
        {
            try
            {
                SendMessToHost(IdClient, NameTableOperation.Load, nameTable.ToString());
                if (!clients.ContainsKey(IdClient))
                {
                    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    return null;
                }
                return dicOperTables[nameTable].Load(IdClient);
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(except));
            }
            catch (Exception error)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(error.Message));

                //SendErrorOfClient(ServerEventArgs.Error.UnknownError, error.Message);
                //return null;
            }
        }

        ClassDataDependASP ICommonTableOperation.LoadDataFilterASP(NameTable nameTable, int NumberASP, int IdClient)
        {
            try
            {
                SendMessToHost(IdClient, NameTableOperation.LoadByFilterAsp, nameTable.ToString());
                if (!clients.ContainsKey(IdClient))
                {
                    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    return null;
                }
                if (!(dicOperTables[nameTable] is IDependentAsp))
                {
                    Errors.EnumDBError error = Errors.EnumDBError.NoColumnAsp;

                    SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error));

                    throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(Errors.EnumDBError.NoColumnAsp));
                }

                var data = (dicOperTables[nameTable] as IDependentAsp).LoadByFilter(IdClient, NumberASP);

                return data;
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(except));
            }
            catch (Exception error)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(error.Message));

                //SendErrorOfClient(ServerEventArgs.Error.UnknownError, error.Message);
                //return null;s
            }
        }

        public ClassDataCommon LoadDataFilterRoute(int IdClient, int NumberRoute)
        {
            try
            {
                SendMessToHost(IdClient, NameTableOperation.LoadByFilterRoute, NameTable.TableRoute.ToString());
                if (!clients.ContainsKey(IdClient))
                {
                    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    return null;
                }

                var data = (dicOperTables[NameTable.TableRoute] as IRouteTable).LoadByFilter(IdClient, NumberRoute);

                return data;
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(except));
            }
            catch (Exception error)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(error.Message));
                //SendErrorOfClient(ServerEventArgs.Error.UnknownError, error.Message);
                //return null;
            }
        }

        public void ClearTableByFilter(NameTable nameTable, int IdFilter, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.ClearByFilter, nameTable.ToString());

                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }

                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();
                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }

                    if ((dicOperTables[nameTable] is IDependentAsp))
                    {
                        (dicOperTables[nameTable] as IDependentAsp).ClearByFilter(IdClient, IdFilter);

                    }

                    if (dicOperTables[nameTable] is IDependTableFHSS)
                    {
                        (dicOperTables[nameTable] as IDependTableFHSS).ClearByFilter(IdClient, IdFilter);

                    }

                    if (nameTable != NameTable.TableMission)
                    {
                        foreach (var table in keyValuePairs)
                        {
                            if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                dicOperTables[table.Key].UpDate(IdClient);
                        }
                    }
                    SendMessToHost(IdClient, NameTableOperation.ClearByFilter, "Ok");

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.ClearByFilter);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }

        private void UpDateMission(object obj, int IdClient)
        {
            try
            {
                foreach (var table in dicOperTables)
                {
                    if (table.Key == NameTable.TableMission)
                        continue;

                    if ((dicOperTables[table.Key] as Operation).IsTemp)
                    {
                        dicOperTables[table.Key].Clear(IdClient);
                        continue;
                    }

                    SendMessToHost(IdClient, NameTableOperation.Update, table.Key.ToString());
                    dicOperTables[table.Key].UpDate(IdClient);
                }
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendError(except, NameTableOperation.Update);
            }
        }
    }
}
