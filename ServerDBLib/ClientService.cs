﻿using System.ServiceModel;

namespace ServerDBLib
{
    public class ClientService
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public OperationContext OperContext { get; set; }
    }
}
