﻿using System.ServiceModel;
using OperationsTablesDBLib;
using InheritorsEventArgs;
using System;

namespace ServerDBLib
{
    [ServiceContract(CallbackContract = typeof(IServerCallback))]
    [ServiceKnownType(typeof(InheritorsEventArgs.OperationTableEventArgs))]
    [ServiceKnownType(typeof(AStandardEventArgs))]
    [ServiceKnownType(typeof(ModelsTablesDBLib.NameTableOperation))]
    [ServiceKnownType(typeof(ModelsTablesDBLib.NameChangeOperation))]
    [ServiceKnownType(typeof(InheritorsEventArgs.DataEventArgs))]
    [ServiceKnownType(typeof(Errors.DBError))]
    [ServiceKnownType(typeof(EventArgs))]
    [ServiceKnownType(typeof(RecordEventArgs))]
    public interface IServiceDB : ICommonTableOperation
    {
        [OperationContract]
        int Connect(string name);

        [OperationContract]
        void Disconnect(int IdClient);

        [OperationContract]
        bool Ping(int IdClient);

    }

    public interface IServerCallback : ICommonCallback
    {
        /// <summary>
        /// Получено сообщение об ошибке
        /// </summary>
        /// <param name="msg"></param>
        [OperationContract(IsOneWay = true)]
        void ErrorCallback(OperationTableEventArgs error);// (InheritorsException.ErrorServiceDb eventArgs);

        /// <summary>
        /// Отсоединение клиентов
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void Abort();
    }
}
